%
%  SOLVLU -  Forward/Backward substitution solver
%             for positive-definite
%             systems factored by LU's algorithm
%             L * U
%
function b = solvlu(L,U,b)
%
n = length(b);
%
% Forward step, by rows of L
%
for i = 2 : n
	for j = 1 : i-1
		b(i) = b(i) - L(i,j) * b(j);
	end
end
%
%
% Backward step, by rows of L
%
b(n) = b(n)/U(n,n);
for i = n-1 : -1 : 1
	for j = i+1 : n
		b(i) = b(i) - U(i,j) * b(j);
	end
    b(i) = b(i)/U(i,i);
end

%*******************************************************  
