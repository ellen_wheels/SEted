%% Calculo da matriz Y, G, B, gkm e bkm
clear i
%% Entrada

na = Linha (:,2); %barra de
nb = Linha (:,3); %barra para
r = Linha (:,4); %resistencia
x = Linha (:,5); %reatancia
bsh = (Linha (:,6))/2; %shunt de linha
bshb = barra (:,9);

% nramos
nramos = size(Linha(:,1),1);

%% Vetor/Matriz gkm e bkm

% gkm=r./((r.^2)+(x.^2));
%     
% bkm=-x./((r.^2)+(x.^2));

% gkm e bkm - elementos de linha
for aux=1:nramos
    matrizbshkm(na(aux),nb(aux))=bsh(aux);
    matrizbshkm(nb(aux),na(aux))=bsh(aux);
end
for aux=1:nbar
    matrizbshb(vetorB(aux),vetorB(aux))=bshb(aux);
end
% %% Montando a Matriz Y
% 
% matrizY = Ybarra;
% 
% 
%  %% Montando matriz G e B
% matrizG = real(matrizY);
% matrizB = imag(matrizY);

%%

%----------------------------------------------------------------------
% CARREGA OS DADOS DE BARRA E DE LINHA
%----------------------------------------------------------------------

% Dados de barra
bshbar = barra(:,9);
bshlin = linha(:,5)/2;

%----------------------------------------------------------------------
% MONTAGEM DA MATRIZ DE ADMITANCIAS Ybarra
%----------------------------------------------------------------------

Y = zeros(nbar);

for k = 1:nbar
    Y(k,k) = 1i*bshbar(k);
end

for j = 1:nl
    k = na(j);
    m = nb(j);
    y(j) = 1/(r(j)+1i*x(j));

    Y(k,k) = Y(k,k) + y(j) + 1i*bshlin(j);
    Y(m,m) = Y(m,m) + y(j) + 1i*bshlin(j);
    Y(k,m) = Y(k,m) - y(j);
    Y(m,k) = Y(m,k) - y(j);
end

% Ybarra = Y;
% G = real(Ybarra);   % Parte real da matriz Ybarra
% B = imag(Ybarra);   % Parte Imaginaria da matriz Ybarra

%% Montando a Matriz Y
Ybarra = Y;
matrizY = Ybarra;

 %% Montando matriz G e B
matrizG = real(matrizY);
matrizB = imag(matrizY);
