%% ARQUIVO PRINCIPAL
%%
%    disp('###########################################################################')
% fprintf('#                                                                         #\n');
%    disp('#                    ESTIMADOR DE ESTADOS DESACOPLADO                     #')
% fprintf('#                                                                         #\n');
%    disp('###########################################################################')

%% VERS�ES
% 03/05/16 - Atualiza��o da matriz Jacobiana nas itera��es
% Arquivos alterados: monta_delta_zlinha_SE, monta_delta_zAlinha_SE e
% monta_delta_zRlinha_SE
% Arquivos criados: monta_HAA_SE e monta_HRR_SE
% 08/07/16 - Inserida permuta��o na atualiza��o de deltaAt e deltaReat
% 18/07/16 - Vers�o Desacoplada no Algoritmo

%%

% if arquivo == 14
%     sist14bns
% elseif arquivo == 142
%     sist14bns2
% elseif arquivo == 143
%      sist14bns3
% elseif arquivo == 144
%      sist14bns4
% elseif arquivo == 145
%      sist14bns5
% elseif arquivo == 146
%      sist14bns6
% else
%     input('Digite o nome do arquivo de dados >> ')
%     disp(' ')
% end
%%
vetorL = 1:1:size(linha,1);
vetorB = 1:1:size(barra,1);
Linha = [vetorL' linha];

%%
nref = length(ref);
nbar = max(linha(:,2));
nramos = size(Linha(:,1),1);
nl = nramos-nd;
%par�metros iniciais
V(1:nbar,1)=1;  % V0 - tens�o inicial
teta(1:nbar,1)=0; % teta0 - �ngulo inicial
Vnew(1:nbar,1)=1; % tens�o estimada na itera��o

 %% N�vel SE
% t_est(1:nd,1) = 0; % fluxo de pot�ncia ativa inicial nos disjuntores
% u_est(1:nd,1) = 0; % fluxo de pot�ncia reativa inicial nos disjuntores

%% Vetor de Estados
xnew = [teta;Vnew];    

%% Matrix Y
matrizes_SE;
%%
% [z] = monta_Zzao(Medidas);
[R] = monta_Rzao(Medidas);
[h] = monta_hzinho(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas);

[H] = monta_Hzao(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas); %alterar
[G] = monta_G(H,R); %alterar

[delta_z] = monta_deltaz(z,h);
[delta_zlinha] = monta_delta_zlinha(delta_z, V, Medidas);
[T] = monta_T(H, R, delta_zlinha); 

%% Permuta��o LU

tic;
    [L,U,P]=lu(G);
    % Permutando vetor do lado direito de acordo com permutacao da fatoracao
    perm = P*T;
    delta = solvlu (L,U,perm);
tempocompleto = toc;
%%

deltaeq = [0;
          delta];

tic;
      
if nref ~= 1
    for aux = 2:nref
        bref = ref(aux);
    deltaeq = [deltaeq(1:bref-1);
               0
               deltaeq(bref+1:2*(nd+nbar))];
    end    
end

xnew = xnew + deltaeq;
contit=1;
%%
it=max(abs(delta));

if ((it>tol)) %inicia o processo iterativo se o criterio ainda n�o foi atingido
%         for contit = 1:2
while((it>tol))

            tetanew = xnew(1:nbar);
%             tnew = xnew(nbar+1:nbar+nd);
            Vnew = xnew(nbar+1:2*nbar);  
%             unew = xnew(2*nbar+nd+1:2*(nbar+nd));
            
            [h] = monta_hzinho(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas); % ok
            [H] = monta_Hzao(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas); %ok
            [G] = monta_G(H,R);
            [delta_z] = monta_deltaz(z,h); %ok
            [delta_zlinha] = monta_delta_zlinha(delta_z, Vnew, Medidas); %ok
            [T] = monta_T(H, R, delta_zlinha); %ok

                % atualizar V
                % permuta��o LU
                 [L,U,P]=lu(G);
%                 Permutando vetor do lado direito de acordo com permutacao da fatoracao
                perm = P*T;
                delta = solvlu (L,U,perm);
                %
                deltaeq = [0;
                           delta];
                xnew=xnew+deltaeq; 

                contit=1+contit;
                it=max(abs(delta));
                
                
                tempoit = toc;
             
                if contit>=30
                    disp('O programa atingiu 30 itera��es e n�o convergiu.');
                    break;
                end
 
end  
else  %caso os criterios tenham sido atingidos, fornece a resposta.
        disp('RESULTADO') 
         Vnew;
         (tetanew./pi).*180;
%          if nd~=0
%              unew = u_est;
%          unew;
%          tnew;
%          end

end
%%
