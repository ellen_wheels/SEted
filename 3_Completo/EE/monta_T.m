function [ T ] = monta_T( H, R, delta_zlinha)
% ESTA FUN��O CALCULA O TA=HAA'*RA^-1*AzA'
%                       TR=HRR'*RR^-1*AzR'

T = H'*inv(R)*delta_zlinha;

end