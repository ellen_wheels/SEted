function [ G ] = monta_G(H, R)
% ESTA FUN��O CALCULA O GAA=HAA'*RA^-1*HAA
%                       GRR=HRR'*RR^-1*HRR

G=H'*inv(R)*H;

end