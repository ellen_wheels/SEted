%    disp('###########################################################################')
% fprintf('#                                                                         #\n');
%    disp('#                    ESTIMADOR DE ESTADOS DESACOPLADO                     #')
% fprintf('#                                                                         #\n');
%    disp('###########################################################################')

% if arquivo == 1
%     sistCasoOriginal
% elseif arquivo == 2
%     sistDist
% elseif arquivo == 135
%     sistDist135
% elseif arquivo == 1352
%     sistDist135_radial
% elseif arquivo == 1350
%     sist135_ang0
% elseif arquivo == 13510
%     sist135_ang10
% elseif arquivo == 13520
%     sist135_ang20
% elseif arquivo == 13530
%     sist135_ang30
% elseif arquivo == 13540
%     sist135_ang40
% elseif arquivo == 13550
%     sist135_ang50
% elseif arquivo == 13560
%     sist135_ang60
% elseif arquivo == 13570
%     sist135_ang70
% elseif arquivo == 13580
%     sist135_ang80
% elseif arquivo == 13590
%     sist135_ang90    
% else
%     input('Digite o nome do arquivo de dados >> ')
%     disp(' ')
% end
%%
vetorL = 1:1:size(linha,1);
vetorB = 1:1:size(barra,1);
Linha = [vetorL' linha];

%%
nref = size(ref,1);
nbar = max(linha(:,2));
%par�metros iniciais
V(1:nbar,1)=1;  % V0 - tens�o inicial
teta(1:nbar,1)=0; % teta0 - �ngulo inicial
Vnew(1:nbar,1)=1; % tens�o estimada na itera��o

%%
matrizes;
%%
[zA,zR] = monta_Zzao(Medidas);
[RA,RR] = monta_Rzao(Medidas);

[ HAA, HRR ] = monta_Hzao(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas, ref);
[ haa, hrr ] = monta_hzinho(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas);
[ GAA, GRR ] = monta_G(HAA, RA, HRR, RR);
[ delta_zA,  delta_zR ] = monta_deltaz(zA, zR, haa, hrr);
[ delta_zAlinha,  delta_zRlinha ] = monta_delta_zlinha(delta_zA, delta_zR, V, Medidas);
[ TA ] = monta_TA(HAA, RA, delta_zAlinha);
[ TR ] = monta_TR(HRR, RR, delta_zRlinha);

%% Permuta��o LU

    [LA,UA,PA]=lu(GAA);
    % Permutando vetor do lado direito de acordo com permutacao da fatoracao
    permA = PA*TA;
    deltateta = solvlu (LA,UA,permA);
    
    %% Permuta��o LU

    [LR,UR,PR]=lu(GRR);
    % Permutando vetor do lado direito de acordo com permutacao da fatoracao
    permR = PR*TR;
    deltaV = solvlu (LR,UR,permR);
    
%%
deltatetaeq = [zeros(size(nref,1));
               deltateta];
tetanew=teta+deltatetaeq; % teta estimado na itera��o

contativa=1;
contreativa=0;
contit = 1;
%%
ativo=max(abs(deltateta));
reativo=max(abs(deltaV));

% tol = 1e-3;

if ((ativo>tol)||(reativo>tol)) %inicia o processo iterativo se o criterio ainda n�o foi atingido
     V;
    (tetanew./pi).*180;
    
while((ativo>tol)||(reativo>tol))

         % meia itera��o reativa
            if(reativo>tol)
                 
%          [ HRR ] = monta_HRR(V, teta, matrizG, matrizB, matrizgkm, matrizbkm, matrizbshkm, Linha, Medidas, ref);
           [ hrr ] = monta_hzinhorr(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);
           [ delta_zR ] = monta_deltazR(zR, hrr);
           [ delta_zRlinha ] = monta_delta_zRlinha(delta_zR, V, Medidas);
           [ TR ] = monta_TR(HRR, RR, delta_zRlinha);

                %atualizar V
%                 [L,U,P]=lu(GRR);
%                 % Permutando vetor do lado direito de acordo com permutacao da fatoracao
                permR = PR*TR;
                deltaV = solvlu (LR,UR,permR);
    
                Vnew=Vnew+deltaV; %vai passar a ser uma matriz [Vnew; tkmnew] = [Vnew; tkmnew] + [deltaV deltatkm]

                contreativa=1+contreativa;
                Vnew;
                (tetanew./pi).*180;

            contit = contit +1; %atualizar contador da itera��o
            reativo=max(abs(deltaV));
            
             end
             
%             if (reativo<tol)
%                 
%                 while(ativo>tol)
%           
% %          [ HAA ] = monta_HAA(V, teta, matrizG, matrizB, matrizgkm, matrizbkm, matrizbshkm, Linha, Medidas, ref);
%            [ haa ] = monta_hzinhoaa(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);
%            [ delta_zA ] = monta_deltazA(zA, haa);
%            [ delta_zAlinha ] = monta_delta_zAlinha(delta_zA, V, Medidas);
%            [ TA ] = monta_TA(HAA, RA, delta_zAlinha);
% 
%                 %atualizar teta
%                 [L,U,P]=lu(GAA);
%                 % Permutando vetor do lado direito de acordo com permutacao da fatoracao
%                 perm = P*TA;
%                 deltateta = solvlu (L,U,perm);
%     
%                 deltatetaeq = [0;
%                                deltateta];
%                 tetanew=tetanew+deltatetaeq;
% 
%                 contativa=1+contativa;
%                  Vnew;
%                 (tetanew./pi).*180;
%                 
%             contint = contint +1; %atualizar contador da itera��o
%             ativo=max(abs(deltateta));
%             
%                      if(ativo<tol)
%                          break;
%                      end
%                 end
%             end
            
            if(ativo>tol)
           
%          [ HAA ] = monta_HAA(V, teta, matrizG, matrizB, matrizgkm, matrizbkm, matrizbshkm, Linha, Medidas, ref);
           [ haa ] = monta_hzinhoaa(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);
           [ delta_zA ] = monta_deltazA(zA, haa);
           [ delta_zAlinha ] = monta_delta_zAlinha(delta_zA, V, Medidas);
           [ TA ] = monta_TA(HAA, RA, delta_zAlinha);

                %atualizar teta
                [LA,UA,PA]=lu(GAA);
                % Permutando vetor do lado direito de acordo com permutacao da fatoracao
                permA = PA*TA;
                deltateta = solvlu (LA,UA,permA);
    
                deltatetaeq = [0;
                               deltateta];
                tetanew=tetanew+deltatetaeq;

                contativa=1+contativa;
                Vnew;
                (tetanew./pi).*180;
                
            contit = contit +1; %atualizar contador da itera��o
            ativo=max(abs(deltateta));
           
            end
            
%             if (ativo<tol)
%                              
%                 while(reativo>tol)
%                     
% %          [ HRR ] = monta_HRR(V, teta, matrizG, matrizB, matrizgkm, matrizbkm, matrizbshkm, Linha, Medidas, ref);
%            [ hrr ] = monta_hzinhorr(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);
%            [ delta_zR ] = monta_deltazR(zR, hrr);
%            [ delta_zRlinha ] = monta_delta_zRlinha(delta_zR, V, Medidas);
%            [ TR ] = monta_TR(HRR, RR, delta_zRlinha);
% 
%                 %atualizar V
%                 [L,U,P]=lu(GRR);
%                 % Permutando vetor do lado direito de acordo com permutacao da fatoracao
%                 perm = P*TR;
%                 deltaV = solvlu (L,U,perm);
%     
%                 Vnew=Vnew+deltaV;
% 
%                 contreativa=1+contreativa;
%                 Vnew;
%                 (tetanew./pi).*180;
% 
%             contint = contint +1; %atualizar contador da itera��o
%             reativo=max(abs(deltaV));
%                   
%                      if(reativo<tol)
%                           break;
%                      end
%                 end
%             end 
                if contit>=200
                    disp('************************************************');
                    disp('O programa atingiu 30 itera��es e n�o convergiu.');
                    disp('************************************************');
                break;
                end
%              % encerra o processo iterativo     
%                 if ((ativo<tol)||(reativo<tol))
%                    break;
%                 end
end  
else  %caso os criterios tenham sido atingidos, fornece a resposta.
        disp('RESULTADO') 
        fprintf('Resultados do %g� looping ativo', contativa);
        fprintf('\n');
        V;
        (tetanew./pi).*180;
end

