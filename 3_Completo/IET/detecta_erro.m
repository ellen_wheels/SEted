%% 

% 21/08/17 - Detectar erro de topologia via m�ximo res�duo normalizado
% 17/10/17 - Altera��o para quando o omega for zero!

%% Calculando o res�duo
[h] = monta_hzinho(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);

r = z-h;

%% Calculando a ra�z quadrada de omega

omega = R - (H*inv(G)*H');
omega_sqrt = sqrt(diag(omega));


%% Calculando o res�duo normalizado

for cont = 1:size(Medidas,1)
    if omega_sqrt(cont,1) == 0
        r_N(cont,1) = 0;
    else
        r_N(cont,1) = r(cont,1)/omega_sqrt(cont,1);
    end
end
resn = [Medidas(:,1) Medidas(:,3:5) r_N];


%% Detec��o de erro

r_Nmax = max(abs(r_N));

for n = 1:size(r_N,1)
    if abs(r_N(n,1))== r_Nmax
        resnmax = resn(n,:);
    end
end

