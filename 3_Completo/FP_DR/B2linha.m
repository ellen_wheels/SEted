% =========================================================================
% ROTINA RESPONS�VEL PELA FORMA��O DE B2linha NO CASO DE UM BANCO DE DADOS
% BARRA-RAMO OU B2linha_ext NO CASO DE UM SISTEMA QUE APRESENTE RAMOS
% CHAVEADOS
% =========================================================================

% nbar: no barras
% nr: numero ramos (incluindo disjuntores)
% bshlin: shunt da linha j� dividido por 2
if nd == 0

    B2l_ext = zeros(nbar);
    for l = 1:nr
        i = na(l);
        j = nb(l);
        bser = -x(l)/(r(l)^2+x(l)^2);
        B2l_ext(i,i) = B2l_ext(i,i) - (bser + bshlin(l));
        B2l_ext(j,j) = B2l_ext(j,j) - (bser + bshlin(l));
        B2l_ext(i,j) = B2l_ext(i,j) + bser;
        B2l_ext(j,i) = B2l_ext(j,i) + bser;
    end

else

    B2l_ext = zeros(nbar+nd);
    s = 1;
    for l = 1:nr
        i = na(l);
        j = nb(l);
        if (x(l) ~= 0.0 & x(l) ~= 9999)
            bser = -x(l)/(r(l)^2+x(l)^2);
            B2l_ext(i,i) = B2l_ext(i,i) - (bser + bshlin(l));
            B2l_ext(j,j) = B2l_ext(j,j) - (bser + bshlin(l));
            B2l_ext(i,j) = B2l_ext(i,j) + bser;
            B2l_ext(j,i) = B2l_ext(j,i) + bser;
        elseif x(l) == 0.0
            B2l_ext(i,nbar+s) = 1;
            B2l_ext(j,nbar+s) = -1;
            B2l_ext(nbar+s,i) = 1;
            B2l_ext(nbar+s,j) = -1;
            s = s + 1;
        elseif x(l) == 9999.
            B2l_ext(i,nbar+s) = 1;
            B2l_ext(j,nbar+s) = -1;
            B2l_ext(nbar+s,nbar+s) = 1;
            s = s + 1;
        end
    end

end

% Considerando shunt de barra
% Obs que na entrada de dados shunt � positivo se for capacitivo e negativo se for indutivo
for l=1:nbar
    B2l_ext(l,l) = B2l_ext(l,l)-bshbar(l);
end
% % Caso particular
% if num_df==num_disj
%     B(n+dref,:)=0;
%     B(n+dref,n+dref)=1;
% end

%%Definicao das referencias, zerando a linha e coluna e colocando 'um' na diagonal%%
for l=1:nbar
    if (tipo(l) == 1 || tipo(l) == 2)
        B2l_ext(l,l) = eps;
    end
end

if nd==0 % Fluxo convencional
    ns = nbar; % numero de estados para fluxo convencional
    bb = DQ./V; % vetor do lado direito para fluxo convenicional
else % Fluxo no nivel de subestacao
    ns = nbar + nd; % numero de estados
    bb = [DQ./V;zeros(nd,1)]; % vetor do lado direito
end
