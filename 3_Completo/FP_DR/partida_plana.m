%----------------------------------------------------------------------
% DEFINI�AO DO ESTADO INICIAL DA REDE PARA O CALCULO DAS POTENCIAS
% ATIVAS (Pcalc) E REATIVAS (Qcalc), E PARA CALCULO DOS DESVIOS
% DE POTENCIA ATIVA (DP) E REATIVA (DQ).
%----------------------------------------------------------------------
% PARTIDA PLANA:
% Define-se angulo = 0 p/ barras PQ (tipo 0) e PV (tipo 1)
% e define-se o valor de tensao = 1 pu p/ barras PQ (tipo 0)
% (barra de referencia: tipo 2)

% for k = 1:nbar
%     if tipo(k)~= 2
%         teta(k) = 0.0;
%         if tipo(k) == 0
%             V(k) = 1.0;
%         end
%     end
% end

% Inicializa fluxos ativos e reativos nos disjuntores e iguala tensoes das barras
% terminais dos disjuntores fechados sempre que uma delas for do tipo 1 ou 2

for j = 1:nd
    t(j,1) = 0;
    u(j,1) = 0;
    jj = j + nl;
    k = na(jj);
    m = nb(jj);
    if (status(j) == 1 & tipo(k) ~= 0)
        V(m) = V(k);
    elseif (status(j) == 1 & tipo(m) ~= 0)
        V(k) = V(m);
    end

end