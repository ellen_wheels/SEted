%----------------------------------------------------------------------
% MONTAGEM DA MATRIZ DE ADMITANCIAS Ybarra
%----------------------------------------------------------------------

Y = zeros(nbar);

for k = 1:nbar
    Y(k,k) = i*bshbar(k);
end

for j = 1:nl
    k = na(j);
    m = nb(j);
    y(j) = 1/(r(j)+i*x(j));

    Y(k,k) = Y(k,k) + y(j) + i*bshlin(j);
    Y(m,m) = Y(m,m) + y(j) + i*bshlin(j);
    Y(k,m) = Y(k,m) - y(j);
    Y(m,k) = Y(m,k) - y(j);
end

Ybarra = Y;
G = real(Ybarra);   % Parte real da matriz Ybarra
B = imag(Ybarra);   % Parte Imaginaria da matriz Ybarra