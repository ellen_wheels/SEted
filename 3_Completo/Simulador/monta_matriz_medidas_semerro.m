function [Medidas,MedAt,MedReat] = monta_matriz_medidas_semerro(Pkm, Qkm, Pcalc, Qcalc, V, linha, barra, nl, nbar)
%% FLUXO

% TIPO
% t - medida de fluxo de pot�ncia ativa
% u - medida de fluxo de pot�ncia reativa

% �NDICE
% 0 - n�o h� medida
% 1 - medida no extremo 'de'
% 2 - medida no extremo 'para'
% 3 - medida nos dois extremos

line = linha(:,1:2);
med_t = linha(:,6);
med_u = linha(:,7);
sigma = 0.001;
jj=0;
ii=0;

%% MEDIDAS NAS LINHAS (RAMOS CONVENCIONAIS)

for aux = 1:nl
    %% MEDIDAS DE FLUXO ATIVO
    if med_t(aux) == 1
        jj = jj+1;
        t_info(jj,:) = [line(aux,:) Pkm(aux)];
    end
    %% MEDIDAS DE FLUXO REATIVO
    if med_u(aux) == 1
        ii = ii+1;
        u_info(ii,:) = [line(aux,:) Qkm(aux)];
    end
end

%% INJE��O

% TIPO
% p - medida de inje��o de pot�ncia ativa
% q - medida de inje��o de pot�ncia reativa
% v - medida de magnitude de tens�o
% tetaref - medida de �ngulo de refer�ncia
% i - medida de magnitude de corrente

% �NDICE
% 0 - n�o h� medida
% 1 - h� medida

bus = barra(:,1);
med_p = barra(:,10); % inje��o de pot�ncia ativa
med_q = barra(:,11); % inje��o de pot�ncia reativa
med_v = barra(:,12); % m�dulo da tens�o

rr = 0;
tt = 0;
xx = 0;

%% MEDIDAS NAS BARRAS

for aux = 1:nbar
    %% MEDIDAS DE INJE��O DE POT�NCIA ATIVA
    if med_p(aux) == 1
        rr = rr+1;
        p_info(rr,:) = [bus(aux,:) Pcalc(aux)];
    end
    %% MEDIDAS DE INJE��O DE POT�NCIA REATIVA
    if med_q(aux) == 1
        tt = tt+1;
        q_info(tt,:) = [bus(aux,:) Qcalc(aux)];
    end
    %% MEDIDAS DE M�DULO DA TENS�O
    if med_v(aux) == 1
        xx = xx+1;
        v_info(xx,:) = [bus(aux,:) V(xx,1)];
    end
    
end

%% RESULTADOS

    nmed = 1;
%     fprintf('\n \n');
%     disp('================================================================');
%     disp('                     MEDIDAS ATIVAS                             ');
%     disp('================================================================');
    %%
    if sum(med_t)~=0
%     disp('________________________________________________________________');
%     fprintf('      FLUXO DE POT�NCIA ATIVO NOS RAMOS CONVENCIONAIS\n')
%     disp('________________________________________________________________');
%     fprintf('      De       Para       Pkm\n')
%     fprintf('                         (pu)\n')
%     disp('________________________________________________________________');
%     fprintf('\n');
%     disp(t_info)
    nmed = 1 + size(t_info,1);
    % %         Medidas                      tipo                      n               i    j         valor (pu) cov(pu)(sqrt(R))  
    MedAt = [ones(size(t_info,1),1)  (1:nmed-1)' zeros(size(t_info,1),1) t_info(:,1:2)    t_info(:,3)    8*sigma*ones(size(t_info,1),1)];
    end
    %%
    if sum(med_p)~=0
%     disp('________________________________________________________________');
%     fprintf('      INJE��O DE POT�NCIA ATIVA\n')
%     disp('________________________________________________________________');
%     fprintf('    Barra       p\n')
%     fprintf('               (pu)\n')
%     disp('________________________________________________________________');
%     fprintf('\n');
%     disp(p_info)
    nmed2 = nmed + size(p_info,1)-1;
    % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
    MedAt(nmed:nmed2,:) = [3*ones(size(p_info,1),1)  (nmed:nmed2)'   p_info(:,1) zeros(size(p_info,1),2)    p_info(:,2)    10*sigma*ones(size(p_info,1),1)];
    nmed = nmed + size(p_info,1);
    end
    %%
    nmed = 1;
%     fprintf('\n \n');
%     disp('================================================================');
%     disp('                     MEDIDAS REATIVAS                             ');
%     disp('================================================================');
    %%
    if sum(med_u)~=0
%     disp('________________________________________________________________');
%     fprintf('      FLUXO DE POT�NCIA REATIVO NOS RAMOS CONVENCIONAIS\n')
%     disp('________________________________________________________________');
%     fprintf('      De       Para       Qkm\n')
%     fprintf('                         (pu)\n')
%     disp('________________________________________________________________');
%     fprintf('\n');
%     disp(u_info)
    nmed2 = nmed + size(u_info,1)-1;
    % %                                 Medidas     tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
    MedReat(nmed:nmed2,:) = [2*ones(size(u_info,1),1) (nmed:nmed2)'   zeros(size(u_info,1),1) u_info(:,1:2)    u_info(:,3)    8*sigma*ones(size(u_info,1),1)];
    nmed = nmed + size(u_info,1);
    end
    %%
    if sum(med_q)~=0
%     disp('________________________________________________________________');
%     fprintf('      INJE��O DE POT�NCIA REATIVA\n')
%     disp('________________________________________________________________');
%     fprintf('    Barra       q\n')
%     fprintf('               (pu)\n')
%     disp('________________________________________________________________');
%     fprintf('\n');
%     disp(q_info)
    nmed2 = nmed + size(q_info,1)-1;
    % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
    MedReat(nmed:nmed2,:) = [4*ones(size(q_info,1),1)  (nmed:nmed2)'   q_info(:,1) zeros(size(q_info,1),2)    q_info(:,2)    10*sigma*ones(size(q_info,1),1)];
    nmed = nmed + size(q_info,1);
    end
    %%
    if sum(med_v)~=0
%     disp('________________________________________________________________');
%     fprintf('     M�DULO DA TENS�O\n')
%     disp('________________________________________________________________');
%     fprintf('    Barra       V\n')
%     fprintf('               (pu)\n')
%     fprintf('\n');
%     disp(v_info)
    nmed2 = nmed + size(v_info,1)-1;
    % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
    MedReat(nmed:nmed2,:) = [5*ones(size(v_info,1),1)  (nmed:nmed2)'   v_info(:,1) zeros(size(v_info,1),2)    v_info(:,2)    4*sigma*ones(size(v_info,1),1)];
    nmed = nmed + size(v_info,1);
    end 
    %%

    Medidas = [MedAt;MedReat];
    nmedidas = size(Medidas,1);
%     disp('TIPOS DE MEDIDAS')
%     disp('1 - FLUXO DE POT�NCIA ATIVO NOS RAMOS CONVENCIONAIS')
%     disp('2 - FLUXO DE POT�NCIA REATIVO NOS RAMOS CONVENCIONAIS')
%     disp('3 - INJE��O DE POT�NCIA ATIVA')
%     disp('4 - INJE��O DE POT�NCIA REATIVA')
%     disp('5 - M�DULO DA TENS�O')
          
     fprintf('\n');
%     disp('==========================================================================================');
%     disp('                                    DADOS DE MEDIDAS                           ');
%     disp('==========================================================================================');
%     disp('      TIPO          N          BARRA        DE         PARA      MEDIDA(pu)  COV-(sqrt(R))') 
%     for n = 1:nmedidas
%     fprintf('  %7d        %4d         %4d        %4d        %4d      %9.3f       %7.3f \n', Medidas(n,1), Medidas(n,2),Medidas(n,3),Medidas(n,4),Medidas(n,5),Medidas(n,6),Medidas(n,7))
%     end

end

