%%
% 31/08/17 - Cria��o de arquivo
% 26/09/17 - Modifica��o do simulador de erros - Renan

%%
pergunta = input('\nAdicionar erros \n N�O (1) ou SIM (2)? >> ');
if pergunta == 1
     monta_z0_monofasico;
      [Medidas,MedAt,MedReat] = monta_matriz_medidas_semerro(Pkm, Qkm, Pcalc, Qcalc, V_medida, linha, barra, nl, nbar);

%      suporte_matriz_medidas; 
%      monta_matriz_medidas;
elseif pergunta == 2
     monta_z0_monofasico;
%      simulador_de_erros;
     simulador_de_erros_modificado_renan;   
     [Medidas,MedAt,MedReat] = monta_matriz_medidas_comerro(Pkm, Qkm, Pcalc, Qcalc, V_medida, linha, barra, nl, nbar, fepotencia);

%      suporte_matriz_medidas;
%      monta_matriz_medidas;
elseif (pergunta~=1) && (pergunta~=2)
     fprintf('\n\nOp��o inv�lida\n\n');
     return;
end