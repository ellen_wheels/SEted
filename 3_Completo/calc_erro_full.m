%% rotina para c�lculo do erro 

% 04/01/18 - cria��o do arquivo

%% V
erromaxV = max(abs(VEEfull-VFPfull));
aux = 0;
for n = 1:nbar
aux(n) = abs(VEEfull(n)-VFPfull(n));
end
erroabsV = sum(aux)/nbar;

aux2 = 0;
for n = 1:nbar
aux2(n) = (abs((VEEfull(n)-VFPfull(n))-erroabsV))^2;
end
desvpadV = sqrt(sum(aux2)/(nbar-1));

%% �ngulo
erromaxteta = max(abs(tetaEEfull-tetaFPfull));
aux3 = 0;
for n = 1:nbar
aux3(n) = abs(tetaEEfull(n)-tetaFPfull(n));
end
erroabsteta = sum(aux3)/nbar;

aux4 = 0;
for n = 1:nbar
aux4(n) = (abs((tetaEEfull(n)-tetaFPfull(n))-erroabsteta))^2;
end
desvpadteta = sqrt(sum(aux4)/(nbar-1));

%% Imprimindo na tela

fprintf('  Erro Max   Erro Abs  DesvPad \n');
fprintf('  %9.6f   %9.6f   %9.6f \n',erromaxV*1e3,erroabsV*1e3,desvpadV*1e3);
fprintf('  %9.6f   %9.6f   %9.6f \n',erromaxteta*1e3,erroabsteta*1e3,desvpadteta*1e3);

% fprintf('  %9.6f   %9.6f   %9.6f \n',erromaxV,erroabsV,desvpadV);
% fprintf('  %9.6f   %9.6f   %9.6f \n',erromaxteta,erroabsteta,desvpadteta);