%% ALTERACOES
% 31/08/17 - ERROS ALEATORIOS

%%
clear all
clc

%% DADOS

%% CPU    

N_inicio
abs(V_matpower);
(180/pi)*angle(V_matpower);
novos_dadosD135barras_radial_full
sistDist135_radial

%% CPU    
% cpu = input('Deseja normalizar os dados em pu(1) ou cpu(2)? >> ');
% if cpu==1
% elseif cpu ==2
%     [Barra,Geracao,Linha] = N_caso_radial136bus();
%     N_inicio
%     barra(:,5) = pdcpu;
%     barra(:,6) = qdcpu;
%     linha(:,3) = rcpu;
%     linha(:,4) = xcpu;
%     ng = size(pgcpu,1);
%     for aux = 1:ng
%         barra(Geracao(aux,1),7) = pgcpu(aux,1);
%         barra(Geracao(aux,1),8) = qgcpu(aux,1);
%     end
% end

 %% PU - para uso no simulador de medidas
% disp('Normalizando os dados em pu...');
%     [Barra,Geracao,Linha] = N_caso_radial136bus();
%     N_inicio
%     barra(:,5) = pdpu;
%     barra(:,6) = qdpu;
%     linha(:,3) = rpu;
%     linha(:,4) = xpu;
%     ng = size(pgcpu,1);
%     for aux = 1:ng
%         barra(Geracao(aux,1),7) = pgpu(aux,1);
%         barra(Geracao(aux,1),8) = qgpu(aux,1);
%     end

%% GERADOR DE MEDIDAS - FLUXO DE POT�NCIA
fprintf('\n');
% principal_pu;

matpowerparaEE_full;

nd = 0;
tol = 10^-3;

% % para o c�lculo do erro
% VFPfull = V;
% tetaFPfull = teta;

%% SIMULADOR DE ERROS ALEAT�RIOS
simula;

%% MONTANDO MATRIZ DE MEDIDAS
% cpu;

%     [Medidas] = monta_matriz_medidas_semerro(Pkm, Qkm, Pcalc, Qcalc, V, linha, barra, nl, nbar);

%% Fluxo n�o convergiu
% if iter >= itermax
%         return
% end

%% SIMULANDO ERRO DE TOPOLOGIA

%                erro_topologia

%% MONTA zA e zR

%                 [zA] = monta_ZA(Medidas, Linha, nramos);
%                 
                [z] = monta_Zzao(Medidas);
                 
%% SIMULANDO ERRO EM MEDIDA

%               erro_medida

%% ESTIMADOR DESACOPLADO
tic;
principal_completo
tempo = toc;

   disp(' RESULTADOS DO ESTIMADOR DE ESTADOS DESACOPLADO R�PIDO      ');
    disp('================================================================');
    fprintf('             Sistema com %g Barras e %g Ramos\n', nbar,nramos)
    disp('================================================================');
    fprintf('\n Tempo gasto no processo iterativo %g s\n', tempo);
    fprintf('\n N�mero de itera��es ativas: %g \n', contit);
 
    fprintf('\n');
    disp('================================================================');
    disp('         ESTIMADOR DE ESTADOS DESACOPLADO R�PIDO     ');
    disp('================================================================');
    disp('              BARRA       TENS�O(V)    �NGULO(GRAUS)') 
    for n = 1:nbar
    fprintf('              %4d      %9.4f       %7.4f \n',  n, Vnew(n), (tetanew(n).*180)/pi()   );
    end
    fprintf('\n');
    tempo
    
%     clear all
    
%% para o c�lculo do erro

VFPfull = abs(V_matpower);
tetaFPfull = angle(V_matpower);  
VEEfull = Vnew;
tetaEEfull = tetanew;

calc_erro_full

 %% IDENTIFICA��O DE ERROS DE TOPOLOGIA

% detecta_erro
% identifica_erro
% 
%     disp('================================================================');
%     disp('                  DETEC��O E IDENTIFICA��O DE ERROS    ');
%     disp('================================================================');
% 
%     disp('======================================');
%     disp('          ATIVO       rN  ') 
%     disp('======================================');
%     for n = 1:size(resn,1)
%         if resn(n,1)==1
%         fprintf('  Fluxo Ativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resn(n,2:4),resn(n,5));
%         elseif resn(n,1)==2
%         fprintf('  Fluxo Reativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resn(n,2:4),resn(n,5));
%         elseif resn(n,1)==3
%         fprintf('  Inje��o Ativa                             %d  %d  %d   -  %g          \n', resn(n,2:4),resn(n,5));
%         elseif resn(n,1)==4
%         fprintf('  Inje��o Reativa                             %d  %d  %d   -  %g          \n', resn(n,2:4),resn(n,5));
%         elseif resn(n,1)==5
%         fprintf('  M�dulo de Tens�o                            %d  %d  %d   -  %g          \n', resn(n,2:4),resn(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado')
%     fprintf('   %d %d  %d  %d   -  %g          \n', resnmax);
%   
%    fprintf('\n');
%     if (isempty(conjS)) == 0
%         disp('======================================');
%         disp('        TESTE DO COSSENO     ') 
%         disp('======================================');
%     elseif isempty(conjS) == 1
%         disp('======================================');
%         disp('        N�o apresenta erro     ') 
%         disp('======================================');
%     end 
%     if isempty(conjS) == 0
%         disp('Conjunto Suspeito ')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjS,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjS(n,1:4),conjS(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphi);
%     end
%    
% %%     
% %   sort (conjS(:,5))
% 
%   sort(resn(:,5))
% 
% % calc_erro

