% *************************************************************************
% *    INICIALIZAÇÃO DA PREPARAÇÃO DOS DADOS DE ENTRADA PARA MATPOWER     *
% *************************************************************************

close all;                                                                  % Fecha todas as janelas de figuras
clear all;                                                                  % Limpa o workspace (dados armazenados)
clc;                                                                        % Limpa a janela de comandos

%%                CARREGAMENTO DOS DADOS DE ENTRADA

        [Barra,Geracao,Linha,Vbase,Zbase,Sbase] = input('Digite o nome do arquivo de dados >> ');

[Barra,Geracao,Linha,V_matpower,bus_matpower,gen_matpower,branch_matpower,Ybarra] = N_preparacao_dados(Barra,Geracao,Linha,Vbase,Zbase,Sbase);                       % Função - PREPARAÇÃO DOS DADOS

