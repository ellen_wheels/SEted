% *************************************************************************
% *              C�LCULO DO �NGULO DE BASE - LINHAS (TORTELLI)            *
% *************************************************************************

function [angbase] = N_angulo_baseT(nl,nbar,nger,x,r,Pd,Qd,Pg,Qg,Geracao)

relxr = x./r;
Pgaux = zeros(nbar,1);
Qgaux = zeros(nbar,1);

alpha_avg  = 0;
epsilon = 0;

for j = 1:nl
    alpha_avg = alpha_avg + atan(x(j)/r(j));
end

    alpha_avg  = alpha_avg/nl;
    gama_avg = (atan(max(relxr))+atan(min(relxr)))/2;
              
for aux = 1:nger
    Pgaux(Geracao(aux,1)) = Pg(aux);    
    Qgaux(Geracao(aux,1)) = Qg(aux);
end

%for auxb = 1:nbar
%    epsilon = cos(atan((Qgaux(auxb)-Qd(auxb))/(Pgaux(auxb)-Pd(auxb))));
%end

%    epsilon = 1 - epsilon/nbar;
%    angbase = ((pi/2) - (alpha_avg + gama_avg)/2)*(1-epsilon);
    angbase = ((pi/2) - (alpha_avg + gama_avg)/2);
    
end
