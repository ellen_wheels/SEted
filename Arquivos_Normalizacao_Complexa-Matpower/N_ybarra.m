% *************************************************************************
% *               MONTAGEM DA MATRIZ DE ADMITANCIAS Ybarra                *
% *************************************************************************

function [G,B] = N_ybarra(nbar,nl,r,x,Linha,Barra)

Y = zeros(nbar);

for k = 1:nbar
    Y(k,k) = 1i*Barra(k,6);
end

for j = 1:nl
    k = Linha(j,1);
    m = Linha(j,2);
    y(j) = 1/(r(j)+1i*x(j));

    Y(k,k) = Y(k,k) + y(j) + 1i*Linha(j,5);
    Y(m,m) = Y(m,m) + y(j) + 1i*Linha(j,5);
    Y(k,m) = Y(k,m) - y(j);
    Y(m,k) = Y(m,k) - y(j);
end

Ybarra = Y;
G = real(Ybarra);   % Parte real da matriz Ybarra
B = imag(Ybarra);   % Parte Imaginaria da matriz Ybarra

