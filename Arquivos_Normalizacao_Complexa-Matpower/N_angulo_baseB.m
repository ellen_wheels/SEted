% *************************************************************************
% *              C�LCULO DO �NGULO DE BASE - BARRAS (HALEY)               *
% *************************************************************************

function [angbase] = N_angulo_baseB(nbar,nl,r,x,Linha,Barra)

[G,B] = N_ybarra(nbar,nl,r,x,Linha,Barra);                                  % Fun��o - YBARRA

s = 0;
for j = 1:nbar
    for p = 1:nbar
        if j==p
            s = s + atan(G(j,j)/B(j,j));
        end
    end
end

angbase = -s/nbar;                                          


