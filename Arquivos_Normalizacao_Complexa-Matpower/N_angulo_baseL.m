% *************************************************************************
% *              C�LCULO DO �NGULO DE BASE - LINHAS (GOMES)               *
% *************************************************************************

function [angbase] = N_angulo_baseL(nl,x,r)

s = 0;

for j = 1:nl
    s = s + (pi/2) - atan(x(j)/r(j));
end

angbase = s/nl;

