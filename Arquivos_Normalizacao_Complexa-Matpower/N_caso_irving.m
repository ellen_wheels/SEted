function [Barra,Geracao,Linha,Vbase,Zbase,Sbase] = N_caso_irving()
q = 1;                                                                      % Carregamento

Sbase = 1; %MVA

%% bus data %% dados em MW e MVar
%	bus_i	type	Pd      Qd          Gs	Bs	area	Vm      Va	baseKV	zone	Vmax	Vmin

Barra = [1	3	0 	0	    0	0	1	1	0	12.5	1	1.1	0.9;
         2	1	0.9	0.45      0	0	1	1	0	12.5	1	1.1	0.9;
         3	1	0.9 0.45		0	0	1	1	0	12.5	1	1.1	0.9;
        % 400	2	0.4	0.2	0	0	1	1	0	12.5	1	1.1	0.9;
        ];

%% bus data 

 nbarra = Barra(:,1); 
 nb = size(nbarra,1);
 type = Barra(:,2); % Padr�o Matpower
 Pd = Barra(:,3);
 Qd = Barra(:,4); 
 vbaseaux = Barra(:,10);
 Bshunt = Barra(:,6);
 Vmod = Barra(:,8);
 angulo = Barra(:,9);
 
 %% dados em MW e MVar
%	     bus_i	type	Pd    Qd   Gs                   Bs      area                Vm    Va    baseKV   zone                  Vmax	                     Vmin
Barra = [nbarra type    Pd    Qd   zeros(size(Barra,1),1) Bshunt  ones(size(Barra,1),1) Vmod    angulo   vbaseaux   ones(size(Barra,1),1)   0.9*ones(size(Barra,1),1)   1.1*ones(size(Barra,1),1)
    ];
         
%% generator data %% dados em MW e MVar
%	bus	Pg      Qg      Qmax	Qmin	Vg	mBase	status	Pmax	Pmin    Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
Geracao = [1	2	0.9 	10	-10	1.00	100	1	10	0	0	0	0	0	0	0	0	0	0	0	0];
	
%% branch data %% dados em pu
%	fbus	tbus	r       x       b       rateA	rateB	rateC	ratio	angle	status	angmin	angmax
Linha = [
	1	2	0.0481	0.0096	0	0	0	0	0	0	1	-360	360; %coloquei R e X trocados do Irving
	1	3	0.0641	0.0028	0	0	0	0	0	0	1	-360	360;
  %  2	3	0.0000	9999	0	0	0	0	0	0	0	-360	360;
	%400	1	0.003	0.006	0	0	0	0	1.025	0	1	-360	360;									
      ];
  


%% branch data 

 fromnew = Linha(:,1);
 tonew = Linha(:,2);
 nl = size(Linha,1);
 
 % Montando Vbase 
 ind = nbarra;
 [xf,yf] = ind2sub(ind,fromnew);
 [xt,yt] = ind2sub(ind,tonew);
 
 Vbase = vbaseaux(yf);
 
 Zbase = (Vbase.^2)/Sbase;
    
            Rpu = Linha(:,3);
            Xpu = Linha(:,4);
            Bpu = Linha(:,5);
            
%    for naux = 1:nl
%        for nzbase = 1:nb
% %            if naux == nzbase
%             Rpu(naux,1) = Linha(naux,3)/Zbase(nzbase,1);
%             Xpu(naux,1) = Linha(naux,4)/Zbase(nzbase,1);
%             Bpu(naux,1) = Linha(naux,5)/Zbase(nzbase,1);
%            end
%        end
%    end
 

%% dados em pu
%	       fbus	   tbus	    r    x     b   rateA       rateB       rateC       ratio       angle       status      angmin	        angmax
Linha = [ 
          fromnew tonew    Rpu  Xpu   Bpu  zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) ones(nl,1) -360*ones(nl,1)  360*ones(nl,1)
];
