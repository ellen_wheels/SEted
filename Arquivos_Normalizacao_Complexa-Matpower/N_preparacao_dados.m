% *************************************************************************
% *             PREPARA��O DOS DADOS DE ENTRADA PARA MATPOWER             *
% *************************************************************************

function [Barra,Geracao,Linha,V_matpower,bus,gen,branch,Ybus] = N_preparacao_dados(Barra,Geracao,Linha,Vbase,Zbase,Sbase)
%%                     LEITURA DOS DADOS DE ENTRADA

[nl,col]   = size(Linha);                                                   % Tamanho da matriz Linha
[nbar,col] = size(Barra);                                                   % Tamanho da matriz Barra
[nger,col] = size(Geracao);                                                 % Tamanho da matriz Gera��o

r = Linha(:,3);
x = Linha(:,4);
Pd = Barra(:,3);
Qd = Barra(:,4);
Pg = Geracao(:,2);
Qg = Geracao(:,3);

%%                     INFORMA��ES DADAS PELO USU�RIO

% Sbase = input('\nPot�ncia de base - Sbase (MVA) >> ');
% Vbase = input('\nTens�o de base   - Vbase (kV)  >> ');
resp1 = input('\nOs dados de barra foram inseridos em: MW/MVAr (1) ou p.u. (2)? ');
resp2 = input('\nOs dados de linha foram inseridos em: ohms (1) ou p.u. (2)? ');

%%      TRANSFORMA��O DOS DADOS DE P.U.EM VALORES REAIS DAS GRANDEZAS

% Zbase = Vbase.^2/Sbase;                                                      % C�lculo de Zbase

switch resp1
    case 2
        Pd = Pd*Sbase;                                                      % Transforma Pd pu -> MW
        Qd = Qd*Sbase;                                                      % Transforma Qd pu -> MVAr
        Pg = Pg*Sbase;                                                      % Transforma Pg pu -> MW
        Qg = Qg*Sbase;                                                      % Transforma Qg pu -> MVAr
end

switch resp2
    case 2
        r = r.*Zbase;                                                        % Transforma R pu -> ohms
        x = x.*Zbase;                                                        % Transforma X pu -> ohms
end

%%                      NORMALIZA��O COMPLEXA

disp('                                                          ');
disp('Selecione a op��o desejada: Entrada direta no MATPOWER (1)');
disp('                            Normaliza��o complexa      (2)');
resp3 = input('');

switch resp3
    case 1
        fprintf('---------------------------------------------------------')
        fprintf('\n               ENTRADA DIRETA NO MATPOWER\n')
        fprintf('---------------------------------------------------------\n')
        
        % �NGULO DE BASE NULO
        
        angbase = 0;                                                        % Normaliza��o convencional de r e x
        ang     = 0;

    case 2
        fprintf('---------------------------------------------------------')
        fprintf('\n                  NORMALIZA��O COMPLEXA\n')
        fprintf('---------------------------------------------------------\n')
        
        % C�LCULO DO �NGULO DE BASE
        
        disp('Escolha a op��o desejada: Entrar com valor do �ngulo de base escolhido     (1)')
        disp('                          Calcular �ngulo de base segundo GOMES - Linha (2)')        
        disp('                          Calcular �ngulo de base segundo TORTELLI et. al. (3)')
%         disp('                          Calcular �ngulo de base segundo HALEY - Barra (3)')
        resp3 = input('');
        
        switch resp3
            case 1
                ang = input('�ngulo de base (graus) >> ');
                angbase = ang*pi/180;
%             case 2
%                 [angbase] = N_angulo_baseT(nl,nbar,nger,x,r,Pd,Qd,Pg,Qg,Geracao);  % Chama fun��o - �NGULO DE BASE - LINHAS (TORTELLI)
%                 ang = angbase*180/pi;
            case 2
                [angbase] = N_angulo_baseL(nl,x,r);                         % Chama fun��o - �NGULO DE BASE - LINHAS (GOMES)
                ang = angbase*180/pi;
            case 3
                [angbase] = N_angulo_baseT(nl,nbar,nger,x,r,Pd,Qd,Pg,Qg,Geracao);  % Chama fun��o - �NGULO DE BASE - LINHAS (TORTELLI)
                ang = angbase*180/pi;
%              case 3
%                 [angbase] = N_angulo_baseB(nbar,nl,r,x,Linha,Barra);        % Chama fun��o - �NGULO DE BASE - BARRAS (HALEY)
%                 ang = angbase*180/pi;
        end
end

% NORMALIZA��O COMPLEXA E CONVENCIONAL DE P, Q, R, X

[rpu,xpu,rcpu,xcpu] = N_normaliz_complexa_z(nl,x,r,angbase,Zbase);          % Chama fun��o - NORMALIZA��O COMPLEXA Z
[pdpu,qdpu,pgpu,qgpu,pdcpu,qdcpu,pgcpu,qgcpu] = N_normaliz_complexa_S(nbar,nger,Pd,Qd,Pg,Qg,angbase,Sbase); % Chama fun��o - NORMALIZA��O COMPLEXA S

% NORMALIZA��O INVERSA DE P E Q
        
pd = pdcpu*Sbase;
qd = qdcpu*Sbase;
pg = pgcpu*Sbase;
qg = qgcpu*Sbase;

% RESULTADOS
        
fprintf('\n�ngulo de base = %g rad (%g�)\n',angbase,ang)
%         
% fprintf('\nVALORES REAIS DA GRANDEZA(OHMS - MW - MVar)\n')
% fprintf('\nr\n')
% fprintf('%f\n',r)
% fprintf('\nx\n')
% fprintf('%f\n',x)
% fprintf('\nPd\n')
% fprintf('%f\n',Pd)
% fprintf('\nQd\n')
% fprintf('%f\n',Qd)
% fprintf('\nPg\n')
% fprintf('%f\n',Pg)
% fprintf('\nQg\n')
% fprintf('%f\n',Qg)
%            
% fprintf('\nREPRESENTA��O EM P.U.\n')
% fprintf('\nrpu\n')
% fprintf('%f\n',rpu)
% fprintf('\nxpu\n')
% fprintf('%f\n',xpu)
% fprintf('\nPdpu\n')
% fprintf('%f\n',pdpu)
% fprintf('\nQdpu\n')
% fprintf('%f\n',qdpu)
% fprintf('\nPgpu\n')
% fprintf('%f\n',pgpu)
% fprintf('\nQgpu\n')
% fprintf('%f\n',qgpu)
%         
% fprintf('\nREPRESENTA��O EM C.P.U.\n')
% fprintf('\nrcpu\n')
% fprintf('%f\n',rcpu)
% fprintf('\nxcpu\n')
% fprintf('%f\n',xcpu)
% fprintf('\nPdcpu\n')
% fprintf('%f\n',pdcpu)
% fprintf('\nQdcpu\n')
% fprintf('%f\n',qdcpu)
% fprintf('\nPgcpu\n')
% fprintf('%f\n',pgcpu)
% fprintf('\nQgcpu\n')
% fprintf('%f\n',qgcpu)
%         
% fprintf('\nNOVOS VALORES REAIS DA GRANDEZA (MW - MVar)\n')
% fprintf('\nPd\n')
% fprintf('%f\n',pd)
% fprintf('\nQd\n')
% fprintf('%f\n',qd)
% fprintf('\nPg\n')
% fprintf('%f\n',pg)
% fprintf('\nQg\n')
% fprintf('%f\n',qg)
% fprintf('----------------------------------------------------------\n')
        
Barra(:,3) = pd;                                                            % Altera o valor de Pd
Barra(:,4) = qd;                                                            % Altera o valor de Qd
        
Geracao(:,2) = pg;                                                          % Altera o valor de Pg
Geracao(:,3) = qg;                                                          % Altera o valor de Qg
       
Linha(:,3) = rcpu;                                                          % Altera o valor de r
Linha(:,4) = xcpu;                                                          % Altera o valor de x

mpc = N_cria_arquivo(Sbase,Barra,Geracao,Linha);                            % Chama fun��o - CRIA_ARQUIVO

disp('Selecione o m�todo iterativo: Newton                (1)');
disp('                              Desacoplado R�pido XB (2)');
disp('                              Desacoplado R�pido BX (3)');
disp('                              Gauss-Seidel          (4)');
resp4 = input('');
mpopt = mpoption('PF_ALG',resp4);                                           % Seta o m�todo escolhido
[MVAbase, bus, gen, branch, success, et, V_matpower,Ybus]=runpf(mpc,mpopt);                                                           % Roda o MATPOWER para o novo arquivo de dados modificado

