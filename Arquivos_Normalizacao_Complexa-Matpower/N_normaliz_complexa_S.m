% *************************************************************************
% *                      NORMALIZAÇÃO DADOS DE BARRA                      *
% *************************************************************************

function [pdpu,qdpu,pgpu,qgpu,pdcpu,qdcpu,pgcpu,qgcpu] = N_normaliz_complexa_S(nbar,nger,Pd,Qd,Pg,Qg,angbase,Sbase)
%% NORMALIZAÇÃO DE PD E QD

for j = 1:nbar
    pdpu(j,1) = Pd(j)/Sbase;
    qdpu(j,1) = Qd(j)/Sbase;
    pdcpu(j,1) = pdpu(j,1)*cos(angbase)-qdpu(j,1)*sin(angbase);
    qdcpu(j,1) = qdpu(j,1)*cos(angbase)+pdpu(j,1)*sin(angbase);
end

%% NORMALIZAÇÃO DE PG E QG

for j = 1:nger
    pgpu(j,1) = Pg(j)/Sbase;
    qgpu(j,1) = Qg(j)/Sbase;
    pgcpu(j,1) = pgpu(j,1)*cos(angbase)-qgpu(j,1)*sin(angbase);
    qgcpu(j,1) = qgpu(j,1)*cos(angbase)+pgpu(j,1)*sin(angbase);
end