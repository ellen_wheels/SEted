% *************************************************************************
% *                      NORMALIZAÇÃO DADOS DE LINHA                      *
% *************************************************************************

function [rpu,xpu,rcpu,xcpu] = N_normaliz_complexa_z(nl,x,r,angbase,Zbase)
%% NORMALIZAÇÃO DE R E X

for j = 1:nl
    rpu(j,1) = r(j)/Zbase(j);
    xpu(j,1) = x(j)/Zbase(j);
    rcpu(j,1) = rpu(j,1)*cos(angbase)-xpu(j,1)*sin(angbase);
    xcpu(j,1) = xpu(j,1)*cos(angbase)+rpu(j,1)*sin(angbase);
end
