% *************************************************************************
% *                  NOVO ARQUIVO COM DADOS MODIFICADOS                   *
% *************************************************************************

function mpc = N_cria_arquivo(Sbase,Barra,Geracao,Linha)

% MATPOWER Case Format : Version 2
mpc.version = 'usuario';

% system MVA base
mpc.baseMVA = Sbase;

% bus data
%	bus_i	type	Pd      Qd      Gs	Bs	area	Vm      Va      baseKV	zone	Vmax	Vmin
mpc.bus = Barra; 

% generator data
%	bus	Pg      Qg      Qmax	Qmin	Vg      mBase	status	Pmax	Pmin	Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
mpc.gen = Geracao;

% branch data
%	fbus	tbus	r	x	b	rateA	rateB	rateC	ratio	angle	status	angmin	angmax
mpc.branch = Linha;

