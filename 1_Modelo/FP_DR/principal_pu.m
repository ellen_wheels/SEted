%**********************************************************************
%*                                                                    *
%*  UFPR - Universidade Federal do Parana                             *
%*                                                                    *
%*  Algoritmo para Calculo do Fluxo de Carga                          *
%*  Metodo Newton-Raphson Desacoplado R�pido                          *
%*                                                                    *
%*   Vers�o 1: NR com sistema modelado no n�vel de secao de barra     *
%**********************************************************************

% close all
% clear all
% clc
% dados14b

%----------------------------------------------------------------------
% VERIFICA O NUMERO DE BARRAS (nbar) E O NUMERO DE RAMOS (Convencionais
% e chave�veis)(nr).
%----------------------------------------------------------------------

[nbar,col] = size(barra);
[nr,col] = size(linha); %numero total de ramos: LTs, trafos e disjuntores

%----------------------------------------------------------------------
% CARREGA OS DADOS DE BARRA E DE LINHA
%----------------------------------------------------------------------

% Dados de barra
BARRAS = barra(:,1);
tipo   = barra(:,2);
V      = barra(:,3);
teta   = barra(:,4);
Pd     = barra(:,5);
Qd     = barra(:,6);
Pg     = barra(:,7);
Qg     = barra(:,8);
bshbar = barra(:,9);

% Calculo das injecoes de potencias ativa e reativa

Pesp    = (Pg - Pd);
Qesp    = (Qg - Qd);

% Dados de linha

na     = linha(:,1);
nb     = linha(:,2);
r      = linha(:,3);
x      = linha(:,4);
bshlin = linha(:,5)/2;

% Determina os status dos disjuntores e associa uma vari�vel
% de estado para cada disjuntor (state)

state = zeros(nr,1);
nd = 0;     % numero total de disjuntores
ndf = 0;    % num disjuntores fechados
for j = 1:nr
    
    if x(j) == 0
        ndf = ndf + 1; 
        nd = nd + 1;
        status(nd,1) = 1;
        state(j,1) = nd;
    
    elseif x(j) == 9999.
        nd = nd + 1; 
        status(nd,1) = 0;
        state(j,1) = nd;

    end
end

nda = nd - ndf; % numero de disjuntores abertos
nl = nr - nd;   %numero de ramos convencionais

ybarra % Chama a rotina para a montagem da matriz Ybarra e suas componentes

partida_plana % Chama a rotina de inicializa��o das vari�veis do fluxo de pot�ncia

% Define-se a tolerancia maxima admissivel para os debalan�os de
% potencia DP e DQ, o numero maximo de itera�oes permitida e o valor
% elevado (eps) utilizado para inclusao das equacoes "complementares"

iter = 0;
tol = 1e-3;
eps = 1e8;
itermax = 30;
maxDP=10^10;
maxDPQ=10^10;
decisao = input('Voc� deseja calcular o fluxo de potencia \n pelo m�todo tradicional (1) \n ou pelo desacoplado r�pido (2)? >> ');
if decisao == 1
    FPNR    % Chama a rotina para o c�lculo do fluxo de pot�ncia
            % via M�todo de N-R
else
    FPNRDR  % Chama a rotina para o c�lculo do fluxo de pot�ncia
            % via M�todo de N-R Desacoplado R�pido
end

%----------------------------------------------------------------------
% CALCULO DOS FLUXOS DE PONTENCIAS NAS LINHAS DE TRANSMISSAO E
% TRANSFORMADORES.
%----------------------------------------------------------------------

for j = 1:nl
    k = na(j);
    m = nb(j);

    % Calculo do angulo entre as barras
    tetakm = teta(k) - teta(m);

    Pkm(j) = -V(k)*V(k)*G(k,m) + V(k)*V(m)*(G(k,m)*cos(tetakm)+B(k,m)*sin(tetakm));
    Pmk(j) = -V(m)*V(m)*G(k,m) + V(m)*V(k)*(G(k,m)*cos(tetakm)-B(k,m)*sin(tetakm));
    Qkm(j) = V(k)*V(k)*(B(k,m)-bshlin(j)) - V(k)*V(m)*(B(k,m)*cos(tetakm)-G(k,m)*sin(tetakm));
    Qmk(j) = V(m)*V(m)*(B(k,m)-bshlin(j)) - V(m)*V(k)*(B(k,m)*cos(tetakm)+G(k,m)*sin(tetakm));

    %----------------------------------------------------------------------
    % CALCULO DAS PERDAS DE PONTENCIAS NAS LINHAS
    %----------------------------------------------------------------------

    Pperdas(j) = Pkm(j) + Pmk(j);
    Qperdas(j) = Qkm(j) + Qmk(j);
end

% Tempo gasto no c�lculo do fluxo de carga
tempo = toc;

%----------------------------------------------------------------------
% APRESENTA�AO DOS RESULTADOS
%----------------------------------------------------------------------

if (iter<itermax)
    disp('================================================================');
    disp(' RESULTADOS DO FLUXO DE CARGA NEWTON-RAPHSON DESACOPLADO R�PIDO ');
    disp('================================================================');
    fprintf('             Sistema com %g Barras e %g Ramos\n', nbar,nr)
    if nd ~= 0
        fprintf('    Modelado em N�vel de Se��o de Barra com %g Ramos Chaveados \n', nd)
    end
    disp('================================================================');
    fprintf('\n Tempo gasto no c�lculo do fluxo de carga %g s\n', tempo);
    fprintf('\n N�mero de itera��es no c�lculo do fluxo de carga: %g \n', iter-1);
    disp(' ')
    disp('================================================================');
    disp('              RESULTADOS DAS GRANDEZAS NAS BARRAS');
    disp('================================================================');
    fprintf('    Barra      Tipo     Tensao   �ngulo      P         Q\n')
    fprintf('                         (pu)    (graus)    (MW)     (Mvar)\n')
    vetor = [BARRAS tipo V teta*180/pi Pcalc*100 Qcalc*100];
    disp(vetor)
    disp(' ');
    disp('______________________________________________________________________');
    fprintf('      FLUXOS DE POTENCIA NOS RAMOS CONVENCIONAIS (MW e Mvar)\n')
    disp('______________________________________________________________________');
    fprintf('     De  Para     Pkm     Qkm       Pmk     Qmk         Perdas\n')
    fprintf('                 (MW)    (Mvar)    (MW)    (Mvar)    (MW)    (Mvar)\n')
    for j = 1:nl
        fprintf('%7d %4d %9.3f %7.3f %9.3f %7.3f %9.3f   %7.3f\n',na(j),nb(j),100*Pkm(j),100*Qkm(j),100*Pmk(j),100*Qmk(j),(100*Pperdas(j)),(100*Qperdas(j)))
    end

    if nd~=0
        disp(' ');
        disp('______________________________________________________________________');
        fprintf('      FLUXOS DE POTENCIA NOS DISPOSITIVOS CHAVEADOS (MW e Mvar)\n')
        disp('______________________________________________________________________');
        fprintf('     De  Para     Pkm     Qkm\n')
        fprintf('                 (MW)    (Mvar)\n')
        for j = nl+1:nr
            i = state(j);
            fprintf('%7d %4d %9.3f %7.3f\n',na(j),nb(j),100*t(i),100*u(i))
        end
    end
end