%**********************************************************************
%*                                                                    *
%*  UFPR - Universidade Federal do Parana                             *
%*                                                                    *
%*  Mestrado em Engenharia El�trica                                   *
%*                                                                    *
%*  Analise e Opera�ao de Sistemas Eletricos de Potencia              *
%*                                                                    *
%*  Dados de um Sistema para rodar o Algor�tmo de Calculo de Fluxo    *
%*  de Carga Pelo Metodo de Newton Raphson                            *
%*                                                                    *
%**********************************************************************

% ARQUIVO DE DADOS DO SISTEMA

%-----------------------------------------------------------------------------
%                               DADOS DAS BARRAS
%-----------------------------------------------------------------------------
%	      barra  tipo	tens�o	�ngulo	  Pd	    Qd     Pg  Qg  bshbar   med_p  med_q   med_V
%-----------------------------------------------------------------------------    
barra = newbarra;
%-----------------------------------------------------------------------------
%-------------------------------------------------------------
%                      DADOS DAS LINHAS
%-------------------------------------------------------------
%       na     nb       r           x       bshlin  med_t med_u 
%-------------------------------------------------------------            
linha = newlinha;
     
%----------------------
% Barras de refer�ncia
%----------------------
ref = [1];  

from  = linha(:,1)';
to    = linha(:,2)';
x     = linha(:,4)';
  
pd=barra(:,5);
pg=barra(:,7);

p=pg-pd;
p=p';


