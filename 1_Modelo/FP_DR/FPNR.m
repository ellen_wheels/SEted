%----------------------------------------------------------------------
% TESTE DA CONVERGENCIA FEITO A CADA ITERA�AO ANTES DE ENCOTRAR OS
% RESULTADOS FINAIS
%----------------------------------------------------------------------

% Verifica se o maior valor de DPQ (desbalan�o de potencia ativa e
% reativa) e maior que o limite de tolerancia especificado.
% Caso nao seja, passa para o calculo de fluxo de potencia nas linhas
% de transmissao e calculo de perda de potencia.
tic;
while maxDPQ>tol

    % Incrementa o n�mero de itera��es

    iter = iter+1;

    % Verifica se o numero de itera�oes nao ultrapassou o numero
    % maximo de itera�oes pre determinado, caso ultrapasse encerra
    % a aplica�ao.

    if iter >= itermax
        disp('     NAO CONVERGIU     ');
        break
    end

    %----------------------------------------------------------------------
    %CALCULO DAS POTENCIAS ATIVAS (Pcalc) E REATIVAS (Qcalc) NAS BARRAS:
    %----------------------------------------------------------------------
    Pcalc = zeros(nbar,1);
    Qcalc = zeros(nbar,1);

    for j = 1:nl
        k = na(j);
        m = nb(j);
        tetakm = teta(k) - teta(m);
        Pcalc(k) = Pcalc(k)-V(k)*V(k)*G(k,m) + V(k)*V(m)*(G(k,m)*cos(tetakm)+B(k,m)*sin(tetakm));
        Pcalc(m) = Pcalc(m)-V(m)*V(m)*G(k,m) + V(k)*V(m)*(G(k,m)*cos(tetakm)-B(k,m)*sin(tetakm));
        Qcalc(k) = Qcalc(k)+V(k)*V(k)*(B(k,m)-bshlin(j)/2) - V(k)*V(m)*(B(k,m)*cos(tetakm)-G(k,m)*sin(tetakm));
        Qcalc(m) = Qcalc(m)+V(m)*V(m)*(B(k,m)-bshlin(j)/2) - V(k)*V(m)*(B(k,m)*cos(tetakm)+G(k,m)*sin(tetakm));
    end
    for j = (nl+1):nr
        k = na(j);
        m = nb(j);
        l = state(j);
        Pcalc(k) = Pcalc(k)+t(l);
        Pcalc(m) = Pcalc(m)-t(l);
        Qcalc(k) = Qcalc(k)+u(l);
        Qcalc(m) = Qcalc(m)-u(l);
    end

    %----------------------------------------------------------------------
    % CALCULO DOS DESVIOS DE POTENCIA ATIVA (DP) e REATIVA (DQ)
    %----------------------------------------------------------------------
    DP = Pesp-Pcalc;
    DQ = Qesp-Qcalc;

    DPQ = [DP;DQ];

    % Armazena os valores do DP das barras tipo PQ e PV  em DPQ

    kk=0;
    for k=1:nbar
        if tipo(k)~=2
            kk=kk+1;
            DPQr(kk,1) = DP(k);
        end
    end

    % Armazena os valores do DQ das barras tipo PQ em DPQ
    for k=1:nbar
        if tipo(k)==0
            kk=kk+1;
            DPQr(kk,1)=DQ(k);
        end
    end

    %----------------------------------------------------------------------
    % RESTRICOES OPERACIONAIS
    %----------------------------------------------------------------------
    dt=zeros(ndf,1);
    dv=dt;
    dtt=zeros(nda,1);
    dtu=dtt;
    for j=1:nd
        jj=j+nl;
        k=na(jj);
        m=nb(jj);
        if status(j)==1
            dt(j)=teta(k)-teta(m);
            dv(j)=V(k)-V(m);
        elseif status(j)==0
            dtt=t(j);
            dtu=u(j);
        end
    end
    Desvio=[DPQr;dt;dv;dtt;dtu];

    % Armazena o valor maximo existente no vetor de desvios

    maxDPQ=max(abs(Desvio));

    %----------------------------------------------------------------------
    %FORMA�AO DA MATRIZ JACOBIANA
    %----------------------------------------------------------------------

    %----------------------------------------------------------------------
    %Forma�ao de H:

    H=zeros(nbar);

    % Calculo dos elementos diagonais de H:

    for j=1:nl

        k=na(j);
        m=nb(j);

        t1=teta(k)-teta(m);
        H(k,k)=H(k,k)+V(k)*V(m)*(-G(k,m)*sin(t1)+B(k,m)*cos(t1));

        t2=teta(m)-teta(k);
        H(m,m)=H(m,m)+V(m)*V(k)*(-G(m,k)*sin(t2)+B(m,k)*cos(t2));
    end

    %Calculo dos elementos fora da diagonal de H:

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        H(k,m)=V(k)*V(m)*(G(k,m)*sin(t1)-B(k,m)*cos(t1));
        H(m,k)=V(m)*V(k)*(G(m,k)*sin(t2)-B(m,k)*cos(t2));
    end

    %----------------------------------------------------------------------
    % Forma�ao de N:

    N=zeros(nbar);

    % Calculo dos Elementos da Diagonais de N:

    for k=1:nbar
        N(k,k)=2*V(k)*G(k,k);
    end

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        N(k,k)=N(k,k)+V(m)*(G(k,m)*cos(t1)+B(k,m)*sin(t1));
        N(m,m)=N(m,m)+V(k)*(G(m,k)*cos(t2)+B(m,k)*sin(t2));
    end

    %Calculo dos Elementos fora da Diagonal de N:

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        N(k,m)=V(k)*(G(k,m)*cos(t1)+B(k,m)*sin(t1));
        N(m,k)=V(m)*(G(m,k)*cos(t2)+B(m,k)*sin(t2));
    end

    %----------------------------------------------------------------------
    % Forma�ao de M:

    M=zeros(nbar);

    %Calculo dos Elementos da Diagonais de M:

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        M(k,k)=M(k,k)+V(k)*V(m)*(G(k,m)*cos(t1)+B(k,m)*sin(t1));
        M(m,m)=M(m,m)+V(m)*V(k)*(G(m,k)*cos(t2)+B(m,k)*sin(t2));
    end

    %Calculo dos Elementos fora da Diagonal de M:

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        M(k,m)=-V(k)*V(m)*(G(k,m)*cos(t1)+B(k,m)*sin(t1));
        M(m,k)=-V(m)*V(k)*(G(m,k)*cos(t2)+B(m,k)*sin(t2));
    end

    %----------------------------------------------------------------------
    %Forma�ao de L:

    % Forma uma matriz quadrada nula de dimensao "nbar" (numero de barras)

    L=zeros(nbar);

    % Calculo dos elementos da diagonal de L:

    for k=1:nbar
        L(k,k)=-2*V(k)*B(k,k);
    end
    for i=1:nbar
        for j=1:nl
            k=na(j);
            m=nb(j);
            if k==i
                t1=teta(k)-teta(m);
                L(k,k)=L(k,k)+V(m)*(G(k,m)*sin(t1)-B(k,m)*cos(t1));
            end
            if m==i
                t2=teta(m)-teta(k);
                L(m,m)=L(m,m)+V(k)*(G(m,k)*sin(t2)-B(m,k)*cos(t2));
            end
        end
    end

    %Calculo dos elementos fora da diagonal de L:

    for i=1:nl
        k=na(i);
        m=nb(i);
        t1=teta(k)-teta(m);
        t2=teta(m)-teta(k);
        L(k,m)=V(k)*(G(k,m)*sin(t1)-B(k,m)*cos(t1));
        L(m,k)=V(m)*(G(m,k)*sin(t2)-B(m,k)*cos(t2));
    end

    % Inclusao das restri��es das barras de referencia em H e L e das barras PV em L
    for j=1:nbar
        if (tipo(j)==2)
            H(j,j)=eps;
            L(j,j)=eps;
        end
        if (tipo(j)==1)
            L(j,j)=eps;
        end
    end
    nref=max(size(ref));  % Referencias surgidas pela modelagem no nivel de secao de barra
    for j=1:nref
        k=ref(j);
        H(k,k)=eps;
        L(k,k)=eps;
    end

    %----------------------------------------------------------------------
    % Submatrizes para fluxo n�vel SE
    %----------------------------------------------------------------------
    if nd~=0
        jf=0;
        ja=0;
        JT=zeros(nbar,nd);
        JU=JT;
        JDT=zeros(ndf,nbar);
        JDV=JDT;
        JTo=zeros(nda,nd);
        JUo=JTo;
        for i=nl+1:nr
            k=na(i);
            m=nb(i);
            j=state(i);
            % Parcela do fluxo no disjuntor na inje��o ativa das barras vizinhas
            JT(k,j)=1;
            JT(m,j)=-1;
            % Parcela do fluxo no disjuntor na inje��o ativa das barras vizinhas
            JU(k,j)=1;
            JU(m,j)=-1;
            % Restri��es operacionais para disjuntores fechados
            if status(j)==1
                jf=jf+1;
                JDT(jf,k)=1;
                JDT(jf,m)=-1;
                JDV(jf,k)=1;
                JDV(jf,m)=-1;
            end
            % Restri��es operacionais para disjuntores abertos
            if status(j)==0
                ja=ja+1;
                JTo(ja,j)=1;
                JUo(ja,j)=1;
            end
        end
        % Matrizes nulas complementares
        zu1=zeros(nbar,nd);
        zt1=zu1;
        zdt=zeros(ndf,nbar+2*nd);
        zdv1=zeros(ndf,nbar);
        zdv2=zeros(ndf,2*nd);
        zto1=zeros(nda,2*nbar);
        zto2=zeros(nda,nd);
        zuo=zeros(nda,2*nbar+nd);
    end

    %----------------------------------------------------------------------
    % MATRIZ JACOBIANA
    %----------------------------------------------------------------------

    if nd==0 % Fluxo convencional
        Jacob=[H N;M L];
        ns=2*nbar; % numero de estados para fluxo convencional
        b=DPQ; % vetor do lado direito para fluxo convenicional
    else % Fluxo no nivel de subestacao
        Jacob=[ H N JT zu1;
            M L zt1 JU;
            JDT zdt;
            zdv1 JDV zdv2;
            zto1 JTo zto2;
            zuo,JUo];
        ns=2*nbar+2*nd; % numero de estados
        b=[DPQ;zeros(2*nd,1)]; % vetor do lado direito
    end

    %     %----------------------------------------------------------------------
    %     % Determina��o das vari�veis de estado por fatoracao LU(P)
    %     %----------------------------------------------------------------------
    %
    %     % Substituicao Direta
    %
    %     [Low,Up,PP] = lu(Jacob);
    %     b=PP*b;
    %     for i=1:ns
    %         aux=0;
    %         for j=1:i-1
    %             aux=aux+Low(i,j)*Dx_linha(j);
    %         end
    %         Dx_linha(i)=b(i)-aux;
    %     end
    %
    %     % Substituicao Inversa
    %
    %     for i=ns:-1:1
    %         aux=0;
    %         for j=ns:-1:i+1
    %             aux=aux+(Up(i,j)*Dx(j))/Up(i,i);
    %         end
    %         Dx(i)=Dx_linha(i)/Up(i,i)-aux;
    %     end
    Dx = inv(Jacob)* b;
    %----------------------------------------------------------------------
    % Atualiza��o dos valores de tensao "V" e dos angulos "teta".
    for i=1:nbar
        teta(i)=teta(i)+Dx(i);
        V(i)=V(i)+Dx(nbar+i);
    end
    for i=1:nd
        t(i)=t(i)+Dx(2*nbar+i);
        u(i)=u(i)+Dx(2*nbar+nd+i);
    end
end
