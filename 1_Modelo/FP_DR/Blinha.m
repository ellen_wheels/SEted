% =========================================================================
% ROTINA RESPONS�VEL PELA FORMA��O DE Blinha NO CASO DE UM BANCO DE DADOS
% BARRA-RAMO OU Blinha_ext NO CASO DE UM SISTEMA QUE APRESENTE RAMOS
% CHAVEADOS
% =========================================================================
if nd == 0

    Bl_ext = zeros(nbar);
    for l = 1:nr
        i = na(l);
        j = nb(l);
        Bl_ext(i,i) = Bl_ext(i,i) + 1/x(l);
        Bl_ext(j,j) = Bl_ext(j,j) + 1/x(l);
        Bl_ext(i,j) = Bl_ext(i,j) - 1/x(l);
        Bl_ext(j,i) = Bl_ext(j,i) - 1/x(l);
    end

else
    
    Bl_ext = zeros(nbar+nd);
    s = 1;
    for l = 1:nr
        i = na(l);
        j = nb(l);
        if (x(l) ~= 0.0 & x(l) ~= 9999)
            Bl_ext(i,i) = Bl_ext(i,i) + 1/x(l);
            Bl_ext(j,j) = Bl_ext(j,j) + 1/x(l);
            Bl_ext(i,j) = Bl_ext(i,j) - 1/x(l);
            Bl_ext(j,i) = Bl_ext(j,i) - 1/x(l);
        elseif x(l) == 0.0
            Bl_ext(i,nbar+s) = 1;
            Bl_ext(j,nbar+s) = -1;
            Bl_ext(nbar+s,i) = 1;
            Bl_ext(nbar+s,j) = -1;
            s = s + 1;
        elseif x(l) == 9999.
            Bl_ext(i,nbar+s) = 1;
            Bl_ext(j,nbar+s) = -1;
            Bl_ext(nbar+s,nbar+s) = 1;
            s = s + 1;
        end
    end

end


% % Caso particular
% if num_df==num_disj
%     B(n+dref,:)=0;
%     B(n+dref,n+dref)=1;
% end

%%Definicao das referencias, zerando a linha e coluna e colocando 'um' na diagonal%%
for l = 1:nbar
    if (tipo(l) == 2)
        Bl_ext(l,l) = eps;
    end
end

if nd == 0 % Fluxo convencional
    ns = nbar; % numero de estados para fluxo convencional
    b = DP./V; % vetor do lado direito para fluxo convenicional
else % Fluxo no nivel de subestacao
    ns = nbar + nd; % numero de estados
    b = [DP./V;zeros(nd,1)]; % vetor do lado direito
end
