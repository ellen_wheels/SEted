%----------------------------------------------------------------------
% TESTE DA CONVERGENCIA FEITO A CADA ITERA�AO ANTES DE ENCOTRAR OS
% RESULTADOS FINAIS
%----------------------------------------------------------------------

% Verifica se o maior valor de DPQ (desbalan�o de potencia ativa e
% reativa) e maior que o limite de tolerancia especificado.
% Caso nao seja, passa para o calculo de fluxo de potencia nas linhas
% de transmissao e calculo de perda de potencia.
tic;

while maxDP>tol

    % Incrementa o n�mero de itera��es

    iter=iter+1;

    % Verifica se o numero de itera�oes nao ultrapassou o numero
    % maximo de itera�oes pre determinado, caso ultrapasse encerra
    % a aplica�ao.

    if iter >= itermax
        disp('     NAO CONVERGIU     ');
        break
    end

    %----------------------------------------------------------------------
    %CALCULO DAS POTENCIAS ATIVAS (Pcalc)
    %----------------------------------------------------------------------
    Pcalc = zeros(nbar,1);

    for j = 1:nl
        k = na(j);
        m = nb(j);
        tetakm = teta(k) - teta(m);
        Pcalc(k) = Pcalc(k)-V(k)*V(k)*G(k,m) + V(k)*V(m)*(G(k,m)*cos(tetakm)+B(k,m)*sin(tetakm));
        Pcalc(m) = Pcalc(m)-V(m)*V(m)*G(k,m) + V(m)*V(k)*(G(k,m)*cos(tetakm)-B(k,m)*sin(tetakm));
    end
    for j = nl+1:nr
        k = na(j);
        m = nb(j);
        l = state(j);
        Pcalc(k) = Pcalc(k) + t(l);
        Pcalc(m) = Pcalc(m) - t(l);
    end

    %----------------------------------------------------------------------
    % CALCULO DOS DESVIOS DE POTENCIA ATIVA (DP) e REATIVA (DQ)
    %----------------------------------------------------------------------
    DP = Pesp - Pcalc;

    % Armazena os valores do DP das barras tipo PQ e PV  em DPQ

    kk=0;
    for k=1:nbar
        if tipo(k)~=2
            kk=kk+1;
            DPr(kk,1) = DP(k);
        end
    end

    %----------------------------------------------------------------------
    % RESTRICOES OPERACIONAIS
    %----------------------------------------------------------------------
    dt = zeros(ndf,1);
    dtt = zeros(nda,1);
    for j = 1:nd
        jj = j + nl;
        k = na(jj);
        m = nb(jj);
        if status(j) == 1
            dt(j) = teta(k) - teta(m);
        elseif status(j) == 0
            dtt = t(j);
        end
    end
    Desvio1 = [DPr;dt;dtt];

    % Armazena o valor maximo existente no vetor de desvios

    maxDP = max(abs(Desvio1));
    
    Blinha   % Chama a rotina de forma��o da matriz Blinha

    %----------------------------------------------------------------------
    % Determina��o das vari�veis de estado utilizando inversa da Blinha
    %----------------------------------------------------------------------

    Dxa = inv(Bl_ext)* b;

    %----------------------------------------------------------------------
    % Atualiza��o dos valores dos angulos "teta".
    for i = 1:nbar
        teta(i) = teta(i) + Dxa(i);
    end
    for i = 1:nd
        t(i) = t(i)+Dxa(nbar+i);
    end
    
    
%     C�LCULO DOS Q
    Qcalc=zeros(nbar,1);

    for j = 1:nl
        k = na(j);
        m = nb(j);
        tetakm = teta(k) - teta(m);
        Qcalc(k) = Qcalc(k) + V(k)*V(k)*(B(k,m)-bshlin(j)) - V(k)*V(m)*(B(k,m)*cos(tetakm)-G(k,m)*sin(tetakm));
        Qcalc(m) = Qcalc(m) + V(m)*V(m)*(B(k,m)-bshlin(j)) - V(m)*V(k)*(B(k,m)*cos(tetakm)+G(k,m)*sin(tetakm));
    end
    for j = nl+1:nr
        k = na(j);
        m = nb(j);
        l = state(j);
        Qcalc(k) = Qcalc(k) + u(l);
        Qcalc(m) = Qcalc(m) - u(l);
    end

    %----------------------------------------------------------------------
    % CALCULO DOS DESVIOS DE POTENCIA REATIVA (DQ)
    %----------------------------------------------------------------------
    DQ = Qesp - Qcalc;

    % Armazena os valores do DQ das barras tipo PQ em DQ
    kk = 0;
    for k=1:nbar
        if tipo(k) == 0
            kk=kk+1;
            DQr(kk,1) = DQ(k);
        end
    end

    %----------------------------------------------------------------------
    % RESTRICOES OPERACIONAIS
    %----------------------------------------------------------------------
    dv=zeros(ndf,1);
    dtu=zeros(nda,1);
    for j=1:nd
        jj=j+nl;
        k=na(jj);
        m=nb(jj);
        if status(j)==1
            dv(j)=V(k)-V(m);
        elseif status(j)==0
            dtu=u(j);
        end
    end
    Desvio2 = [DQr;dv;dtu];

    % Armazena o valor maximo existente no vetor de desvios

    maxDQ = max(abs(Desvio2));
    
    if maxDQ >= tol
        B2linha   % Chama a rotina de forma��o da matriz B2linha

        %----------------------------------------------------------------------
        % Determina��o das vari�veis de estado utilizando inversa da B2linha
        %----------------------------------------------------------------------

        Dxr = inv(B2l_ext)* bb;

        %----------------------------------------------------------------------
        % Atualiza��o dos valores de tensao "V" .
        for i = 1:nbar
            V(i) = V(i) + Dxr(i);
        end
        for i = 1:nd
            u(i) = u(i) + Dxr(nbar+i);
        end
    end
end