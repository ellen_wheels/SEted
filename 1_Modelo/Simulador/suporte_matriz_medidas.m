function [Pkm, Qkm, Pcalc, Qcalc, V, teta] = suporte_matriz_medidas(angbase, nl, nbar, Z0)
%% Monta matrizes necessárias para a matriz Medidas

%Pkma, Pkmb, Pkmc
Pkm=zeros(nl,1);

Qkm=zeros(nl,1);

Pcalc=zeros(nbar,1);

Qcalc=zeros(nbar,1);

V=zeros(nbar,1);

teta=zeros(nbar,1);

for i=1:nl
    Pkm(i)=Z0(i,1)*cos(angbase)-Z0(2*nl+i,1)*sin(angbase);
       
    Qkm(i)=Z0(2*nl+i,1)*cos(angbase)+Z0(i,1)*sin(angbase);
end

for i=1:nbar
    Pcalc(i)=Z0(4*nl+i,1)*cos(angbase)-Z0(4*nl+nbar+i,1)*sin(angbase);
       
    Qcalc(i)=Z0(4*nl+nbar+i,1)*cos(angbase)+Z0(4*nl+i,1)*sin(angbase);
    
    V(i)=Z0(4*nl+2*nbar+i,1);
    
    teta(i)=Z0(4*nl+3*nbar+i,1);   
end
end
    