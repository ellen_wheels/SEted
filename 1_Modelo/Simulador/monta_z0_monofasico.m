%% Transforma resultados de sa�da Pkm, Pmk, Qkm, Qmk em vetor

itkm=zeros(nl,1);
itmk=zeros(nl,1);
iukm=zeros(nl,1);
iumk=zeros(nl,1);

for i=1:nl
    itkm(i,1)=Pkm(i);
    itmk(i,1)=Pmk(i);
    iukm(i,1)=Qkm(i);
    iumk(i,1)=Qmk(i);
end
%% Monta  vetor de medidas Z0t e Z0u 

Z0t=zeros(2*nl,1);
Z0u=zeros(2*nl,1);

    for i=1:nl
    Z0t(i,1)=itkm(i,1);
    Z0t(i+nl,1)=itmk(i,1);
    Z0u(i,1)=iukm(i,1);
    Z0u(i+nl,1)=iumk(i,1);
    end
%% Transforma Pk, Qk, V e theta em vetor e monta vetor de medidas Z0p, Z0q, Z0v e Z0theta

Z0p=zeros(nbar,1);
Z0q=zeros(nbar,1);
Z0v=zeros(nbar,1);
Z0teta=zeros(nbar,1);

for i=1:nbar
    Z0p(i,1)=Pcalc(i,1);
    Z0q(i,1)=Qcalc(i,1);
    Z0v(i,1)=V_medida(i,1);
    Z0teta(i,1)=teta_medida(i,1);
end

%% Monta vetor de medidas Z0
Z0=[Z0t;Z0u;Z0p;Z0q;Z0v;Z0teta];

Z0original=Z0;

