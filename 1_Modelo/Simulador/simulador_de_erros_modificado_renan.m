%% C�lculo fe para medidas de pot�ncia (fluxo e inje��o) 

Z0potencia=[Z0t;Z0u;Z0p;Z0q];
fepotencia=zeros(size(Z0potencia,1));
sinalpotencia=zeros(size(Z0potencia,1));

if nbar<=14
    for i=1:(4*nl+2*nbar)
        for p=1:1
            
            if Z0potencia(i,p)>=0 %matriz que vai garantir que o sinal do fluxo volte a ser o mesmo depois
                sinalpotencia(i,p)=1;
            else
                sinalpotencia(i,p)=-1;
            end
            
            if (abs(Z0potencia(i,p)))>=2.5 %tabela de valores de fe para diferentes valores de fluxos e inje��es
                fepotencia(i,p)=2.5;
            else                              
                if abs(Z0potencia(i,p))>=2.0
                    fepotencia(i,p)=2.5;
                else                    
                    if abs(Z0potencia(i,p))>=1.0
                        fepotencia(i,p)=2.0;
                    else                        
                        if abs(Z0potencia(i,p))>=0.65
                            fepotencia(i,p)=1.0;
                        else                            
                            if abs(Z0potencia(i,p))>=0.35
                                fepotencia(i,p)=0.65;
                            else                                
                                if abs(Z0potencia(i,p))>=0.2
                                    fepotencia(i,p)=0.35;
                                else                                    
                                    if abs(Z0potencia(i,p))>=0.1
                                        fepotencia(i,p)=0.2;
                                    else                                        
                                        if abs(Z0potencia(i,p))>=0.05
                                            fepotencia(i,p)=0.1;
                                        else
                                            fepotencia(i,p)=0.05;
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
else

%%%%%% COM MODIFICA��ES FEITAS POR RENAN
    for i=1:(4*nl+2*nbar)
        for p=1:1
            
            if Z0potencia(i,p)>0 %matriz que vai garantir que o sinal do fluxo volte a ser o mesmo depois
                sinalpotencia(i,p)=1;
            else
                sinalpotencia(i,p)=-1;
            end
            
            if (abs(Z0potencia(i,p)))>=2.8 %tabela de valores de fe para diferentes valores de fluxos e inje��es
                fepotencia(i,p)=3;
            elseif abs(Z0potencia(i,p))>=2.0
                fepotencia(i,p)=2.8;
            elseif abs(Z0potencia(i,p))>=1.8
                fepotencia(i,p)=2.0;
            elseif abs(Z0potencia(i,p))>=1.0
                fepotencia(i,p)=1.8;
            elseif abs(Z0potencia(i,p))>=0.5
                fepotencia(i,p)=1.0;
            elseif abs(Z0potencia(i,p))>=0.2
                fepotencia(i,p)=0.5;
            elseif abs(Z0potencia(i,p))>=0.1
                fepotencia(i,p)=0.2;
            elseif abs(Z0potencia(i,p))>=0.08                                       
                fepotencia(i,p)=0.1;
            elseif abs(Z0potencia(i,p))>=0.05
                fepotencia(i,p)=0.08;
            elseif abs(Z0potencia(i,p))>=0.02
                fepotencia(i,p)=0.05;
            elseif abs(Z0potencia(i,p))>=0.01
                fepotencia(i,p)=0.02;
            elseif abs(Z0potencia(i,p))>=0.008
                fepotencia(i,p)=0.01;
            elseif abs(Z0potencia(i,p))>=0.005
                fepotencia(i,p)=0.008;
            elseif abs(Z0potencia(i,p))>=0.002
                fepotencia(i,p)=0.005;
            elseif abs(Z0potencia(i,p))>=0.001
                fepotencia(i,p)=0.002;
            elseif abs(Z0potencia(i,p))>=0.0008
                fepotencia(i,p)=0.001;
            elseif abs(Z0potencia(i,p))>=0.0005
                fepotencia(i,p)=0.0008;
            elseif abs(Z0potencia(i,p))>=0.0002
                fepotencia(i,p)=0.0005;
            elseif abs(Z0potencia(i,p))>=0.0001
                fepotencia(i,p)=0.0002;
            else
                fepotencia(i,p)=0.0001;
            end
        end
        
    end
  
%%%%% ORIGINAL FEITO PELA CINTHIA
%         for i=1:(4*nl+2*nbar)
%         for p=1:1
%             
%             if Z0potencia(i,p)>0 %matriz que vai garantir que o sinal do fluxo volte a ser o mesmo depois
%                 sinalpotencia(i,p)=1;
%             else
%                 sinalpotencia(i,p)=-1;
%             end
%             
%             if (abs(Z0potencia(i,p)))>=2.8 %tabela de valores de fe para diferentes valores de fluxos e inje��es
%                 fepotencia(i,p)=2.8;
%             else                              
%                 if abs(Z0potencia(i,p))>=2.0
%                     fepotencia(i,p)=2.8;
%                 else                    
%                     if abs(Z0potencia(i,p))>=1.8
%                         fepotencia(i,p)=2.0;
%                     else                        
%                         if abs(Z0potencia(i,p))>=1.0
%                             fepotencia(i,p)=1.8;
%                         else                            
%                             if abs(Z0potencia(i,p))>=0.5
%                                 fepotencia(i,p)=1.0;
%                             else                                
%                                 if abs(Z0potencia(i,p))>=0.2
%                                     fepotencia(i,p)=0.5;
%                                 else                                    
%                                     if abs(Z0potencia(i,p))>=0.1
%                                         fepotencia(i,p)=0.2;
%                                     else                                        
%                                         fepotencia(i,p)=0.1;
%                                     end
%                                 end
%                             end
%                         end
%                     end
%                 end
%             end
%         end
%     end
end
                                
%% Inclus�o de erros nas medidas de pot�ncia

Z0medidopotencia=size(Z0potencia,1);

for i=1:(4*nl+2*nbar) %% c�lcula Z0medido (inclui os erros) para medidas de potencia
    for p=1:1
        Z0medidopotencia(i,p)=sinalpotencia(i,p)*(abs(Z0potencia(i,p))+0.02*fepotencia(i,p)*randn(1));
    end
end

%% Inclus�o de erros nas medidas magnitude de tens�o

Z0medidov=size(Z0v,1);

for i=1:nbar
    for p=1:1
        Z0medidov(i,p)=abs(Z0v(i,p))+0.02*1.1*randn(1);
    end
end

 %% Monta vetor de medidas com inclus�o de erros aleat�rios

Z0=[Z0medidopotencia;Z0medidov;Z0teta];

%% 
relacaoZ0=zeros(size(Z0,1));                
for i=1:(4*nl+4*nbar)
    for p=1:1
        relacaoZ0(i,p)=100*(Z0original(i,p)-Z0(i,p))/Z0(i,p);
    end
end   

