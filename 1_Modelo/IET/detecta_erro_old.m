%% 

% 21/08/17 - Detectar erro de topologia via m�ximo res�duo normalizado
% 17/10/17 - Altera��o para quando o omega for zero!

%% Calculando o res�duo

[ haa ] = monta_hzinhoaa_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);
[ hrr ] = monta_hzinhorr_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);

r_ativo = zA-haa;
r_reativo = zR-hrr;

%% Calculando a ra�z quadrada de omega

omegaA = RA - (HAA*inv(GAA)*HAA');
omegaA_sqrt = sqrt(diag(omegaA));

omegaR = RR - (HRR*inv(GRR)*HRR');
omegaR_sqrt = sqrt(diag(omegaR));

%% Calculando o res�duo normalizado

for contAt = 1:size(MedAt,1)
    if omegaA_sqrt(contAt,1) == 0
        r_ativoN(contAt,1) = 0;
    else
        r_ativoN(contAt,1) = r_ativo(contAt,1)/omegaA_sqrt(contAt,1);
    end
end
resnA = [MedAt(:,1) MedAt(:,3:5) r_ativoN];

for contReat = 1:size(MedReat,1)
    if omegaR_sqrt(contReat,1) == 0
        r_reativoN(contReat,1) = 0;
    else
        r_reativoN(contReat,1) = r_reativo(contReat,1)/omegaR_sqrt(contReat,1);
    end
end
resnR = [MedReat(:,1) MedReat(:,3:5) r_reativoN];

%% Detec��o de erro

r_ativoNmax = max(abs(r_ativoN));
r_reativoNmax = max(abs(r_reativoN));

for n = 1:size(r_ativoN,1)
    if abs(r_ativoN(n,1))== r_ativoNmax
        resnAmax = resnA(n,:);
    end
end

for n = 1:size(r_reativoN,1)
    if abs(r_reativoN(n,1))== r_reativoNmax
        resnRmax = resnR(n,:);
    end
end

