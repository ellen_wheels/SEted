%% 

% 28/10/17 - Inserida rotina para conjunto suspeito

%% Informa��es suspeitas

nA = 0;
r_ativoS = [];
conjSA = [];
omegaA_S = 0;
omegaA_SS = 0;
rtol = 3.0;
if r_ativoNmax > rtol %verificando a presen�a de erro
    for cont = 1:size(r_ativoN,1)
        if abs(r_ativoN(cont,1)) > rtol
            nA = nA+1;
            conjSA(nA,:) = resnA(cont,:); %conjunto suspeito
            r_ativoS(nA,1) = r_ativo(cont,1); %montando rS
            identR_A(nA,1) = cont;
%             omegaA_S(nA,1) = omegaA(cont,cont); %montando omegaS
        end
    end
%         identR_A = [15];
omegaA_SS = inv(RA(identR_A,identR_A))*omegaA(identR_A,identR_A)*inv(RA(identR_A,identR_A));
%  omegaA_SS = omegaA(identR_A,identR_A);
end
% 
nR = 0;
r_reativoS = [];
conjSR = [];
omegaR_S = 0;
omegaR_SS = 0;
if r_reativoNmax > rtol
    for cont = 1:size(r_reativoN,1)
        if abs(r_reativoN(cont,1)) > rtol
            nR = nR+1;
            conjSR(nR,:) = resnR(cont,:); %conjunto suspeito
            r_reativoS(nR,1) = r_reativo(cont,1); %montando rS
            identR_R(nR,1) = cont;
%             omegaR_S(nR,1) = omegaR(cont,cont); %montando omegaS
        end
    end
%     identR_R = [43; 45];
omegaR_SS = inv(RR(identR_R,identR_R))*omegaR(identR_R,identR_R)*inv(RR(identR_R,identR_R));
%  omegaR_SS = omegaR(identR_R,identR_R);
end
%% 29/11/17 reuni�o TOPOLOGIA
% identR_A = 111;
% identR_R = 148;
% 
% identR = [111; 
%          148];

% 
% % omega_SS = [omegaA_SS;
% 
% r_ativoS = r_ativo(identR_A,1);
% r_reativoS = r_reativo(identR_R,1);
    
%% 29/11/17 reuni�o ERRO EM MEDIDA
%  identR_A = 20;
% identR_R = 45;
% % % 
% % identR = [111; 
% %          148];


% % omega_SS = [omegaA_SS;
% 
if isempty(r_ativoS) == 0
    r_ativoS = r_ativo(identR_A,1);
end

if isempty(r_reativoS) == 0
    r_reativoS = r_reativo(identR_R,1);
end
    
%% Verifica se todas as informa��es err�neas est�o selecionadas como suspeitas

ecos = 0.1;

cosphiA = [];
nA=0;

%   
if isempty(conjSA) == 0
    num = r_ativoS'*inv(omegaA_SS)*r_ativoS;
    den = r_ativo'*RA*r_ativo;
    cosphiA = sqrt(num/den);
end

cosphiR = [];
if isempty(conjSR) == 0
    num = r_reativoS'*inv(omegaR_SS)*r_reativoS;
    den = r_reativo'*RR*r_reativo;
    cosphiR = sqrt(num/den);
%     if cosphiR < (1-ecos)
%     %reduzir limiar
%     end
end

%% Processamento de erros simult�neos (grosseiros em medidas e topologia)

%     if cosphiA > (1-ecos)
% %         for aux = 1:size(conjSA,1)
% %             nA = nA+1;
%             nA = 2;
%             conjSA(nA,:) = [];
%             r_ativoS(nA,:) = []; %montando rS
%             identR_A(nA,:) = [];
%             omegaA_SS = inv(RA(identR_A,identR_A))*omegaA(identR_A,identR_A)*inv(RA(identR_A,identR_A));
%             num = r_ativoS'*inv(omegaA_SS)*r_ativoS;
%             den = r_ativo'*RA*r_ativo;
%             cosphiA = sqrt(num/den);
% %         end
%      end



