%% 

% 28/10/17 - Inserida rotina para conjunto suspeito

%% Informa��es suspeitas

% n = 0;
% r_S = 0;
% conjS = [];
% omega_S = 0;
% omega_SS = 0;
% identR = 0;
% rtol = 3.0;
% if r_Nmax > rtol %verificando a presen�a de erro
%     for cont = 1:size(r_N,1)
%         if abs(r_N(cont,1)) > rtol
%             n = n+1;
%             conjS(n,:) = resn(cont,:); %conjunto suspeito
% %             r_S(n,1) = r(cont,1); %montando rS
%             identR(n,1) = cont;
% %             omega_S(n,1) = omega(cont,:); %montando omegaS
%         end
%     end 
%     omega_S = omega(:,identR); %montando omegaS
% % omega_SS = omega(identR,identR);
% % omega_SS =inv(R(identR,identR))*omega_SS*inv(R(identR,identR));
% end
% % 

rtol = 3.0;
ecos = 0.05;

auxA_m = 0;
auxR_m = 0;
auxA_op = 0;
auxR_op = 0;


%%     conjunto suspeito ativo para topologia
nA_op = 0;
r_ativoS_op = [];
conjSA_op = [];
omegaA_S_op = 0;
omegaA_SS_t = 0;
identR_A_op = [];
if r_ativoNmax_op > rtol %verificando a presen�a de erro
disp('ANOMALIA DETECTADA - Pseudomedida Ativa')
    identR_A_op =find(abs(resnA_op(:,6))>rtol)
%     resnA_crit = [abs(resnA_op(identR_A_op,:)) (1:size(identR_A_op,1))']
    disp('ELIMINE AS MEDIDAS CRITICAS!!! - VETOR identR_A_op')
%     pause
%     conjSA_op = resnA_op(identR_A_op,:)
    omegaA_S_op = omegaA_op(:,identR_A_op); %montando omegaS
%     r_ativoS_op = r_ativo(identR_A_op,1); %IDENT CORRESPONDENTE AO VETOR COMPLETO
end

% % if isempty(conjSA_op) == 0
if isempty(identR_A_op) == 0

    RA_op = RA(at_m+1:at_m+at_op,at_m+1:at_m+at_op);
    
    numA_op = (r_ativo_op'*RA_op*omegaA_S_op)*inv(omegaA_S_op'*RA_op*omegaA_S_op)*(omegaA_S_op'*RA_op*r_ativo_op);
    denA_op = (r_ativo_op'*RA_op*r_ativo_op);

    cosphiA_op = sqrt(numA_op/denA_op)
    
                    disp('====================================================');
                    disp(' TESTE DO COSSENO (restri��es operacionais (ativa))   ') 
                    disp('====================================================');
    while cosphiA_op >= (1-ecos)

            conjSA_op = resnA_op(identR_A_op,:); % conjunto suspeito

            RA_op = RA(at_m+1:at_m+at_op,at_m+1:at_m+at_op);

            numA_op = (r_ativo_op'*RA_op*omegaA_S_op)*inv(omegaA_S_op'*RA_op*omegaA_S_op)*(omegaA_S_op'*RA_op*r_ativo_op);
            denA_op = (r_ativo_op'*RA_op*r_ativo_op);

            cosphiA_op = sqrt(numA_op/denA_op);
           

                        disp('Conjunto Suspeito ')
                        disp('   TIPO   ID   BARRA   DE  PARA    rN') 
                        for n = 1:size(conjSA_op,1)
                        fprintf('    %d    %d   %d     %d   %d    %g         \n', conjSA_op(n,1:5),conjSA_op(n,6));
                        end
                        fprintf('\n');
                        disp('   cosPHI (pseudomedidas ativas)') 
                        fprintf('    %g          \n', cosphiA_op);
    %     
        if cosphiA_op >= (1-ecos)

                    if isempty(identR_A_op) == 0 % verificando se o conjunto suspeito n�o est� vazio
                            auxA_op = auxA_op +1;
                            identR_A_op(auxA_op,:) = [];
                            omegaA_S_op(:,auxA_op) = [];

                    else % conjunto suspeito est� vazio
                        fprintf('\n');
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                        disp('  N�o apresenta erro em restri��es operacionais (ativa) ') 
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                    end
        else 
                 if isempty(identR_A_op) == 0 % verificando se o conjunto suspeito n�o est� vazio
                                         fprintf('\n');
                else
                        fprintf('\n');
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                        disp('  N�o apresenta erro em restri��es operacionais (ativa) ') 
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                 end
        end
    end
else
                        fprintf('\n');
                    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                    disp('  N�o apresenta erro em restri��es operacionais (ativa) ') 
                    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
end

%% conjunto suspeito reativa para topologia

nR_op = 0;
r_reativoS_op = [];
conjSR_op = [];
omegaR_S_op = 0;
% omegaR_SS = 0;
identR_R_op = [];

if r_reativoNmax_op > rtol

disp('ANOMALIA DETECTADA - Pseudomedida Reativa')
    identR_R_op =find(abs(resnR_op(:,6))>rtol)%posi��o dos res�duos maiores que a toler�ncia rtol
%     resnR_crit = [abs(resnR_op(identR_R_op,:)) (1:size(identR_R_op,1))']
        disp('ELIMINE AS MEDIDAS CRITICAS!!! - VETOR identR_A_op') % verificando a exist�ncia de medidas cr�ticas
%     conjSR_op = resnR_op(identR_R_op,:)
    omegaR_S_op = omegaR_op(:,identR_R_op); %montando omegaS
%     r_reativoS_op = r_reativo(identR_R_op,1); %IDENT CORRESPONDENTE AO VETOR COMPLETO
end

if isempty(identR_R_op) == 0

    RR_op = RR(reat_m+1:reat_m+reat_op,reat_m+1:reat_m+reat_op);

    numR_op = (r_reativo_op'*RR_op*omegaR_S_op)*inv(omegaR_S_op'*RR_op*omegaR_S_op)*(omegaR_S_op'*RR_op*r_reativo_op);
    denR_op = (r_reativo_op'*RR_op*r_reativo_op);

    cosphiR_op = sqrt(numR_op/denR_op)

         disp('======================================================');
         disp(' TESTE DO COSSENO (restri��es operacionais (reativa))   ') 
         disp('======================================================'); 

    while cosphiR_op >= (1-ecos)

         conjSR_op = resnR_op(identR_R_op,:)

         RR_op = RR(reat_m+1:reat_m+reat_op,reat_m+1:reat_m+reat_op);

         numR_op = (r_reativo_op'*RR_op*omegaR_S_op)*inv(omegaR_S_op'*RR_op*omegaR_S_op)*(omegaR_S_op'*RR_op*r_reativo_op);
         denR_op = (r_reativo_op'*RR_op*r_reativo_op);

         cosphiR_op = sqrt(numR_op/denR_op)

            disp('Conjunto Suspeito ')
            disp('   TIPO   ID   BARRA   DE  PARA    rN') 
            for n = 1:size(conjSR_op,1)
            fprintf('     %d    %d   %d     %d   %d    %g          \n', conjSR_op(n,1:5),conjSR_op(n,6));
            end
            fprintf('\n');
            disp('   cosPHI (pseudomedida reativa)') 
            fprintf('    %g          \n', cosphiR_op);

                if cosphiR_op >= (1-ecos)
                    if isempty(identR_R_op) == 0

                        auxR_op = auxR_op + 1;
                        identR_R_op(auxR_op,:) = [];
                        omegaR_S_op(:,auxR_op) = [];

                    else
                        fprintf('\n');
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                        disp(' N�o apresenta erro em restri��es operacionais (reativa)    ') 
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                    end
                else
                    if isempty(identR_R_op) == 0
                        fprintf('\n');

                    else
                        fprintf('\n');
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                        disp(' N�o apresenta erro em restri��es operacionais (reativa)    ') 
                        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                    end
                end
    end
else
                    fprintf('\n');
                    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
                    disp(' N�o apresenta erro em restri��es operacionais (reativa)    ') 
                    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
end 

%% % conjunto suspeito ativo para as medidas convencionais
nA_m = 0;
r_ativoS_m = [];
conjSA_m = [];
omegaA_S_m = 0;
omegaA_SS = 0;
identR_A_m = [];

if r_ativoNmax_m > rtol %verificando a presen�a de erro
disp('ANOMALIA DETECTADA - Medida Ativa')
    identR_A_m =find(abs(resnA_m(:,6))>rtol)
%        identR_A_m =[  1     2     3     5     7];
%      identR_A_m = [2 3 5 7];    
    abs(resnA_m(identR_A_m,6))
%     pause
%     conjSA_m = resnA_m(identR_A_m,:)
    omegaA_S_m = omegaA_m(:,identR_A_m); %montando omegaS
%     r_ativoS_m = r_ativo_m(identR_A_m,1);
end

if isempty(identR_A_m) == 0

    RA_m = RA(1:at_m,1:at_m);

    numA_m = (r_ativo_m'*RA_m*omegaA_S_m)*inv(omegaA_S_m'*RA_m*omegaA_S_m)*(omegaA_S_m'*RA_m*r_ativo_m);
    denA_m = (r_ativo_m'*RA_m*r_ativo_m);

    cosphiA_m = sqrt(numA_m/denA_m);
            
        disp('======================================');
        disp('   TESTE DO COSSENO (medidas ativas)    ') 
        disp('======================================');
      while cosphiA_m >= (1-ecos) 

            conjSA_m = resnA_m(identR_A_m,:);

            RA_m = RA(1:at_m,1:at_m);

            numA_m = (r_ativo_m'*RA_m*omegaA_S_m)*inv(omegaA_S_m'*RA_m*omegaA_S_m)*(omegaA_S_m'*RA_m*r_ativo_m);
            denA_m = (r_ativo_m'*RA_m*r_ativo_m);

            cosphiA_m = sqrt(numA_m/denA_m);

%     if cosphiA_m >= (1-ecos) 


        fprintf('\n');
        disp('Conjunto Suspeito ')
        disp('   TIPO   ID   BARRA   DE  PARA    rN') 
        for n = 1:size(conjSA_m,1)
        fprintf('    %d    %d   %d     %d   %d    %g          \n', conjSA_m(n,1:5),conjSA_m(n,6));
        end
        fprintf('\n');
        disp('   cosPHI (medidas ativas)') 
        fprintf('    %g          \n', cosphiA_m);
    
               if cosphiA_m >= (1-ecos)
                    if isempty(identR_A_m) == 0
                        auxA_m = auxA_m +1;
                        conjSA_m(auxA_m,:)=[];
                        identR_A_m(auxA_m,:) = [];
                        omegaA_S_m(:,auxA_m) = [];
                    else
                        fprintf('\n');
                        disp('========================================');
                        disp('  CORRIGIDO!    ') 
                        disp('========================================');
                    end
               else 
                    if isempty(identR_A_m) == 0
                        fprintf('\n');

                    else
                        fprintf('\n');
                        disp('=============');
                        disp('  CORRIGIDO!    ') 
                        disp('=============');
                    end
               end
      end 
else                    fprintf('\n');
                        disp('========================================');
                        disp('  N�o apresenta erro em medidas ativas    ') 
                        disp('========================================');
end

%% conjunto suspeito reativa para as medidas convencionais
% 
nR_m = 0;
r_reativoS_m = [];
conjSR_m = [];
omegaR_S_m = 0;
% omegaR_SS = 0;
identR_R_m = [];

if r_reativoNmax_m > rtol
disp('ANOMALIA DETECTADA - Medida Reativa')
    identR_R_m = find(abs(resnR_m(:,6))>rtol) 
    abs(resnR_m(identR_R_m,6))
%     pause
%     conjSR_m = resnR_m(identR_R_m,:)
    omegaR_S_m = omegaR_m(:,identR_R_m); %montando omegaS
%     r_reativoS_m = r_reativo_m(identR_R_m,1);
end

if isempty(identR_R_m) == 0


    RR_m = RR(1:reat_m,1:reat_m);

    numR_m = (r_reativo_m'*RR_m*omegaR_S_m)*inv(omegaR_S_m'*RR_m*omegaR_S_m)*(omegaR_S_m'*RR_m*r_reativo_m);
    denR_m = (r_reativo_m'*RR_m*r_reativo_m);

    cosphiR_m = sqrt(numR_m/denR_m);

        disp('======================================');
        disp('  TESTE DO COSSENO (medidas reativas) ') 
        disp('======================================');
    
    while cosphiR_m >= (1-ecos)
        
        conjSR_m = resnR_m(identR_R_m,:);
        
        RR_m = RR(1:reat_m,1:reat_m);

        numR_m = (r_reativo_m'*RR_m*omegaR_S_m)*inv(omegaR_S_m'*RR_m*omegaR_S_m)*(omegaR_S_m'*RR_m*r_reativo_m);
        denR_m = (r_reativo_m'*RR_m*r_reativo_m);

        cosphiR_m = sqrt(numR_m/denR_m);

        fprintf('\n');

        disp('Conjunto Suspeito ')
        disp('   TIPO   ID   BARRA   DE  PARA    rN') 
        for n = 1:size(conjSR_m,1)
        fprintf('    %d    %d   %d     %d   %d    %g          \n', conjSR_m(n,1:5),conjSR_m(n,6));
        end
        fprintf('\n');
        disp('   cosPHI (medidas reativas') 
        fprintf('    %g          \n', cosphiR_m);
        
                if cosphiR_m >= (1-ecos)
                    if isempty(identR_R_m) == 0
                        auxR_m = auxR_m + 1;
                        identR_R_m(auxR_m,:) = [];
                        omegaR_S_m(:,auxR_m) = [];
                    else
                        fprintf('\n');
                        disp('========================================');
                        disp('  CORRIGIDO!    ') 
                        disp('========================================');
                    end
                else
                    if isempty(identR_R_m) == 0
                        fprintf('\n');
                    else
                        fprintf('\n');
                        disp('==============');
                        disp('  CORRIGIDO!    ') 
                        disp('==============');

                    end
                end
    end
else
        fprintf('\n');
        disp('========================================');
        disp(' N�o apresenta erro em medidas reativas   ') 
        disp('========================================');
end





%% conjunto suspeito para topologia (ativa e reativa)

% n_op = 0;
% r_S_op = [];
% conjS_op = [];
% omega_S_op = 0;
% % omegaR_SS = 0;
% identR_op = [];
% if r_Nmax_op > rtol
% disp('ANOMALIA DETECTADA - Pseudomedidas!!!!!')
%     identR_op =find(abs(resn_op(:,6))>rtol)%posi��o dos res�duos maiores que a toler�ncia rtol
%     abs(resn_op(identR_op,6))
%         disp('ELIMINE AS MEDIDAS CRITICAS!!! - VETOR identR_A_op') % verificando a exist�ncia de medidas cr�ticas
% %     for cont = 1:size(identR_R_op)
% %         
% %     end
%     pause
%     conjS_op = resn_op(identR_op,:)
%     omega_S_op = omega_op(:,identR_op); %montando omegaS
% % omega_SS = omega(identR,identR);
% % omega_SS =inv(R(identR,identR))*omega_SS*inv(R(identR,identR));
% end
%     
%% 29/11/17 reuni�o ERRO EM MEDIDA

% r_S_op = [r_ativoS_op;r_reativoS_op];
% 
% 
% R_op = [RA_op  zeros(at_op,at_op);
%      zeros(reat_op,reat_op)  RR_op];

%% Verifica se todas as informa��es err�neas est�o selecionadas como suspeitas %% Processamento de erros simult�neos (grosseiros em medidas e topologia)

%     r_op = [r_ativo_op;r_reativo_op];
% 
%     num_op = (r_op'*R_op*omega_S_op)*inv(omega_S_op'*R_op*omega_S_op)*(omega_S_op'*R_op*r_op);
%     den_op = (r_op'*R_op*r_op);
% 
%     cosphi_op = sqrt(num_op/den_op)


