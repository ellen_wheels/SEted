
x_sqrAm = chi2inv(0.95,at_m-(nbar-1));
x_sqrRm = chi2inv(0.95,reat_m-nbar);
x_sqrAop = chi2inv(0.95,at_op-nd);
x_sqrRop = chi2inv(0.95,reat_op-nd);

%%

[ haa ] = monta_hzinhoaa_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);
[ hrr ] = monta_hzinhorr_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);

zA_m = zA(1:at_m);
zR_m = zR(1:reat_m);
zA_op = zA(at_m+1:at_m+at_op);
zR_op = zR(reat_m+1:reat_m+reat_op);

haa_m = haa(1:at_m);
hrr_m = hrr(1:reat_m);
haa_op = haa(at_m+1:at_m+at_op);
hrr_op = hrr(reat_m+1:reat_m+reat_op);

RA_m = RA(1:at_m,1:at_m);
RR_m = RR(1:reat_m,1:reat_m);
RA_op = RA(at_m+1:at_m+at_op,at_m+1:at_m+at_op);
RR_op = RR(reat_m+1:reat_m+reat_op,reat_m+1:reat_m+reat_op);

%%
J_x_Am = (zA_m-haa_m)'*inv(RA_m)*(zA_m-haa_m);
J_x_Rm = (zR_m-hrr_m)'*inv(RR_m)*(zR_m-hrr_m);
J_x_Aop = (zA_op-haa_op)'*inv(RA_op)*(zA_op-haa_op);
J_x_Rop = (zR_op-hrr_op)'*inv(RR_op)*(zR_op-hrr_op);

%%

if J_x_Am > x_sqrAm
    disp('ERRO ATIVO - MEDIDA')
    fprintf('x_sqrAm %9.4f J_x_Am  %7.4f \n',  x_sqrAm, J_x_Am);
end
if J_x_Rm > x_sqrRm
    disp('ERRO REATIVO - MEDIDA')
    fprintf('x_sqrRm %9.4f J_x_Rm  %7.4f \n',  x_sqrRm, J_x_Rm);
end
if J_x_Aop > x_sqrAop
    disp('ERRO ATIVO - OPERACIONAL')
    fprintf('x_sqrAop %9.4f J_x_Aop  %7.4f \n',  x_sqrAop, J_x_Aop);
end
if J_x_Rop > x_sqrRop
    disp('ERRO REATIVO - OPERACIONAL')
    fprintf('x_sqrRop %9.4f J_x_Rop  %7.4f \n\n',  x_sqrRop, J_x_Rop);
end



