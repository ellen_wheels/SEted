%% 

% 21/08/17 - Detectar erro de topologia via m�ximo res�duo normalizado
% 17/10/17 - Altera��o para quando o omega for zero!
% 29/11/17 - Vers�o completa
% 15/05/18 - Atualizando H e G
%          - Calculando �mega pela matrix de sensibilidade S


%% Calculando o res�duo

% [h] = monta_hzinho(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas);
% [H] = monta_Hzao(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas); %ok
% [G] = monta_G(H,R);
% 
% r = z-h;

[ haa ] = monta_hzinhoaa_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);
[ hrr ] = monta_hzinhorr_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);

[ HAA ] = monta_HAA_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from);    
[ HRR ] = monta_HRR_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from);

[ GAA, GRR ] = monta_G_SE(HAA, RA, HRR, RR);

r_ativo = zA-haa;
r_ativo_m = r_ativo(1:at_m);
r_ativo_op = r_ativo(at_m+1:at_m+at_op);

r_reativo = zR-hrr;
r_reativo_m = r_reativo(1:reat_m);
r_reativo_op = r_reativo(reat_m+1:reat_m+reat_op);

%% Calculando a ra�z quadrada de omega
% 
% omega = R - (H*inv(G)*H');
% omega_sqrt = sqrt(diag(omega));
% 
%% Calculando a matriz K e a matriz sensibilidade S 
% K = H*inv(G)*H'*inv(R);
% 
% S = eye(size(K,1))- K;

KA = HAA*inv(GAA)*HAA'*inv(RA);
KR = HRR*inv(GRR)*HRR'*inv(RR);

SA = eye(size(KA,1))- KA;
SR = eye(size(KR,1))- KR;

%% Calculando a ra�z quadrada de omega

% omega = S*R;
% % w_ii = inv(R)*omega*inv(R);
% omega_sqrt = sqrt(diag(omega));

omegaA = SA*RA;
omegaR = SR*RR;

% w_ii = inv(R)*omega*inv(R);
omegaA_sqrt = sqrt(abs(diag(omegaA)));
omegaR_sqrt = sqrt(abs(diag(omegaR)));

omegaA_m = omegaA(1:at_m,:);
omegaA_op = omegaA(at_m+1:at_m+at_op,:);
omegaR_m = omegaR(1:reat_m,:);
omegaR_op = omegaR(reat_m+1:reat_m+reat_op,:);

omega_op = [omegaA_op omegaR_op];

%% Calculando o res�duo normalizado

% for cont = 1:size(Medidas,1)
%     if omega_sqrt(cont,1) == 0
%         r_N(cont,1) = 0;
%     else
%         r_N(cont,1) = abs(r(cont,1))/omega_sqrt(cont,1);
%     end
% end

% for cont = 1:size(Medidas,1)
%     if omega_sqrt(cont,1) == 0
%         r_N(cont,1) = 0;
%     else
%         r_N(cont,1) = abs(r(cont,1))/omega_sqrt(cont,1);
%     end
% end
% 
% resn = [Medidas(:,1) Medidas(:,3:5) r_N];

for contAt = 1:size(MedAt,1) % monta o vetor completo do res�duo normalizado
    if omegaA_sqrt(contAt,1) == 0
        r_ativoN(contAt,1) = 0;
    else
        r_ativoN(contAt,1) = abs(r_ativo(contAt,1)/omegaA_sqrt(contAt,1));
    end
end

% resnA = [MedAt(:,1) MedAt(:,3:5) r_ativoN];
resnA = [MedAt(:,1:5) r_ativoN];

% separa a real e imagin�ria
resnA_m = resnA(1:at_m,:);
resnA_op = resnA(at_m+1:at_m+at_op,:);

for contReat = 1:size(MedReat,1) % monta o vetor completo do res�duo normalizado
    if omegaR_sqrt(contReat,1) == 0
        r_reativoN(contReat,1) = 0;
    else
        r_reativoN(contReat,1) = abs(r_reativo(contReat,1)/omegaR_sqrt(contReat,1));
    end
end
% resnR = [MedReat(:,1) MedReat(:,3:5) r_reativoN];
resnR = [MedReat(:,1:5) r_reativoN];

% separa a real e imagin�ria
resnR_m = resnR(1:reat_m,:);
resnR_op = resnR(reat_m+1:reat_m+reat_op,:);

% juntando os conjuntos operacionais (ativos e reativos)
resn_op = [resnA_op;resnR_op];

%% Detec��o de erro

% r_Nmax = max(abs(r_N));
% 
% for n = 1:size(r_N,1)
%     if abs(r_N(n,1))== r_Nmax
%         resnmax = resn(n,:);
%     end
% end

r_ativoN_m = r_ativoN(1:at_m,:);
r_ativoN_op = r_ativoN(at_m+1:at_m+at_op,:);

r_ativoNmax_m = max(abs(r_ativoN_m));
r_ativoNmax_op = max(abs(r_ativoN_op));

r_reativoN_m = r_reativoN(1:reat_m,:);
r_reativoN_op = r_reativoN(reat_m+1:reat_m+reat_op,:);

r_reativoNmax_m = max(abs(r_reativoN_m));
r_reativoNmax_op = max(abs(r_reativoN_op));

%  maximo res�duo normalizado completo
r_Nop =[r_ativoN_op;r_reativoN_op];

r_Nmax_op = max([r_ativoNmax_op;r_reativoNmax_op]);


for n = 1:size(r_ativoN_m,1)
    if abs(r_ativoN_m(n,1))== r_ativoNmax_m
        resnAmax_m = resnA_m(n,:);
    end
end
for n = 1:size(r_ativoN_op,1)
    if abs(r_ativoN_op(n,1))== r_ativoNmax_op
        resnAmax_op = resnA_op(n,:);
    end
end

for n = 1:size(r_reativoN_m,1)
    if abs(r_reativoN_m(n,1))== r_reativoNmax_m
        resnRmax_m = resnR_m(n,:);
    end
end
for n = 1:size(r_reativoN_op,1)
    if abs(r_reativoN_op(n,1))== r_reativoNmax_op
        resnRmax_op = resnR_op(n,:);
    end
end

