%% ALTERACOES
% 31/08/17 - ERROS ALEATORIOS

%%
clear all
clc

%% CPU  

N_inicio
abs(V_matpower);
(180/pi)*angle(V_matpower);
novos_dadosD141barras_radial_modelo
sistDist141_radial

%% GERADOR DE MEDIDAS - FLUXO DE POT�NCIA
fprintf('\n');
% principal_pu;

matpowerparaEE_modelo;

nd = 0;
tol = 10^-3;

% % para o c�lculo do erro
% VFPfull = V;
% tetaFPfull = teta;

%% SIMULADOR DE ERROS ALEAT�RIOS
% simula;

%% MONTANDO MATRIZ DE MEDIDAS
% cpu;

%     [Medidas,MedAt,MedReat] = monta_matriz_medidas_semerro(Pkm, Qkm, Pcalc, Qcalc, V, linha, barra, nl, nbar);

%% Fluxo n�o convergiu
% if iter >= itermax
%         return
% end

%% SIMULANDO ERRO DE TOPOLOGIA

%                erro_topologia
pee;

monta_medidas;

%clear_all;

%% MONTA zA e zR

                 [zA] = monta_ZA_SE(Medidas, Linha, nramos);
                 [zR] = monta_ZR_SE(Medidas, Linha, nramos);

%                [zA] = monta_ZA(Medidas);
%                [zR] = monta_ZR(Medidas);
                 
%% SIMULANDO ERRO EM MEDIDA

%             erro_medida

%% ESTIMADOR DESACOPLADO
tic;
principal_SE
tempo = toc;

%    diary on
%     fprintf('\n\n');
    disp('================================================================');
    disp(' RESULTADOS DO ESTIMADOR DE ESTADOS DESACOPLADO R�PIDO - SE     ');
    disp('================================================================');
    fprintf('             Sistema com %g Barras e %g Ramos\n', nbar,nramos)
    if nd ~= 0
        fprintf('    Modelado em N�vel de Subesta��o com %g Ramos Chave�vel\n', nd)
    end
    disp('================================================================');
    fprintf('\n Tempo gasto no processo iterativo %g s', tempo);
    fprintf('\n N�mero de itera��es: %g ', contit);
    fprintf('\n N�mero de itera��es ativas: %g ', contativa);
    fprintf('\n N�mero de itera��es reativas: %g \n', contreativa);
 
%     fprintf('\n');
    disp('================================================================');
    disp('         ESTIMADOR DE ESTADOS DESACOPLADO R�PIDO - SE       ');
    disp('================================================================');
    disp('              BARRA       TENS�O(V)    �NGULO(GRAUS)') 
    for n = 1:nbar
    fprintf('              %4d      %9.4f       %7.4f \n',  n, Vnew(n), (tetanew(n).*180)/pi()   );
    end
%     fprintf('\n');
    disp('================================================================');
    disp('          DISJUNTOR       FLUXO ATIVO(MW)  FLUXO REATIVO(MVar)') 
    for n = 1:nd
    fprintf('           %d - %d          %9.4f           %7.4f \n',  from(nl+n), to(nl+n), tnew(n)*100, unew(n)*100   );
    end

%     fprintf('\n');
    tempo
    
%     clear all

%% para o c�lculo do erro

% VFPmodelo = abs(V_matpower);
% tetaFPmodelo = angle(V_matpower);  
% VEEmodelo = Vnew;
% tetaEEmodelo = tetanew;
% 
% calc_erro_modelo
    
%% IDENTIFICA��O DE ERROS DE TOPOLOGIA

 %x_sqr % Teste chi-square

 detecta_erro % Verifica a presen�a do erro
 identifica_erro %Identifica��o do erro segundo a natureza
 
% 
%     disp('================================================================');
%     disp('                  DETEC��O E IDENTIFICA��O DE ERROS    ');
%     disp('================================================================');
% 
%     disp('======================================');
%     disp('          ATIVO       rN  ') 
%     disp('======================================');
%     for n = 1:size(resnA,1)
%         if resnA(n,1)==1
%         fprintf('  Fluxo Ativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         elseif resnA(n,1)==3
%         fprintf('  Inje��o Ativa                             %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Ativo)')
%     fprintf('   %d %d  %d  %d   -  %g          \n', resnAmax);
%     fprintf('\n');
%     disp('======================================');
%     disp('         REATIVO     rN') 
%     disp('======================================');
%     for n = 1:size(resnR,1)
%         if resnR(n,1)==2
%         fprintf('  Fluxo Reativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==4
%         fprintf('  Inje��o Reativa                             %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==5
%         fprintf('  M�dulo de Tens�o                            %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Reativo)')
%     fprintf('    %d %d  %d  %d   -  %g          \n', resnRmax);
%     
%    fprintf('\n');
%     if (isempty(conjSA)||isempty(conjSR)) == 0
%         disp('======================================');
%         disp('        TESTE DO COSSENO     ') 
%         disp('======================================');
%     elseif isempty(conjSA)&&isempty(conjSR) == 1
%         disp('======================================');
%         disp('        N�o apresenta erro     ') 
%         disp('======================================');
%     end 
%     if isempty(conjSA) == 0
%         disp('Conjunto Suspeito (Ativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSA,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSA(n,1:4),conjSA(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiA);
%     end
%     fprintf('\n');
%     if isempty(conjSR) == 0
%         disp('Conjunto Suspeito (Reativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSR,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSR(n,1:4),conjSR(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiR);
%     end
% %     
% %   sort (conjSA(:,5))
% % % 
% %   sort (conjSR(:,5))
% 
% %  sort (resnA(:,5))
% % % 
% disp('_')
% %   sort (resnR(:,5))
% % 
% % % % zse1
% % 
% % diary off
% % calc_erro
% 
% %% Reestima��o
% % 
% aux = auxA3;
% % 
% reestimacao
% 
% %% IDENTIFICA��O DE ERROS DE TOPOLOGIA
% 
% detecta_erro
% identifica_erro
% 
%     disp('================================================================');
%     disp('                  DETEC��O E IDENTIFICA��O DE ERROS    ');
%     disp('================================================================');
% 
%     disp('======================================');
%     disp('          ATIVO       rN  ') 
%     disp('======================================');
%     for n = 1:size(resnA,1)
%         if resnA(n,1)==1
%         fprintf('  Fluxo Ativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         elseif resnA(n,1)==3
%         fprintf('  Inje��o Ativa                             %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Ativo)')
%     fprintf('   %d %d  %d  %d   -  %g          \n', resnAmax);
%     fprintf('\n');
%     disp('======================================');
%     disp('         REATIVO     rN') 
%     disp('======================================');
%     for n = 1:size(resnR,1)
%         if resnR(n,1)==2
%         fprintf('  Fluxo Reativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==4
%         fprintf('  Inje��o Reativa                             %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==5
%         fprintf('  M�dulo de Tens�o                            %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Reativo)')
%     fprintf('    %d %d  %d  %d   -  %g          \n', resnRmax);
%     
%    fprintf('\n');
%     if (isempty(conjSA)||isempty(conjSR)) == 0
%         disp('======================================');
%         disp('        TESTE DO COSSENO     ') 
%         disp('======================================');
%     elseif isempty(conjSA)&&isempty(conjSR) == 1
%         disp('======================================');
%         disp('        N�o apresenta erro     ') 
%         disp('======================================');
%     end 
%     if isempty(conjSA) == 0
%         disp('Conjunto Suspeito (Ativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSA,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSA(n,1:4),conjSA(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiA);
%     end
%     fprintf('\n');
%     if isempty(conjSR) == 0
%         disp('Conjunto Suspeito (Reativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSR,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSR(n,1:4),conjSR(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiR);
%     end
%     
%    %% Reestima��o
% 
% aux = auxA2-1;
% 
% % aux = auxA3;
% reestimacao
% 
% %% IDENTIFICA��O DE ERROS DE TOPOLOGIA
% 
% detecta_erro
% identifica_erro
% 
%     disp('================================================================');
%     disp('                  DETEC��O E IDENTIFICA��O DE ERROS    ');
%     disp('================================================================');
% 
%     disp('======================================');
%     disp('          ATIVO       rN  ') 
%     disp('======================================');
%     for n = 1:size(resnA,1)
%         if resnA(n,1)==1
%         fprintf('  Fluxo Ativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         elseif resnA(n,1)==3
%         fprintf('  Inje��o Ativa                             %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Ativo)')
%     fprintf('   %d %d  %d  %d   -  %g          \n', resnAmax);
%     fprintf('\n');
%     disp('======================================');
%     disp('         REATIVO     rN') 
%     disp('======================================');
%     for n = 1:size(resnR,1)
%         if resnR(n,1)==2
%         fprintf('  Fluxo Reativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==4
%         fprintf('  Inje��o Reativa                             %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==5
%         fprintf('  M�dulo de Tens�o                            %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Reativo)')
%     fprintf('    %d %d  %d  %d   -  %g          \n', resnRmax);
%     
%    fprintf('\n');
%     if (isempty(conjSA)||isempty(conjSR)) == 0
%         disp('======================================');
%         disp('        TESTE DO COSSENO     ') 
%         disp('======================================');
%     elseif isempty(conjSA)&&isempty(conjSR) == 1
%         disp('======================================');
%         disp('        N�o apresenta erro     ') 
%         disp('======================================');
%     end 
%     if isempty(conjSA) == 0
%         disp('Conjunto Suspeito (Ativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSA,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSA(n,1:4),conjSA(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiA);
%     end
%     fprintf('\n');
%     if isempty(conjSR) == 0
%         disp('Conjunto Suspeito (Reativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSR,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSR(n,1:4),conjSR(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiR);
%     end
%     
%      %% Reestima��o
% 
% aux = auxA1-2;
% 
% reestimacao
% 
%  % IDENTIFICA��O DE ERROS DE TOPOLOGIA
% 
% detecta_erro
% identifica_erro
% 
%     disp('================================================================');
%     disp('                  DETEC��O E IDENTIFICA��O DE ERROS    ');
%     disp('================================================================');
% 
%     disp('======================================');
%     disp('          ATIVO       rN  ') 
%     disp('======================================');
%     for n = 1:size(resnA,1)
%         if resnA(n,1)==1
%         fprintf('  Fluxo Ativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         elseif resnA(n,1)==3
%         fprintf('  Inje��o Ativa                             %d  %d  %d   -  %g          \n', resnA(n,2:4),resnA(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Ativo)')
%     fprintf('   %d %d  %d  %d   -  %g          \n', resnAmax);
%     fprintf('\n');
%     disp('======================================');
%     disp('         REATIVO     rN') 
%     disp('======================================');
%     for n = 1:size(resnR,1)
%         if resnR(n,1)==2
%         fprintf('  Fluxo Reativo em Ramo Convencional          %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==4
%         fprintf('  Inje��o Reativa                             %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         elseif resnR(n,1)==5
%         fprintf('  M�dulo de Tens�o                            %d  %d  %d   -  %g          \n', resnR(n,2:4),resnR(n,5));
%         end
%     end
% %     fprintf('\n');
%     disp('M�ximo Res�duo Normalizado(Reativo)')
%     fprintf('    %d %d  %d  %d   -  %g          \n', resnRmax);
%     
%    fprintf('\n');
%     if (isempty(conjSA)||isempty(conjSR)) == 0
%         disp('======================================');
%         disp('        TESTE DO COSSENO     ') 
%         disp('======================================');
%     elseif isempty(conjSA)&&isempty(conjSR) == 1
%         disp('======================================');
%         disp('        N�o apresenta erro     ') 
%         disp('======================================');
%     end 
%     if isempty(conjSA) == 0
%         disp('Conjunto Suspeito (Ativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSA,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSA(n,1:4),conjSA(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiA);
%     end
%     fprintf('\n');
%     if isempty(conjSR) == 0
%         disp('Conjunto Suspeito (Reativo)')
%         disp('   TIPO   BARRA   DE  PARA    rN') 
%         for n = 1:size(conjSR,1)
%         fprintf('    %d       %d     %d   %d    %g          \n', conjSR(n,1:4),conjSR(n,5));
%         end
%         disp('   cosPHI') 
%         fprintf('    %g          \n', cosphiR);
%     end  
%     
    
%     sort (resnA(:,5))