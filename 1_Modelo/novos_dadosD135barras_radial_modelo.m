%%
% clc 
% clear all
% 

%%

%	bus_i	type	Pd	Qd	Gs	Bs	area	Vm	Va	baseKV	zone	Vmax	Vmin														
bus	= Barra;

%%
%	bus	Pg	Qg	Qmax	Qmin	Vg	mBase	status	Pmax	Pmin	
gen = Geracao;

%%
%	fbus	tbus	r	x	b	rateA	rateB	rateC	ratio	angle	status
branch = Linha;

%%
for k = 1:size(bus,1)
    nbarra(k,1) = k;
end

 nbus = [bus(:,1) nbarra]; 

    %% Dados Originais
    type = bus(:,2)-1;
    %     V = bus(:,8);
%     angle = bus(:,9);
    V = abs(V_matpower);
    angle = angle(V_matpower);
    
    %% Passando para PU
    Vbase = bus(:,10);
    Sbase = 100; % para sistema 907 barras - 1 MVA
    BshB = bus(:,6)/100;
    Pd = bus(:,3);
    Qd = bus(:,4);    
    Pdpu = Pd./Sbase;
    Qdpu = Qd./Sbase;
    Pg = gen(:,2);
    Qg = gen(:,3);
    Pgpu = zeros(size(bus,1),1);
    Qgpu = zeros(size(bus,1),1);
    
    for nbar = 1:size(nbus,1)
        for ng = 1:size(gen,1)
            if gen(ng,1) == nbus(nbar,1)
            Pgpu(nbar) = Pg(ng)/Sbase;
            Qgpu(nbar) = Qg(ng)/Sbase;
            end
        end        
    end
        
    %% Vetores 'de' e 'para'
    
    from = branch(:,1);
    to = branch(:,2);
    
    aux = 0;
    cont = 0;
    
    for nl = 1:size(branch,1);
        for nbar = 1:size(nbus,1) 
            if to(nl,1) ==  nbus(nbar,1)
                aux = aux + 1;
                tonew(aux,:) = [to(nl,1) nbus(nbar,2)];
            end
            if from(nl,1) ==  nbus(nbar,1)
                cont = cont+1;
                fromnew(cont,:) = [from(nl,1) nbus(nbar,2)];
            end
        end
    end
    
    %% Dados da linha
    
    Rpu = branch(:,3);
    Xpu = branch(:,4);
    Bpu = branch(:,5);
     
    %%
    
  pmuV = zeros(nbar,1);
  pmuV([1;30;61;72],1) = 1;
  
%	      barra  tipo	tens�o	�ngulo	  Pd	    Qd           Pg         Qg      bshbar   med_p  med_q   med_V

newbarra = [nbarra type V angle Pdpu Qdpu Pgpu Qgpu BshB ones(nbar,1) ones(nbar,1) zeros(nbar,1) zeros(nbar,1) zeros(nbar,1) pmuV zeros(nbar,1)];

%       na     nbar    r       x       bshlin  med_t med_u 
newlinha = [fromnew(:,2) tonew(:,2) Rpu Xpu Bpu 3*ones(nl,1) 3*ones(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1) zeros(nl,1)];

%%

clearvars -except nbar nl Sbase newbarra newlinha V_matpower bus_matpower gen_matpower branch_matpower Ybarra G B
