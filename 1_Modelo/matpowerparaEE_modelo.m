%% Criado em 04/04/18

%% Sa�da fluxo do matpower para cria��o do vetor de medidas

Pkm = branch_matpower(:,14)./Sbase;

Pmk = branch_matpower(:,16)./Sbase;

Qkm = branch_matpower(:,15)./Sbase;

Qmk = branch_matpower(:,17)./Sbase;

%% Sa�da inje��o do matpower para cria��o do vetor de medidas

Pgerado = zeros (nbar,1);
bg = gen_matpower(:,1);
Pgerado(bg,1)= gen_matpower(:,2)./Sbase;
Qgerado = zeros (nbar,1);
Qgerado(bg,1) = gen_matpower(:,3)./Sbase;

Pdemandado = bus_matpower(:,3)./Sbase;

Qdemandado = bus_matpower(:,4)./Sbase;

Pcalc = Pgerado - Pdemandado;

Qcalc = Qgerado - Qdemandado;

%% Sa�da tens�o complexa

V_medida = abs(V_matpower);

teta_medida = angle(V_matpower);

%% Matriz Ybarra

G = real(Ybarra);   % Parte real da matriz Ybarra
B = imag(Ybarra);   % Parte Imaginaria da matriz Ybarra