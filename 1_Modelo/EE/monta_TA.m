function [ TA ] = monta_TA( HAA, RA, delta_zAlinha)
% ESTA FUN��O CALCULA O TA=HAA'*RA^-1*AzA'
%                       TR=HRR'*RR^-1*AzR'

TA = HAA'*inv(RA)*delta_zAlinha;

end