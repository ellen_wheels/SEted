function [zA,zR] = monta_Zzao(Medidas) 

% Z - matriz de valores medidos

%%
tipo = Medidas(:,1);
i = Medidas(:,3);
k = Medidas (:,4);
m = Medidas (:,5);
medida = Medidas (:,6);

nmedidas = size(tipo,1);

%% vetores de medidas
pkm = [];
pmk = [];
qkm = [];
qmk = [];
pinj = [];
qinj = [];
V = [];

%% contadores vetores de medidas
ypkm = 0;
ypmk = 0;
yqkm = 0;
yqmk = 0;
ypinj = 0;
yqinj = 0;
yv = 0;

%%
for aux = 1:nmedidas;
    %%
    if tipo (aux) == 1; %medidas de fluxo ativo
        
            if k(aux) <= m(aux) %fluxo da barra k para a barra m 
                ypkm = ypkm+1;
                pkm(ypkm,1) = medida(aux);
            else %fluxo da barra m para a barra k
                ypmk = ypmk+1;
                pmk(ypmk,1) = medida(aux);
            end
    end
   %%
   if tipo(aux) == 2; %medidas de fluxo reativo
       
            if k(aux) <= m(aux) %fluxo da barra k para a barra m
                yqkm = yqkm+1;
                qkm(yqkm,1) = medida(aux);
            else %fluxo da barra m para a barra k
                yqmk = yqmk+1;
                qmk(yqmk,1) = medida(aux);
            end
    end
 %%   
    if tipo(aux) == 3; %medidas inje��o ativa
            ypinj = ypinj+1;
            pinj(ypinj,1) = medida(aux);
    end
 %%   
    if tipo(aux) == 4; %medidas inje��o reativa 
            yqinj = yqinj+1;
            qinj(yqinj,1)= medida(aux); 
    end
  %%  
    if tipo(aux) == 5; %medidas tens�o
       yv = yv+1;
       V(yv,1)= medida(aux);  
    end
end 

%%
zA = [  pkm; % fluxo de pot�ncia ativa de k para m
        pmk; % fluxo de pot�ncia ativa de m para k
        pinj]; % inje��o de pot�ncia ativa

zR = [ qkm; % fluxo de pot�ncia reativa de k para m
       qmk; % fluxo de pot�ncia reativa de m para k
       qinj; % inje��o de pot�ncia reativa
       V]; % m�dulo da tens�o
            

end