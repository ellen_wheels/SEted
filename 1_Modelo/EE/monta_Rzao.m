function [RA,RR] = monta_Rzao(Medidas)

% R - matriz covari�ncia

%%
var = Medidas(:,7);
tipo = Medidas (:,1);
k = Medidas (:,4);
m = Medidas (:,5);

nmedidas = size(tipo,1);
%%

ypkm = 0;
ypmk = 0;
yqkm = 0;
yqmk = 0;
ypinj = 0;
yqinj = 0;
yv = 0;

varPkm = [];
varPmk = [];
varQkm = [];
varQmk = [];
varPinj = [];
varQinj = [];
varV = [];

%%
for aux = 1:nmedidas;
    
    % submatriz fluxo ativo
%%
if tipo (aux) == 1; %medidas de fluxo ativo
        
            if k <= m %fluxo da barra k para a barra m 
                ypkm = ypkm+1;
                varPkm(ypkm,1) = var(aux).^2;
            else %fluxo da barra m para a barra k
                ypmk = ypmk+1;
                varPmk(ypmk,1) = var(aux).^2;
            end
end
%%
 % submatriz fluxo reativo
if tipo (aux) == 2; %medidas de fluxo ativo
        
                if k <= m %fluxo da barra k para a barra m 
                    yqkm = yqkm+1;
                    varQkm(yqkm,1) = var(aux).^2;
                else %fluxo da barra m para a barra k
                    yqmk = yqmk+1;
                    varQmk(yqmk,1) = var(aux).^2;
                end
end
%%
 % submatriz inje��o ativa
if tipo(aux) == 3; 
        ypinj = ypinj+1;
        varPinj(ypinj,1) = var(aux).^2;
end
%%
 % submatriz inje��o ativa
if tipo(aux) == 4; 
           yqinj = yqinj+1;
           varQinj(yqinj,1)= var(aux).^2; 
end
 %%   
 % submatriz tens�o
    if tipo(aux) == 5; 
       yv = yv+1;
       varV(yv,1)= var(aux).^2;  
    end
    
end

%%
% monta mariz R - vari�ncia de medidas
% matriz diagonal

%%RA

raaux = [varPkm;
        varPmk;
        varPinj];
    
    RA = diag(raaux);
    
%%RR

rraux = [varQkm;
         varQmk;
         varQinj;
         varV];
     
     RR = diag (rraux);
     

end

