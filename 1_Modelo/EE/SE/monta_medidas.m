%% MONTA VETOR DE MEDIDAS

% 22/08/17 - arquivo criado a partir de programa��o previamente situada em
% 'principal_SE_modelo'


%%

sigma = 0.001;

nr = nd + nl;

%% FLUXO

% TIPO
% t - medida de fluxo de pot�ncia ativa
% u - medida de fluxo de pot�ncia reativa

% �NDICE
% 0 - n�o h� medida
% 1 - medida no extremo 'de'
% 2 - medida no extremo 'para'
% 3 - medida nos dois extremos


line = linha(:,1:2);
med_t = linha(:,6);
med_u = linha(:,7);
med_i = linha(:,14);

jj1=0;
jj2=0;
ii1=0;
ii2=0;
ww = 0;

tkm_info=[]; 
tmk_info=[];
ukm_info=[];
umk_info=[];
i_real_info = [];
i_imag_info = [];

g = -G;
b = -B;
for aux=1:nr
    bsh(na(aux),nb(aux))=bshlin(aux);
    bsh(nb(aux),na(aux))=bshlin(aux);
end

%% MEDIDAS NAS LINHAS (RAMOS CONVENCIONAIS)

for aux = 1:nl
    %% MEDIDAS DE FLUXO ATIVO
    if med_t(aux) == 1 %medida no extremo k
        jj1 = jj1+1;
        tkm_info(jj1,:) = [line(aux,:) Pkm(aux)];
    elseif med_t(aux) == 2 %medida no extremo m
        jj2 = jj2+1;
        tmk_info(jj2,:) = [line(aux,2) line(aux,1) Pmk(aux)];
    elseif med_t(aux) == 3 %medida nos dois extremos - k e m
        jj1 = jj1+1;
        jj2 = jj2+1;
        tkm_info(jj1,:) = [line(aux,:) Pkm(aux)];
        tmk_info(jj2,:) = [line(aux,2) line(aux,1) Pmk(aux)];
    end
    %% MEDIDAS DE FLUXO REATIVO
    if med_u(aux) == 1
        ii1 = ii1+1;
        ukm_info(ii1,:) = [line(aux,:) Qkm(aux)];
    elseif med_u(aux) == 2 %medida no extremo m
        ii2 = ii2+1;
        umk_info(ii2,:) = [line(aux,2) line(aux,1) Qmk(aux)];
    elseif med_u(aux) == 3 %medida nos dois extremos - k e m
        ii1 = ii1+1;
        ii2 = ii2+1;
        ukm_info(ii1,:) = [line(aux,:) Qkm(aux)];
        umk_info(ii2,:) = [line(aux,2) line(aux,1) Qmk(aux)];
    end
   %% PARTE REAL/ATIVA E IMAGIN�RIA/REATIVA DA MEDIDA DE CORRENTE
    if med_i(aux) == 1
        ww = ww+1;
        k = line(aux,1);
        m = line(aux,2);
        Ikm_real = g(k,m)*V_medida(k)*cos(teta(k))-g(k,m)*V(m)*cos(teta(m))-b(k,m)*V(k)*sin(teta(k))-bsh(k,m)*V_medida(k)*sin(teta(k))+b(k,m)*V_medida(m)*sin(teta(m));
        i_real_info(ww,:) = [line(aux,:) Ikm_real];
        Ikm_imag = b(k,m)*V_medida(k)*cos(teta(k))-b(k,m)*V(m)*cos(teta(m))+bsh(k,m)*V(k)*cos(teta(k))+g(k,m)*V_medida(k)*sin(teta(k))-g(k,m)*V_medida(m)*sin(teta(m));
        i_imag_info(ww,:) = [line(aux,:) Ikm_imag];
        Ikm(ww,:) = [line(aux,:) Ikm_real+1i*Ikm_imag];
    end
end


t_info = [tkm_info; tmk_info];
u_info = [ukm_info; umk_info];

nramos = nl+nd;
med_td = linha(:,8);
med_ud = linha(:,9);
med_Vnulo = linha(:,11);
med_tetanulo = linha(:,10);
med_tnulo = linha(:,12);
med_unulo = linha(:,13);

aa1 = 0;
aa2 = 0;
bb1 = 0;
bb2 = 0;
cc = 0;
dd = 0;
ee = 0;
ff = 0;

tdkm_info=[]; 
tdmk_info=[];
udkm_info=[];
udmk_info=[];

%% MEDIDAS NOS RAMOS CHAVE�VEIS

for aux = nl+1:nramos
    %% MEDIDAS DE FLUXO ATIVO EM RAMOS CHAVE�VEIS
    if med_td(aux) == 1
        aa1 = aa1+1;
        tdkm_info(aa1,:) = [line(aux,:) t(aux-nl)];
    elseif med_td(aux) == 2 %medida no extremo m
        aa2 = aa2+1;
        tdmk_info(aa2,:) = [line(aux,2) line(aux,1) -t(aux-nl)];
    elseif med_td(aux) == 3 %medida nos dois extremos - k e m
        aa1 = aa1+1;
        aa2 = aa2+1;
        tdkm_info(aa1,:) = [line(aux,:) t(aux-nl)];
        tdmk_info(aa2,:) = [line(aux,2) line(aux,1) -t(aux-nl)];
    end
    %% MEDIDAS DE FLUXO REATIVO EM RAMOS CHAVE�VEIS
    if med_ud(aux) == 1
        bb1 = bb1+1;
        udkm_info(bb1,:) = [line(aux,:) u(aux-nl)];
    elseif med_ud(aux) == 2 %medida no extremo m
        bb2 = bb2+1;
        udmk_info(bb2,:) = [line(aux,2) line(aux,1) -u(aux-nl)];
    elseif med_ud(aux) == 3 %medida nos dois extremos - k e m
        bb1 = bb1+1;
        bb2 = bb2+1;
        udkm_info(bb1,:) = [line(aux,:) u(aux-nl) ];
        udmk_info(bb2,:) = [line(aux,2) line(aux,1) -u(aux-nl) ];
    end
    %% DIFEREN�A ANGULAR NULA (DISJUNTORES FECHADOS)
    if med_tetanulo(aux) == 1
        cc = cc+1;
        tetanulo_info(cc,:) = [line(aux,:) 0];
    end
    %% DIFEREN�A DE POTENCIAL NULA (DISJUNTORES FECHADOS)
    if med_Vnulo(aux) == 1
        dd = dd+1;
        Vnulo_info(dd,:) = [line(aux,:) 0];
    end
    %% FLUXO DE POT�NCIA ATIVA NULA (DISJUNTORES ABERTOS)
    if med_tnulo(aux) == 1
        ee = ee+1;
        tnulo_info(ee,:) = [line(aux,:) 0];
    end
    %% FLUXO DE POT�NCIA REATIVA NULA (DISJUNTORES ABERTOS)
    if med_unulo(aux) == 1
        ff = ff+1;
        unulo_info(ff,:) = [line(aux,:) 0];
    end
end

td_info = [tdkm_info; tdmk_info];
ud_info = [udkm_info; udmk_info];

%% INJE��O

% TIPO
% p - medida de inje��o de pot�ncia ativa
% q - medida de inje��o de pot�ncia reativa
% v - medida de magnitude de tens�o
% teta - medida de �ngulo (rad)
% tetaref - medida de �ngulo de refer�ncia
% i - medida de magnitude de corrente

% �NDICE
% 0 - n�o h� medida
% 1 - h� medida

bus = barra(:,1);
med_p = barra(:,10); % inje��o de pot�ncia ativa
med_q = barra(:,11); % inje��o de pot�ncia reativa
med_p0 = barra(:,12); % inje��o nula de pot�ncia ativa
med_q0 = barra(:,13); % inje��o nula de pot�ncia reativa
med_v = barra(:,15); % m�dulo da tens�o
med_teta = barra(:,16); % m�dulo da tens�o
med_tetaref = barra(:,14); % barra de refer�ncia

rr = 0;
ss = 0;
tt = 0;
uu = 0;
vv = 0;
xx = 0;
zz = 0;

%% MEDIDAS NAS BARRAS

for aux = 1:nbar
    %% MEDIDAS DE INJE��O DE POT�NCIA ATIVA
    if med_p(aux) == 1
        rr = rr+1;
        p_info(rr,:) = [bus(aux,:) Pcalc(aux)];
    end
    %% MEDIDAS DE INJE��O DE POT�NCIA ATIVA NULA 
    if med_p0(aux) == 1
        ss = ss+1;
        p0_info(ss,:) = [bus(aux,:) 0];
    end
    %% MEDIDAS DE INJE��O DE POT�NCIA REATIVA
    if med_q(aux) == 1
        tt = tt+1;
        q_info(tt,:) = [bus(aux,:) Qcalc(aux)];
    end
    %% MEDIDAS DE INJE��O DE POT�NCIA ATIVA NULA 
    if med_q0(aux) == 1
        uu = uu+1;
        q0_info(uu,:) = [bus(aux,:) 0];
    end
    %% �NGULO DAS BARRAS DE REFER�NCIA 
    if med_tetaref(aux) == 1
        vv = vv+1;
        tetaref_info(vv,:) = [bus(aux,:) 0];
    end
    %% MEDIDAS DE M�DULO DA TENS�O
    if med_v(aux) == 1
        xx = xx+1;
        v_info(xx,:) = [bus(aux,:) V_medida(xx,1)];
    end
    %% MEDIDAS DE �NGULO
    if med_teta(aux) == 1
        zz = zz+1;
        teta_info(zz,:) = [bus(aux,:) teta(zz,1)];
    end

end

%% TAMANHO DOS VETORES 

at_m = jj1+jj2+aa1+aa2+rr+ss+vv+zz+ww;
at_op = cc+ee;
reat_m = ii1+ii2+bb1+bb2+tt+uu+xx+ww;
reat_op = dd+ff;

%% RESULTADOS
  

nmedFluAt = 0;
nmedFluAtCh = 0;
nmedInjAt = 0;
nmedtetaAt = 0;
nmedtetaRef = 0;
nmedtetaNulo = 0;
nmedtNulo = 0;

%     fprintf('\n \n');
%     disp('================================================================');
%     disp('                     MEDIDAS ATIVAS                             ');
%     disp('================================================================');
    %%
    if sum(med_t)~=0
        for nmedFluAt = 1:size(t_info,1)
             % %         Medidas                      tipo                      n               i    j         valor (pu) cov(pu)(sqrt(R))  
             MedAtConv(nmedFluAt,:) = [   1  nmedFluAt   0    t_info(nmedFluAt,1:3)   8*sigma ];
        end
    end
      %%
    if sum(med_td)~=0
        for nmedFluAtCh = 1:size(td_info,1)
            % %         Medidas                      tipo                      n               i    j         valor (pu) cov(pu)(sqrt(R))  
            MedAtConv(nmedFluAt+nmedFluAtCh,:) = [2  nmedFluAtCh  0  td_info(nmedFluAtCh,1:3)      8*sigma];
        end
    end
     %%
    if sum(med_p)~=0
        for nmedInjAt = 1:size(p_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtConv(nmedFluAt+nmedFluAtCh+nmedInjAt,:) = [5  nmedInjAt   p_info(nmedInjAt,1) 0  0  p_info(nmedInjAt,2)    10*sigma];
        end
    end
    %%
    if sum(med_teta)~=0
        for nmedtetaAt = 1:size(teta_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtConv(nmedFluAt+nmedFluAtCh+nmedInjAt+nmedtetaAt,:) = [15  nmedtetaAt   teta_info(nmedtetaAt,1) 0  0  teta_info(nmedtetaAt,2)    0.1*sigma];
        end
    end
    %%
    if sum(med_i)~=0
        for nmedirealAt = 1:size(i_real_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtConv(nmedFluAt+nmedFluAtCh+nmedInjAt+nmedtetaAt+nmedirealAt,:) = [16  nmedirealAt   0    i_real_info(nmedirealAt,1:2)  i_real_info(nmedirealAt,3)    0.1*sigma];
        end
    end
    %%
    if sum(med_tetaref)~=0
        for nmedtetaRef = 1:size(tetaref_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtConv(nmedFluAt+nmedFluAtCh+nmedInjAt+nmedtetaAt++nmedirealAt+nmedtetaRef,:) = [9  nmedtetaRef   tetaref_info(nmedtetaRef,1) 0 0    tetaref_info(nmedtetaRef,2)    0.1*sigma];
        end
    end
          %%
    if sum(med_tetanulo)~=0
        for nmedtetaNulo = 1:size(tetanulo_info,1)
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtCh(nmedtetaNulo,:) = [11  nmedtetaNulo   0    tetanulo_info(nmedtetaNulo,1:3)    10e-4*sigma];
        end
    end
     %%
    if sum(med_tnulo)~=0
        for nmedtNulo = 1:size(tnulo_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedAtCh(nmedtetaNulo+nmedtNulo,:) = [13  nmedtNulo   0    tnulo_info(nmedtNulo,1:3)   10e-4*sigma];
        end
    end
    
%    MedAt = [MedAtConv;MedAtCh];
     MedAt = [MedAtConv];

    
  %%
  
nmedFluReat = 0;
nmedFluReatCh = 0;
nmedInjReat = 0;
nmedv = 0;
nmedvNulo = 0;
nmeduNulo = 0;

%     fprintf('\n \n');
%     disp('================================================================');
%     disp('                     MEDIDAS REATIVAS                             ');
%     disp('================================================================');
%     %%
%%
    if sum(med_u)~=0
        for nmedFluReat = 1:size(u_info,1)
             % %         Medidas                      tipo                      n               i    j         valor (pu) cov(pu)(sqrt(R))  
             MedReatConv(nmedFluReat,:) = [   3  nmedFluReat   0    u_info(nmedFluReat,1:3)   8*sigma ];
        end
    end
      %%
    if sum(med_ud)~=0
        for nmedFluReatCh = 1:size(ud_info,1)
            % %         Medidas                      tipo                      n               i    j         valor (pu) cov(pu)(sqrt(R))  
            MedReatConv(nmedFluReat+nmedFluReatCh,:) = [4  nmedFluReatCh  0  ud_info(nmedFluReatCh,1:3)      8*sigma];
        end
    end
    %%
    if sum(med_q)~=0
        for nmedInjReat = 1:size(q_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedReatConv(nmedFluReat+nmedFluAtCh+nmedInjReat,:) = [7  nmedInjReat   q_info(nmedInjReat,1) 0  0  q_info(nmedInjReat,2)    10*sigma];
        end
    end
    %%
    if sum(med_v)~=0 
        for nmedv = 1:size(v_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedReatConv(nmedFluReat+nmedFluAtCh+nmedInjReat+nmedv,:) = [10  nmedv   v_info(nmedv,1) 0 0    v_info(nmedv,2)    0.1*sigma];
        end
    end
      %%
    if sum(med_i)~=0
        for nmediimagReat = 1:size(i_imag_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedReatConv(nmedFluReat+nmedFluAtCh+nmedInjReat+nmedv+nmediimagReat,:) = [17  nmediimagReat   0    i_imag_info(nmediimagReat,1:2)  i_imag_info(nmediimagReat,3)    0.1*sigma];
        end
    end
          %%
    if sum(med_Vnulo)~=0
        for nmedvNulo = 1:size(tetanulo_info,1)
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedReatCh(nmedvNulo,:) = [12  nmedvNulo   0    Vnulo_info(nmedvNulo,1:3)    10e-4*sigma];
        end
    end
     %%
    if sum(med_unulo)~=0
        for nmeduNulo = 1:size(unulo_info,1);
            % %                                 Medidas          tipo         n            i    j       valor (pu) cov(pu)(sqrt(R))  
            MedReatCh(nmedvNulo+nmeduNulo,:) = [14  nmeduNulo   0    unulo_info(nmeduNulo,1:3)   10e-4*sigma];
        end
    end
    %%
%     MedReat = [MedReatConv;MedReatCh];
      MedReat = [MedReatConv];

     Medidas = [MedAt;MedReat];
    
%     fprintf('\n\n');
%     disp('TIPOS DE MEDIDAS')
%     disp('1 - FLUXO DE POT�NCIA ATIVO NOS RAMOS CONVENCIONAIS')
%     disp('2 - FLUXO DE POT�NCIA ATIVO NOS RAMOS CHAVE�VEIS')
%     disp('3 - FLUXO DE POT�NCIA REATIVO NOS RAMOS CONVENCIONAIS')
%     disp('4 - FLUXO DE POT�NCIA REATIVO NOS RAMOS CHAVE�VEIS')
%     disp('5 - INJE��O DE POT�NCIA ATIVA')
%     disp('6 - INJE��O DE POT�NCIA ATIVA NULA')
%     disp('7 - INJE��O DE POT�NCIA REATIVA')
%     disp('8 - INJE��O DE POT�NCIA REATIVA NULA')
%     disp('9 - �NGULO DAS BARRAS DE REFER�NCIA')
%     disp('10 - M�DULO DA TENS�O')
%     disp('11 - DIFEREN�A ANGULAR NULA (DISJUNTORES FECHADOS)')    
%     disp('12 - DIFEREN�A DE POTENCIAL NULA (DISJUNTORES FECHADOS)')
%     disp('13 - FLUXO DE POT�NCIA ATIVA NULA (DISJUNTORES ABERTOS)')    
%     disp('14 - FLUXO DE POT�NCIA REATIVA NULA (DISJUNTORES ABERTOS)')
%     disp('15 - �NGULO DAS BARRAS')

%           
%     fprintf('\n\n');
%     disp('==========================================================================================');
%     disp('                                    DADOS DE MEDIDAS                           ');
%     disp('==========================================================================================');
%     disp('      TIPO          N          BARRA        DE         PARA      MEDIDA(pu)  COV-(sqrt(R))') 
%     for n = 1:nmedidas
%     fprintf('  %7d        %4d         %4d        %4d        %4d      %9.3f       %7.3f \n', Medidas(n,1), Medidas(n,2),Medidas(n,3),Medidas(n,4),Medidas(n,5),Medidas(n,6),Medidas(n,7))
%     end
%     clear p_info   q_info   t_info   u_info   v_info   td_info   ud_info
%     fprintf('\n\n');
    