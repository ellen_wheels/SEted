function [z] = monta_Zzao_SE(Medidas, Linha) 

% Z - matriz de valores medidos


% %% Arquivo de dados
% if arquivo == 24
%    sist24bns
% elseif arquivo == 30
%     sist30bns 
% elseif arquivo == 302
%     sist30bnsCaso2   
% else
%     input('Digite o nome do arquivo de dados >> ')
%     disp(' ')
% end
%%
tipo = Medidas(:,1);
i = Medidas(:,3);
k = Medidas (:,4);
m = Medidas (:,5);
medida = Medidas (:,6);

nmedidas = size(tipo,1);

% nramos
nramos = size(Linha(:,1),1);

x = Linha (:,5); % reatancia

%% matriz auxiliar x 

xaux = [Linha(:,2) Linha(:,3) Linha(:,5)]; 

for cont = 1:nramos
    w = xaux(cont,1);
    z = xaux(cont,2);
    xdisj(w,z) = xaux(cont,3);
end

%% vetores de medidas
pkm = [];
pmk = [];
qkm = [];
qmk = [];
pinj = [];
qinj = [];
V = [1];
teta_ref = [];
% ramos chave�veis
pkmd = [];
qkmd = [];
pnula = [];
qnula = [];
delta_teta = [];
delta_V = [];
tkm = [];
ukm = [];

%% contadores vetores de medidas
ypkm = 0;
ypmk = 0;
yqkm = 0;
yqmk = 0;
ypinj = 0;
yqinj = 0;
yv = 0;
yteta = 0;
% ramos chave�veis
ypkmd = 0;
yqkmd = 0;
ypnula = 0;
yqnula = 0;
ytetanulo = 0;
yVnulo = 0;
ytkm = 0;
yukm = 0;

%%
for aux = 1:nmedidas;
%% MEDIDAS DE FLUXO DE POT�NCIA ATIVO EM RAMOS CONVENCIONAIS
    if tipo (aux) == 1; 
        if k(aux) <= m(aux) %fluxo da barra k para a barra m 
                ypkm = ypkm+1;
                pkm(ypkm,1) = medida(aux);
            else %fluxo da barra m para a barra k
                ypmk = ypmk+1;
                pmk(ypmk,1) = medida(aux);
        end
    end

%% MEDIDAS DE FLUXO DE POT�NCIA REATIVO EM RAMOS CONVENCIONAIS    
    if tipo(aux) == 2; %medidas de fluxo reativo    
        if k(aux) <= m(aux) %fluxo da barra k para a barra m
                    yqkm = yqkm+1;
                    qkm(yqkm,1) = medida(aux);
            else %fluxo da barra m para a barra k
                    yqmk = yqmk+1;
                    qmk(yqmk,1) = medida(aux);
        end
    end    

%% MEDIDAS DE INJE��O DE POT�NCIA ATIVA
     if tipo(aux) == 3; 
             ypinj = ypinj+1;
             pinj(ypinj,1) = medida(aux);
     end
%% MEDIDAS DE INJE��O DE POT�NCIA REATIVA    
    if tipo(aux) == 4; 
            yqinj = yqinj+1;
            qinj(yqinj,1)= medida(aux); 
    end


%% MEDIDA DO M�DULO DE TENS�O
    if tipo(aux) == 5; 
            yv = yv+1;
            V(yv,1) = medida(aux);  
    end
        
%%
zA = [  pkm; % fluxo de pot�ncia ativa de k para m
        pmk; % fluxo de pot�ncia ativa de m para k
        pinj]; % inje��o de pot�ncia ativa]; 

zR = [ qkm; % fluxo de pot�ncia reativa de k para m
       qmk; % fluxo de pot�ncia reativa de m para k
       qinj; % inje��o de pot�ncia reativa
       V]; % m�dulo da tens�o
            
z=[zA;zR];
end