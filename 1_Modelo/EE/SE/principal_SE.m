%% ARQUIVO PRINCIPAL

%%
%    disp('###########################################################################')
% fprintf('#                                                                         #\n');
%    disp('#                    ESTIMADOR DE ESTADOS DESACOPLADO                     #')
% fprintf('#                                                                         #\n');
%    disp('###########################################################################')

%% VERS�ES
% 03/05/16 - Atualiza��o da matriz Jacobiana nas itera��es
% Arquivos alterados: monta_delta_zlinha_SE, monta_delta_zAlinha_SE e
% monta_delta_zRlinha_SE
% Arquivos criados: monta_HAA_SE e monta_HRR_SE

%%
% vetorL = 1:1:size(linha,1);
% vetorB = 1:1:size(barra,1);
% Linha = [vetorL' linha];

%%
nref = length(ref);
nbar = max(linha(:,2));
nramos = size(Linha(:,1),1);
nl = nramos-nd;
%par�metros iniciais
V(1:nbar,1)=1;  % V0 - tens�o inicial
teta(1:nbar,1)=0; % teta0 - �ngulo inicial
Vnew(1:nbar,1)=1; % tens�o estimada na itera��o

%%
% % n�vel SE
t_est(1:nd,1) = 0; % fluxo de pot�ncia ativa inicial nos disjuntores
u_est(1:nd,1) = 0; % fluxo de pot�ncia reativa inicial nos disjuntores

%%
matrizes_SE;
state = zeros(nr,1);
Ikm = 0;


%%
%[zA,zR] = monta_Zzao_SE(Medidas, Linha, arquivo);
[RA,RR] = monta_Rzao_SE(Medidas, Linha);

[ HAA, HRR ] = monta_Hzao_SE(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from);
[ haa, hrr ] = monta_hzinho_SE(V, teta, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, t_est, u_est, state, to, from, nd);
[ GAA, GRR ] = monta_G_SE(HAA, RA, HRR, RR);
[ delta_zA,  delta_zR ] = monta_deltaz_SE(zA, zR, haa, hrr);
[ delta_zAlinha,  delta_zRlinha ] = monta_delta_zlinha_SE(delta_zA, delta_zR, V, Medidas);
[ TA ] = monta_TA_SE(HAA, RA, delta_zAlinha);
[ TR ] = monta_TR_SE(HRR, RR, delta_zRlinha);
deltaAt = inv(GAA)*TA;
deltaReat = inv(GRR)*TR;

deltaAteq = [0;
            deltaAt];
if nref ~= 1
    for aux = 2:nref
        bref = ref(aux);
    deltaAteq = [deltaAteq(1:bref-1);
                 0
                 deltaAteq(bref+1:nd+nbar)];
    end    
end
tetanew=teta+deltaAteq(1:nbar); % teta estimado na itera��o
tnew=t_est+deltaAteq(1+nbar:nd+nbar); % teta estimado na itera��o

contativa=1;
contreativa=0;
contit = 0.5;
%%
ativo=max(abs(deltaAt));
reativo=max(abs(deltaReat));

if ((ativo>tol)||(reativo>tol)) %inicia o processo iterativo se o criterio ainda n�o foi atingido
    V;
    (tetanew./pi).*180;
    u_est;
    tnew;
    
 Vnew = V;
 unew = u_est;
     
while((ativo>tol)||(reativo>tol))

         % meia itera��o reativa
            if(reativo>tol)
           
                [zR] = monta_ZR_mod_SE(Medidas, Linha, nramos, tetanew, Ikm);

%                 [ HRR ] = monta_HRR_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from);
           [ hrr ] = monta_hzinhorr_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);
           [ delta_zR ] = monta_deltazR_SE(zR, hrr);
           [ delta_zRlinha ] = monta_delta_zRlinha_SE(delta_zR, V, Medidas);
           [ TR ] = monta_TR_SE(HRR, RR, delta_zRlinha);

                %atualizar V
                deltaReat = inv(GRR)*TR;
                Vnew=Vnew+deltaReat(1:nbar); %vai passar a ser uma matriz [Vnew; tkmnew] = [Vnew; tkmnew] + [deltaV deltatkm]
                if nd~=0
                unew=unew+deltaReat(1+nbar:nd+nbar); % teta estimado na itera��o
                end

                contreativa=1+contreativa;
                Vnew;
                (tetanew./pi).*180;
                if nd~=0
                unew;
                tnew;
                end

            contit = contit +0.5; %atualizar contador da itera��o
            reativo=max(abs(deltaReat));
            
             end

            
            if(ativo>tol)

               [zA] = monta_ZA_mod_SE(Medidas, Linha, nramos, tetanew, Ikm);
            
%                 [ HAA ] = monta_HAA_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from);    
           [ haa ] = monta_hzinhoaa_SE(Vnew, tetanew, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, tnew, unew, state, to, from, nd);
           [ delta_zA ] = monta_deltazA_SE(zA, haa);
           [ delta_zAlinha ] = monta_delta_zAlinha_SE(delta_zA, V, Medidas);
           [ TA ] = monta_TA_SE(HAA, RA, delta_zAlinha);

                %atualizar teta
                deltaAt = inv(GAA)*TA;
                deltaAteq = [zeros(size(nref,1));
                                deltaAt];
                tetanew=tetanew+deltaAteq(1:nbar); % teta estimado na itera��o
                tnew=tnew+deltaAteq(1+nbar:nd+nbar); % teta estimado na itera��o

                contativa=1+contativa;
                Vnew;
                (tetanew./pi).*180;
                if nd~=0
                unew;
                tnew;
                end
                
            contit = contit +0.5; %atualizar contador da itera��o
            ativo=max(abs(deltaAt));
           
            end
            
            
            % encerra o processo iterativo
                if ((ativo<tol)||(reativo<tol))
                   break;
                end
                if contit>=30
                    fprintf('\n');
                    disp('================================================');
                    disp('O programa atingiu 30 itera��es e n�o convergiu.');
                    disp('================================================');
                break;
                end
end  
else  %caso os criterios tenham sido atingidos, fornece a resposta.
        disp('RESULTADO') 
        Vnew;
         (tetanew./pi).*180;
         if nd~=0
             unew = u_est;
         unew;
         tnew;
         end

end

% calc_corrente
%%

% clearvars -except t_est    tempo    teta  tnew   ...        
% to   na       nbar   nramos      u_est          ...
% nb      nd     nl   nr    from    unew   ...         
% ativo    reativo    contit contativa  contreativa  Vnew  tetanew

