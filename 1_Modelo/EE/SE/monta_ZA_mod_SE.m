function [zA] = monta_ZA_mod_SE(Medidas, linha, nramos, tetamod,Ikm) 

% Z - matriz de valores medidos

%%
tipo = Medidas(:,1);
i = Medidas(:,3);
k = Medidas (:,4);
m = Medidas (:,5);
medida = Medidas (:,6);

nmedidas = size(tipo,1);

% G=matrizG;
% B=matrizB;
% g=-G;
% b=-B;
% bsh=matrizbshkm; %bsh de linha

% nramos
% nramos = size(linha(:,1),1);

x = linha (:,5); % reatancia

%% matriz auxiliar x 

xaux = [linha(:,2) linha(:,3) linha(:,5)]; 

for cont = 1:nramos
    w = xaux(cont,1);
    z = xaux(cont,2);
    xdisj(w,z) = xaux(cont,3);
end

%% vetores de medidas
pkm = [];
pmk = [];
qkm = [];
qmk = [];
pinj = [];
qinj = [];
V = [];
teta_ref = [];
teta = [];
i_real = [];
% ramos chave�veis
pkmd = [];
qkmd = [];
pnula = [];
qnula = [];
delta_teta = [];
delta_V = [];
tkm = [];
ukm = [];

%% contadores vetores de medidas
ypkm = 0;
ypmk = 0;
yqkm = 0;
yqmk = 0;
ypinj = 0;
yqinj = 0;
yv = 0;
yteta_ref = 0;
yteta = 0;
yireal = 0;
% ramos chave�veis
ypkmd = 0;
yqkmd = 0;
ypnula = 0;
yqnula = 0;
ytetanulo = 0;
yVnulo = 0;
ytkm = 0;
yukm = 0;

%%
for aux = 1:nmedidas;
%% MEDIDAS DE FLUXO DE POT�NCIA ATIVO EM RAMOS CONVENCIONAIS
    if tipo (aux) == 1; 
        if k(aux) <= m(aux) %fluxo da barra k para a barra m 
                ypkm = ypkm+1;
                pkm(ypkm,1) = medida(aux);
            else %fluxo da barra m para a barra k
                ypmk = ypmk+1;
                pmk(ypmk,1) = medida(aux);
        end
    end
%% MEDIDAS DE FLUXO DE POT�NCIA ATIVO EM RAMOS CHAVE�VEIS
    if tipo (aux) == 2;
        w = k(aux);
        z = m(aux);
        if (xdisj(w,z)==0||xdisj(w,z)==9999)% verifica se � um ramo chave�vel
            ypkmd = ypkmd+1;
            pkmd(ypkmd,1) = medida(aux);
        end
    end
% %% MEDIDAS DE FLUXO DE POT�NCIA REATIVO EM RAMOS CONVENCIONAIS    
%     if tipo(aux) == 3; %medidas de fluxo reativo    
%         if k(aux) <= m(aux) %fluxo da barra k para a barra m
%                     yqkm = yqkm+1;
%                     qkm(yqkm,1) = medida(aux);
%             else %fluxo da barra m para a barra k
%                     yqmk = yqmk+1;
%                     qmk(yqmk,1) = medida(aux);
%         end
%     end    
% %% MEDIDAS DE FLUXO DE POT�NCIA REATIVO EM RAMOS CHAVE�VEIS
%     if tipo(aux) == 4; %medidas de fluxo reativo
%         w = k(aux);
%         z = m(aux);
%         if (xdisj(w,z)==0||xdisj(w,z)==9999)% verifica se � um ramo chave�vel
%             yqkmd = yqkmd+1;
%             qkmd(yqkmd,1) = medida(aux);
%         end
%     end
%% MEDIDAS DE INJE��O DE POT�NCIA ATIVA
     if tipo(aux) == 5; 
             ypinj = ypinj+1;
             pinj(ypinj,1) = medida(aux);
     end
%% MEDIDAS DE INJE��O DE POT�NCIA ATIVA NULA
    if tipo(aux) == 6;
             ypnula = ypnula+1;
             pnula(ypnula,1) = medida(aux);
    end
% %% MEDIDAS DE INJE��O DE POT�NCIA REATIVA    
%     if tipo(aux) == 7; 
%             yqinj = yqinj+1;
%             qinj(yqinj,1)= medida(aux); 
%     end
% %% MEDIDAS DE INJE��O DE POT�NCIA REATIVA NULA    
%     if tipo(aux) == 8;
%             yqnula = yqnula+1;
%             qnula(yqnula,1) = medida(aux);
%     end

%% MEDIDAS DE CORRENTE PARTE REAL/ATIVA
    if tipo(aux) == 16;
             yireal = yireal+1;
             z = m(aux);
             i_real(yireal,1) = abs(Ikm(yireal,3))*cos(angle(Ikm(yireal,3))-tetamod(z));
    end

%% RESTRI��O DE BARRA DE REFER�NCIA
    if tipo(aux) == 9;
            yteta_ref = yteta_ref+1;
            teta_ref (yteta_ref,1) = medida(aux);
    end
% %% MEDIDA DO M�DULO DE TENS�O
%     if tipo(aux) == 10; 
%             yv = yv+1;
%             V(yv,1) = medida(aux);  
%     end
%% MEDIDA DE �NGULO
    if tipo(aux) == 15; 
            yteta = yteta+1;
            teta(yteta,1) = medida(aux);  
    end
%% RESTRI��O DE DIFEREN�A ANGULAR NULA EM RAMOS CHAVE�VEIS FECHADOS
    if tipo(aux) == 11; 
            ytetanulo = ytetanulo+1;
            delta_teta(ytetanulo,1) = medida(aux);
    end 
% %% RESTRI��O DE DIFEREN�A DE POTENCIAL NULA EM RAMOS CHAVE�VEIS FECHADOS
%     if tipo(aux) == 12;
%             yVnulo = yVnulo+1;
%             delta_V(yVnulo,1) = medida(aux);
%     end 
%% RESTRI��O DE FLUXO DE POT�NCIA ATIVO NULO EM RAMOS CHAVE�VEIS ABERTOS
    if tipo(aux) == 13;
            ytkm = ytkm+1;
            tkm(ytkm,1) = medida(aux);
    end 
% %% RESTRI��O DE FLUXO DE POT�NCIA REATIVO NULO EM RAMOS CHAVE�VEIS ABERTOS 
%     if tipo(aux) == 14;
%             yukm = yukm+1;
%             ukm(yukm,1) = medida(aux);
%     end
        
%%
zA = [  pkm; % fluxo de pot�ncia ativa de k para m
        pmk; % fluxo de pot�ncia ativa de m para k
        pkmd; % fluxo de pot�ncia em ramos chave�veis de k para m
        % pmkd; % fluxo de pot�ncia em ramos chave�veis de m para k
        pinj; % inje��o de pot�ncia ativa
        teta; % �ngulo 
        pnula; % inje��o de pot�ncia ativa nula
        i_real; % parte real do fasor da corrente
        teta_ref; % numero de barras de refer�ncia
        delta_teta; % diferen�a angular nula - disjuntores fechados
        tkm]; % fluxo de pot�ncia ativo nulo - disjuntores abertos

% zR = [ qkm; % fluxo de pot�ncia reativa de k para m
%        qmk; % fluxo de pot�ncia reativa de m para k
%        qkmd; % fluxo de pot�ncia em ramos chave�veis de k para m
%        % qmkd; % fluxo de pot�ncia em ramos chave�veis de m para k
%        qinj; % inje��o de pot�ncia reativa
%        V; % m�dulo da tens�o
%         i_imag; % parte imagin�ria do fasor da corrente
%        qnula; % inje��o de pot�ncia reativa nula
%        delta_V; % diferen�a de potencial nula - disjuntores fechados
%        ukm]; % fluxo de pot�ncia reativo nulo - disjuntores abertos
%             

end