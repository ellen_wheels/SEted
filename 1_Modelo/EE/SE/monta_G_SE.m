function [ GAA, GRR ] = monta_G_SE(HAA, RA, HRR, RR)
% ESTA FUN��O CALCULA O GAA=HAA'*RA^-1*HAA
%                       GRR=HRR'*RR^-1*HRR

GAA=HAA'*inv(RA)*HAA;
GRR=HRR'*inv(RR)*HRR;

end