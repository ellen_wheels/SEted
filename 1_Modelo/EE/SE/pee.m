 

% Arquivo criado em 17/10/17 - adapta��o para tirar o monta_z fora do principal do EE

%%
vetorL = 1:1:size(linha,1);
vetorB = 1:1:size(barra,1);
Linha = [vetorL' linha];

%%
nref = length(ref);
nbar = max(linha(:,2));
nramos = size(Linha(:,1),1);
nl = nramos-nd;

%% Calculo da matriz Y, G, B, gkm e bkm

na = Linha(:,2); % barra de
nb = Linha (:,3); % barra para
r = Linha (:,4); % resistencia
x = Linha (:,5); % reatancia
bsh = (Linha (:,6)/2); % shunt de linha
bshb = barra (:,9); % shunt de barra
bshlin = (Linha (:,6)/2); % shunt de linha

