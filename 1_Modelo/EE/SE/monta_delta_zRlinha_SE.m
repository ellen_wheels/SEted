function [ delta_zRlinha ] = monta_delta_zRlinha_SE( delta_zR, V, Medidas)

%% VERS�ES
% 03/05/16 - V n�o divide restri��es referente aos ramos chave�veis (abertos/fechados)

%%

tipo = Medidas(:,1);
n = Medidas (:,3);
i = Medidas (:,4);
j = Medidas (:,5);
nmedidas = size(tipo,1);

nfluatkm = 0; nfluatmk = 0;
nfluatkmd=0;
nflureatkm = 0; nflureatmk = 0;
nflureatkmd = 0;
ninjat = 0;
nteta = 0;
nireal = 0;
niimag = 0;
ninjatnula = 0;
ninjreat = 0;
ninjreatnula = 0;
nbref = 0;
nv = 0;
ntetanulo = 0;
nVnulo = 0;
ntnulo = 0;
nunulo = 0;

Vat=[];
Vatkm=[];
Vatmk=[];
Vatkmd = [];
Vteta = [];
Vatnula = [];
Vireal = [];
Viimag = [];
Vreat=[];
Vreatkm=[];
Vreatmk=[];
Vreatkmd=[];
Vreatnula=[];
Vref = [];
Vv=[];
Vtetanulo = [];
Vnulo = [];
Vtnulo = [];
Vunulo = [];


for cont = 1:nmedidas
    if tipo (cont)==1; %fluxo ativo
        if i(cont)<=j(cont)
        
        nfluatkm = nfluatkm + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vatkm(nfluatkm) = V(k);
        
        else
        
        nfluatmk = nfluatmk + 1;
        Vatmk(nfluatmk) = V(m);
        
        end
    end
    %%
    if tipo(cont)==2 % fluxo nas chaves
        if i(cont)<=j(cont)
        nfluatkmd = nfluatkmd + 1;
        
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vatkmd(nfluatkmd) = V(k);
        end
    end
    %%
    if tipo (cont)==3; %fluxo reativo
        if i(cont)<=j(cont)
        
        nflureatkm = nflureatkm + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vreatkm(nflureatkm) = V(k);
        else
        
        nflureatmk = nflureatmk + 1;
        Vreatmk(nflureatmk) = V(m);
        end
    end
    %%
    if tipo(cont)==4 % fluxo nas chaves
        if i(cont)<=j(cont)
        nflureatkmd = nflureatkmd + 1;
        
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vreatkmd(nflureatkmd) = V(k);
        end
    end
    %%
    if tipo (cont)==5; %inje��o ativa
       k=n(cont);  
       ninjat = ninjat + 1;
       
       Vat(ninjat) = V(k);
    end
    %%
    if tipo (cont)==6; %inje��o nula
       k=n(cont);  
       ninjatnula = ninjatnula + 1;
       
       Vatnula(ninjatnula) = V(k);
    end
    %%
    if tipo (cont)==7; %inje��o reativa
       k=n(cont);  
       ninjreat = ninjreat + 1;
       
       Vreat(ninjreat) = V(k);
    end
    %%
    if tipo (cont)==8; %inje��o nula
       k=n(cont);  
       ninjreatnula = ninjreatnula + 1;
       
       Vreatnula(ninjreatnula) = V(k);
    end
    %%
    if tipo (cont)==9; % barra de referencia
       k=n(cont);  
       nbref = nbref + 1;
       
       Vref(nbref) = V(k);
    end
    %%
    if tipo (cont)==10; %tens�o
       k=n(cont);  
       nv = nv + 1;
       
       Vv(nv) = V(k);
       
    end
      %%
    if tipo (cont)==15; %tens�o
       k=n(cont);  
       nteta = nteta + 1;
       
       Vteta(nteta) = V(k);
       
    end

  %%
    if tipo (cont)==16; %tens�o
        if i(cont)<=j(cont)

        nireal = nireal + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vireal(nireal) = V(k);
        
        end
       
    end

  %%
    if tipo (cont)==17; %tens�o
        if i(cont)<=j(cont)

        niimag = niimag + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Viimag(niimag) = V(k);
        
        end
       
    end
        %%
    if tipo (cont)==11; %dif angular
       ntetanulo = ntetanulo + 1;
       
       Vtetanulo(ntetanulo) = 1;
    end
    %%
    if tipo (cont)==12; %dif potencial
       nVnulo = nVnulo + 1;
       
       Vnulo(nVnulo) = 1;
    end
    %%
    if tipo (cont)==13; % fluxo nulo
       ntnulo = ntnulo + 1;
       
       Vtnulo(ntnulo) = 1;
    end
    %%
    if tipo (cont)==14; % fluxo nulo
       nunulo = nunulo + 1;
       
       Vunulo(nunulo) = 1;
    end
end

%%
% Va = [Vatkm';
%       Vatkmd';
%       Vatmk';
%       Vat';
%       Vteta';
%       Vatnula';
%       Vireal';
%       Vref';
%       Vtetanulo';
%       Vtnulo'];
  
Vr = [Vreatkm';
      Vreatmk';
      Vreatkmd';
      Vreat';
      Vreatnula';
      Vv';
      Viimag';
      Vnulo';
      Vunulo'];


% delta_zAlinha = delta_zA./Va;
delta_zRlinha = delta_zR./Vr;

end