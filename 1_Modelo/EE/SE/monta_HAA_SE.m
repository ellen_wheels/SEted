function [HAA] = monta_HAA_SE(V, teta, matrizG, matrizB, matrizbshkm, Linha, Medidas, nd, state, to, from)

%% VERS�ES
% 03/05/16 - Cria��o do arquivo. Atualiza��o da Jacobiana a cada itera��o.
% 05/11/18 - Altera��o para medidas de corrente

%%
% Monta Estrutura da Matriz hzinho
G=matrizG;
B=matrizB;
g=-G;
b=-B;
bsh=matrizbshkm; %bsh de linha

nb1 = max(Linha (:,2));
nb2 = max(Linha (:,3));
nBarras = max(nb1,nb2);
tipo = Medidas(:,1);		    	% tipo das barras
nmedidas = size(tipo,1);

n = Medidas (:,3);
i = Medidas (:,4);
j = Medidas (:,5);

disj = [from' to' state];
% nramos
nramos = size(Linha(:,1),1);
% n�mero de linhas (convencionais)
nl = nramos-nd;

%% MEDIDAS ATIVAS
nfluatkm = 0; nfluatmk = 0; % quantidade de medidas de fluxo de pot�ncia ativa nos ramos convencionais
nfluatkmd = 0; nfluatmkd = 0; % quantidade de medidas de fluxo de pot�ncia ativa nos ramos chave�veis
ninjat = 0; % quantidade de medidas de inje��o de pot�ncia ativa 
ninjatnula = 0; % quantidade de medidas de inje��o de pot�ncia ativa nula
nteta = 0; % quantidade de medidas de �ngulo
nbref = 0; % quantidade de barras de refer�ncia
nireal = 0; % quantidade de medidas de fasor da corrente parte real
ntetanulo = 0; % quantidade de disjuntores fechados
ntnulo = 0; % quantidade de disjuntores abertos

dPkm_dteta=[]; dPmk_dteta=[]; dPkm_dt=[]; dPmk_dt=[]; % derivada das medidas de fluxo de pot�ncia ativa nos ramos convencionais
dPkmd_dteta=[]; dPmkd_dteta=[]; dPkmd_dt=[]; dPmkd_dt=[]; % derivada das medidas de fluxo de pot�ncia ativa nos ramos chave�veis
dPinj_dteta=[]; dPinj_dt=[]; % derivada das medidas de inje��o de pot�ncia ativa
dPinjnula_dteta=[]; dPinjnula_dt=[]; % derivada das medidas de inje��o de pot�ncia ativa nula
dteta_dteta=[]; dteta_dt=[];  % derivada dos �ngulos
dBref_dteta=[]; dBref_dt=[]; % derivada das barras de refer�ncia
direal_dteta=[]; direal_dt=[];  % derivada de medidas de fasor da corrente parte real
dtetanulo_dteta=[]; dtetanulo_dt=[]; % derivada da diferen�a angular nula para disjuntores fechados
dtnulo_dteta=[]; dtnulo_dt=[]; % derivada do fluxo ativo para disjuntores abertos

%% MEDIDAS REATIVAS
nflureatkm = 0; nflureatmk = 0; % quantidade de medidas de fluxo de pot�ncia reativa nos ramos convencionais
nflureatkmd = 0; nflureatmkd = 0; % quantidade de medidas de fluxo de pot�ncia reativa nos ramos chave�veis
ninjreat = 0; % quantidade de medidas de inje��o de pot�ncia reativa 
ninjreatnula = 0; % quantidade de medidas de inje��o de pot�ncia reativa nula
nv = 0; % quantidade de medidas de m�dulo de tens�o
niimag = 0; % quantidade de medidas de fasor da corrente parte imagin�ria
nVnulo = 0; % quantidade de disjuntores fechados
nunulo = 0; % quantidade de disjuntores abertos

dQkm_dV=[]; dQmk_dV=[]; dQkm_du=[]; dQmk_du=[]; % derivada das medidas de fluxo de pot�ncia reativa nos ramos convencionais
dQkmd_dV=[]; dQmkd_dV=[]; dQkmd_du=[]; dQmkd_du=[]; % derivada das medidas de fluxo de pot�ncia reativa nos ramos chave�veis
dQinj_dV=[];  dQinj_du=[]; % derivada das medidas de inje��o de pot�ncia reativa
dQinjnula_dV=[]; dQinjnula_du=[]; % derivada das medidas de inje��o de pot�ncia reativa nula
dV_dV=[]; dV_du=[];  % derivada dos m�dulos de tens�o
diimag_dV=[]; diimag_du=[];  % derivada de medidas de fasor da corrente parte imagin�ria
dVnulo_dV=[]; dVnulo_du=[]; % derivada da diferen�a de potencial nula para disjuntores fechados
dunulo_dV=[]; dunulo_du=[]; % derivada do fluxo reativo para disjuntores abertos

for cont = 1:nmedidas
% Calculo da derivada parcial de Pkm e Pmk

if tipo(cont)==1;
     
    if i(cont)<=j(cont)
        
        nfluatkm = nfluatkm + 1;
          
        
        k= i(cont);      % barra de
        m= j(cont);      % barra para
    
            for aux=1:nBarras            
                                        
                if k==aux

                dPkm_dteta(nfluatkm,aux)= V(k)*V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                    -b(k,m)*cos(teta(k)-teta(m)));

                elseif m==aux    

                dPkm_dteta(nfluatkm,aux)= -V(k)*V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                    -b(k,m)*cos(teta(k)-teta(m)));

                else

                dPkm_dteta(nfluatkm,aux)=0; 
                
                end
            end
    else
        
        nfluatmk = nfluatmk + 1;
        
        for aux=1:nBarras        
                        
                if k==aux

                dPmk_dteta(nfluatmk,aux)= V(k)*V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

                elseif m==aux    

                dPmk_dteta(nfluatmk,aux)= -V(k)*V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

                else

                dPmk_dteta(nfluatmk,aux)=0; 

                end
        end
   end
end

% Derivadas dos fluxos convencionais pelos fluxos nos ramos chave�veis
    
if nfluatkm ~= 0
    if nd~=0
    dPkm_dt(nfluatkm,nd)=0; 
    end
end
if nfluatmk ~= 0
    if nd~=0
    dPmk_dt(nfluatmk,nd)=0;
    end
end

%% Fluxo ativo nos ramos chave�veis

if tipo(cont)==2; 
    
    nfluatkmd = nfluatkmd + 1;
    
    dPkmd_dt(nfluatkmd,nd) = 0;
    
        if i(cont)<=j(cont)
            
             for aux=1:nd

                    if (i(cont)==disj(aux+nl,1)&&j(cont)==disj(aux+nl,2))

                             jj = state(aux+nl);

                              dPkmd_dt (nfluatkmd,jj)= 1; 

                    end
             end

        elseif  j(cont)<=i(cont)
            
             for aux=1:nd

                    if (j(cont)==disj(aux+nl,1)&&i(cont)==disj(aux+nl,2))

                             jj = state(aux+nl);

                            dPkmd_dt (nfluatkmd,jj) = -1;
                    end
             end

        else

            dPkmd_dt (nfluatkmd,aux)= 0;

        end    
      
end

% Derivadas dos fluxos chave�veis pelos fluxos nos ramos convencionais 

if nfluatkmd ~= 0 
    if nd~=0
    dPkmd_dteta(nfluatkmd,nBarras) = 0;
    end
end

%% Calculo da derivda parcial de Qkm e Qmk

if tipo(cont)==3;
 
    if i(cont)<=j(cont)
        
       nflureatkm = nflureatkm + 1;
       
        for aux=1:nBarras                    % esse aux representa a posi��o no vetor x transposto

               k= i(cont);      % barra de
               m= j(cont);      % barra para

               
                if k==aux

                    dQkm_dV(nflureatkm,aux)= -V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                        -b(k,m)*cos(teta(k)-teta(m)))-2*V(k)*(b(k,m)+bsh(k,m));

                elseif m==aux    

                    dQkm_dV(nflureatkm,aux)= -V(k)*(g(k,m)*sin(teta(k)-teta(m))...
                        -b(k,m)*cos(teta(k)-teta(m)));

                else

                   dQkm_dV(nflureatkm,aux)=0;

                end
        end

    else
        
        nflureatmk = nflureatmk + 1;
        
       for aux=1:nBarras             % esse aux representa a posi��o no vetor x transposto

               if k==aux

                dQmk_dV(nflureatmk,aux)= (-V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k))))-2*V(k)*(b(k,m)+bsh(k,m));

            elseif m==aux    

                dQmk_dV(nflureatmk,aux)= -V(k)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

            else

                dQmk_dV(nflureatmk,aux)=0;

               end
               
       end
       
    end
    % Derivadas dos fluxos convencionais pelos fluxos nos ramos chave�veis
    
    if nflureatkm ~= 0
        if nd~=0
        dQkm_du(nflureatkm,nd)=0; 
        end
    end
    if nflureatmk ~= 0
        if nd~=0
        dQmk_du(nflureatmk,nd)=0;
        end
    end
 
end
%% Fluxo reativo nos ramos chave�veis

if tipo(cont)==4; 
    
   nflureatkmd = nflureatkmd + 1;
   
   dQkmd_du (nflureatkmd,nd) = 0;
    
   for aux = 1:nd
       
        if i(cont)<=j(cont)

               if (i(cont)==disj(aux+nl,1)&&j(cont)==disj(aux+nl,2))
                      
                     jj = state(aux+nl);

                     dQkmd_du (nflureatkmd,jj)= 1; 
                end
    
        elseif  j(cont)<=i(cont)
            
               if (j(cont)==disj(aux+nl,1)&&i(cont)==disj(aux+nl,2))

                   jj = state(aux+nl);

                   dQkmd_du (nflureatkmd,jj) = -1;
                   
               end

        else

            dQkmd_du (nflureatkmd,aux) = 0;

        end
   end
end


% Derivadas dos fluxos chave�veis pelos fluxos nos ramos convencionais 

if nflureatkmd ~= 0
    if nd~=0
    dQkmd_dV (nflureatkmd,nBarras) = 0;
    end
end
    

%% Calculo da derivada parcial de Pinj, sendo  Pinj as inje��es de barra

if tipo (cont)==5;

       k=n(cont);  
       
       ninjat = ninjat + 1;

        for aux=1:nBarras   % esse aux representa a posi��o no vetor x transposto
                
                if aux==k
                
                somadP_dteta=0; 

                    for m=1:nBarras     

                        somadP_dteta=somadP_dteta+V(k)*V(m)*(-G(k,m)*sin(teta(k)-teta(m))+B(k,m)*cos(teta(k)-teta(m)));

                    end

                
                dPinj_dteta(ninjat,aux)=somadP_dteta-V(k)^2*B(k,k); 

                else              

                dPinj_dteta(ninjat,aux)= V(k)*V(aux)*(G(k,aux)*sin(teta(k)-teta(aux))-B(k,aux)*cos(teta(k)-teta(aux)));
            
                end
        end
            for aux=nl+1:nramos     
                    if (disj(aux,1)==k)% verifica se � um ramo chave�vel
                        dPinj_dt (ninjat,aux-nl) = 1; %ajustar
                    elseif (disj(aux,2)==k)
                        dPinj_dt (ninjat,aux-nl) = -1; %ajustar
                    else
                        dPinj_dt (ninjat,aux-nl) = 0; %ajustar
                    end
            end
        
end

%% Inje��o ativa nula

if tipo(cont)==6; 
    
    ninjatnula = ninjatnula + 1;
    
        for aux = 1:nBarras

             dPinjnula_dteta (ninjatnula,aux) = 0; 

        end
        if nd~=0
            for aux = 1:nd

                dPinjnula_dt (ninjatnula,aux) = 0;

            end
        end
    
end

%% Calculo da derivada parcial de Qinj, sendo  Qinj as inje��es de barra
 
if tipo(cont)==7;
 
    k = n(cont);   
    
    ninjreat =  ninjreat + 1;

        for aux=1:nBarras   % esse aux representa a posi��o no vetor x transposto
               
             if aux==k

              somadQ_dV=0;

                for m=1:nBarras      

                    somadQ_dV=somadQ_dV+V(m)*(G(k,m)*sin(teta(k)-teta(m))-B(k,m)*cos(teta(k)-teta(m)));
                end             
                   
                dQinj_dV(ninjreat,aux)= somadQ_dV-V(k)*B(k,k);

             else              

                dQinj_dV(ninjreat,aux)= V(k)*(G(k,aux)*sin(teta(k)-teta(aux))-B(k,aux)*cos(teta(k)-teta(aux)));             
             end
              
        end
            for aux=nl+1:nramos       
                    if (disj(aux,1)==k)% verifica se � um ramo chave�vel
                        dQinj_du (ninjreat,aux-nl) = 1; %ajustar
                    elseif disj(aux,2)==k
                        dQinj_du (ninjreat,aux-nl) = -1; %ajustar
                    else
                        dQinj_du (ninjreat,aux-nl) = 0; %ajustar
                    end
            end
end


%% Inje��o reativa nula

if tipo(cont)==8; 
    
    ninjreatnula = ninjreatnula + 1;
    
        for aux = 1:nBarras

             dQinjnula_dV (ninjreatnula,aux) = 0; 

        end
        if nd~=0
            for aux = 1:nd

                dQinjnula_du (ninjreatnula,aux) = 0;

            end
        end
    
end

%% Barra de refer�ncia

if tipo(cont)==9; 
    
        k = n(cont);
    
        nbref = nbref + 1;
        
        for aux=1:nBarras
            
            if (k==aux)
        
            dBref_dteta (nbref,aux) = 1;
            
            else
                
            dBref_dteta (nbref,aux) = 0;
            
            end
        
        end
        
        for aux = 1:nd

                dBref_dt (nbref,aux) = 0;
                
        end
end

%% Calculo da derivda parcial de Vmag

if tipo(cont)==10;

    k = n(cont);   

    nv = nv + 1;
    
    for aux=1:nBarras
            
            if (k==aux)

            dV_dV(nv,aux)=1;
            
            else
                
            dV_dV(nv,aux)=0;
            
            end
    end
        
            for aux = 1:nd

                dV_du (nv,aux) = 0;

            end
end

%% Calculo da derivda parcial de �ngulo

if tipo(cont)==15;

    k = n(cont);   

    nteta = nteta + 1;
    
    for aux=1:nBarras
            
            if (k==aux)

            dteta_dteta(nteta,aux)=1;
            
            else
                
            dteta_dteta(nteta,aux)=0;
            
            end
    end
        
            for aux = 1:nd

                dteta_dt (nteta,aux) = 0;

            end
end

%% Calculo da derivda parcial da parte real do fasor de corrente

if tipo(cont)==16;

    if i(cont)<=j(cont)
        
        nireal = nireal + 1;
                  
        k= i(cont);      % barra de
        m= j(cont);      % barra para
    
            for aux=1:nBarras 

                    if (k==aux)

                    direal_dteta(nireal,aux)=B(k,m);

                    elseif (m==aux)

                    direal_dteta(nireal,aux)=-B(k,m);
                    
                    else 
                        
                    direal_dteta(nireal,aux)=0;

                    end
            end
   end
end
    
if nireal ~= 0
    if nd~=0
    direal_dt(nireal,nd)=0; 
    end
end

%% Calculo da derivda parcial da parte imag do fasor de corrente

if tipo(cont)==17;

    if i(cont)<=j(cont)
        
        niimag = niimag + 1;
                  
        k= i(cont);      % barra de
        m= j(cont);      % barra para
    
            for aux=1:nBarras 

                    if (k==aux)

                    diimag_dV(niimag,aux)=-B(k,m);

                    elseif (m==aux)

                    diimag_dV(niimag,aux)=B(k,m);
                    
                    else 
                        
                    diimag_dV(niimag,aux)=0;

                    end
            end
   end
end
    
if niimag ~= 0
    if nd~=0
    diimag_du(nireal,nd)=0; 
    end
end


%% Calculo da derivda parcial de diferen�a angular nula

if tipo(cont)==11;
    
    ntetanulo = ntetanulo+1;

        for aux=1:nBarras

               if (i(cont)==aux)

                    dtetanulo_dteta (ntetanulo,aux)= 1;
                    
               elseif (j(cont)==aux)

                    dtetanulo_dteta (ntetanulo,aux)= -1;

               else

                    dtetanulo_dteta (ntetanulo,aux)= 0;

               end    

        end
    
                dtetanulo_dt(ntetanulo,nd)=0;   
    
end
    
%% Calculo da derivda parcial de diferen�a de potencial nula

if tipo(cont)==12;
    
    nVnulo = nVnulo+1;

        for aux=1:nBarras

               if (i(cont)==aux)

                    dVnulo_dV (nVnulo,aux)= 1;
               
               elseif (j(cont)==aux)

                    dVnulo_dV (nVnulo,aux)= -1;

               else

                    dVnulo_dV (nVnulo,aux)= 0;

               end    

        end
    
                dVnulo_du(nVnulo,nd)=0;
    
end

%% Calculo da derivada parcial de fluxo de pot�ncia nulo ukm

if tipo(cont)==13;
    
    ntnulo = ntnulo +1;

    dtnulo_dteta(ntnulo,nBarras)=0;
    
    dtnulo_dt (ntnulo,nd)= 0;

            for aux=1:nd
                
               if (i(cont)==disj(aux+nl,1)&&j(cont)==disj(aux+nl,2))
                   
                    jj = state(aux+nl);

                    dtnulo_dt (ntnulo,jj)= 1;
                                  
               end
            end
    
end
    
%% Calculo da derivada parcial de fluxo de pot�ncia nulo ukm

if tipo(cont)==14;
    
    nunulo = nunulo +1;

    dunulo_dV(nunulo,nBarras)=0;
    
    dunulo_du (nunulo,nd)= 0;
    
            for aux=1:nd
                
               if (i(cont)==disj(aux+nl,1)&&j(cont)==disj(aux+nl,2))
                                      
                     jj = state(aux+nl);

                     dunulo_du (nunulo,jj)= 1; 
                     
               end
    
            end
    
end
    
end

%% Criando o vetor Hz�o

    HAA=[dPkm_dteta            dPkm_dt; 
         dPmk_dteta            dPmk_dt;
         dPkmd_dteta           dPkmd_dt;
         dPinj_dteta           dPinj_dt;
         dPinjnula_dteta       dPinjnula_dt;
         dteta_dteta           dteta_dt;
         direal_dteta          direal_dt;
         dBref_dteta           dBref_dt;
         dtetanulo_dteta       dtetanulo_dt;
         dtnulo_dteta     	   dtnulo_dt];
          
%     HRR = [dQkm_dV             dQkm_du;
%            dQmk_dV             dQmk_du;
%            dQkmd_dV            dQkmd_du;
%            dQinj_dV            dQinj_du;
%            dQinjnula_dV        dQinjnula_du; %verificar posi��o!!!!!!!!
%            dV_dV               dV_du;
%            diimag_dV           diimag_du;
%            dVnulo_dV           dVnulo_du;
%            dunulo_dV           dunulo_du];

HAA(:,1)=[];   % eliminando a coluna da barra de refer�ncia

end
