

%% Calculo da matriz Y, G, B, gkm e bkm

% na = linha(:,1); %barra de
% nb = linha (:,2); %barra para
% r = linha (:,3); %resistencia
% x = linha (:,4); %reatancia
% bsh = (linha (:,5)/2); %shunt de linha
% bshb = barra (:,9);

% nramos
nr = size(linha(:,1),1);

%% Vetor/Matriz gkm e bkm

% gkm e bkm - elementos de linha
for aux=1:nr
    matrizbshkm(na(aux),nb(aux))=bsh(aux);
    matrizbshkm(nb(aux),na(aux))=bsh(aux);
end
for aux=1:nbar
    matrizbshb(aux,aux)=bshb(aux);
end
% %% Montando a Matriz Y
% 
% matrizY = Ybarra;
%    
%  %% Montando matriz G e B
% matrizG = real(matrizY);
% matrizB = imag(matrizY);

Y = zeros(nbar);

for k = 1:nbar
    Y(k,k) = 1i*bshb(k);
end

for j = 1:nl
    k = na(j);
    m = nb(j);
    y(j) = 1/(r(j)+1i*x(j));

    Y(k,k) = Y(k,k) + y(j) + 1i*bsh(j);
    Y(m,m) = Y(m,m) + y(j) + 1i*bsh(j);
    Y(k,m) = Y(k,m) - y(j);
    Y(m,k) = Y(m,k) - y(j);
end

matrizY = Ybarra;

 %% Montando matriz G e B
matrizG = real(matrizY);
matrizB = imag(matrizY);

