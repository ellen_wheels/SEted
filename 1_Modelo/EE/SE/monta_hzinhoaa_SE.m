function [ haa ] = monta_hzinhoaa_SE(V, teta, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas, t_est, u_est, state, to, from, nd)


% fun��o calcula haa e hrr

% Monta Estrutura da Matriz hzinho
G=matrizG;
B=matrizB;
g=-G;
b=-B;
bsh=matrizbshkm; %bsh de linha
bshb=matrizbshb; %bsh de barra

%%
tipo = Medidas(:,1);		    	% tipo das barras
nb1 = max(Linha (:,2));
nb2 = max(Linha (:,3));
nBarras= max(nb1,nb2);
nmedidas = size(tipo,1);

n = Medidas(:,3);
i = Medidas(:,4);
j = Medidas(:,5);

disj = [from' to' state];
% n�mero de ramos (totais)
nramos = size(Linha(:,1),1);
% n�mero de linhas (convencionais)
nl = nramos-nd;

%% matriz auxiliar x
         % na        nb          x
xaux = [Linha(:,2) Linha(:,3) Linha(:,5)]; 

for cont = 1:nramos
    w = xaux(cont,1);
    z = xaux(cont,2);
    xdisj(w,z) = xaux(cont,3);
end

%%
nfluatkm = 0; nfluatmk = 0;
nflureatkm = 0; nflureatmk = 0;
ninjat = 0;
ninjreat = 0;
nv = 0;
nteta = 0;
nireal = 0;
niimag = 0;
% ramos chave�veis
nfluatkmd = 0;
nflureatkmd = 0;
ninjatnula = 0;
ninjreatnula = 0;
nref = 0;
ndifangnula = 0;
nfluatnulo = 0;
ndifVnula = 0;
nflureatnulo = 0;

%
nt = 0;
nu = 0;

%%
Pinj=[]; 
Qinj=[]; 
Pkm=[];  
Pmk=[];  
Qkm=[];  
Qmk=[];  
Vmag=[];
tetamag = [];
ireal = [];
iimag = [];

% ramos chave�veis
tkmd = [];
ukmd = [];
Pnula = [];
Qnula = [];
teta_ref = [];
tetakm = []; % diferen�a angular nula - disjuntores fechados 
tkm = []; % fluxo de pot�ncia ativo nulo - disjuntores abertos
Vkm = []; % diferen�a m�dulo da tens�o nula - disjuntores fechados
ukm = []; % fluxo de pot�ncia ativo nulo - disjuntores abertos

%%
    %     C�LCULO DOS P
    Pcalc = zeros(nBarras,1);

    for aux = 1:nl
        k = from(aux);
        m = to(aux);
        Pcalc(k) = Pcalc(k) + (V(k)*V(k)*g(k,m)) - V(k)*V(m)*g(k,m)*cos(teta(k)- teta(m))- V(k)*V(m)*b(k,m)*sin(teta(k)- teta(m));
        Pcalc(m) = Pcalc(m) + (V(m)*V(m)*g(k,m)) - V(m)*V(k)*g(k,m)*cos(teta(k)- teta(m))+ V(m)*V(k)*b(k,m)*sin(teta(k)- teta(m));
    end
    for aux = nl+1:nramos
        k = from(aux);
        m = to(aux);
        l = state(aux);
        Pcalc(k) = Pcalc(k) + t_est(l);
        Pcalc(m) = Pcalc(m) - t_est(l);
    end
    %     C�LCULO DOS Q
    Qcalc=zeros(nBarras,1);

    for aux = 1:nl
        k = from(aux);
        m = to(aux);
        Qcalc(k) = Qcalc(k) - V(k)*V(k)*(b(k,m)+bsh(k,m)) - V(k)*V(m)*g(k,m)*sin(teta(k)- teta(m))+ V(k)*V(m)*b(k,m)*cos(teta(k)- teta(m));
        Qcalc(m) = Qcalc(m) - V(m)*V(m)*(b(k,m)+bsh(k,m)) + V(m)*V(k)*g(k,m)*sin(teta(k)- teta(m))+ V(m)*V(k)*b(k,m)*cos(teta(k)- teta(m));
    end
    for aux = nl+1:nramos
        k = from(aux);
        m = to(aux);
        l = state(aux);
        Qcalc(k) = Qcalc(k) + u_est(l);
        Qcalc(m) = Qcalc(m) - u_est(l);
    end
    
%% Monta express�es

for cont = 1:nmedidas      
%% CALCULA Pkm (COM DADOS DE LINHA)

if tipo(cont)==1;
    
      if i(cont)<=j(cont)

            nfluatkm = nfluatkm + 1;

                k = i(cont);	% n�mero da barra "de"
                m = j(cont);	% n�mero da barra "para"

                Pkm(nfluatkm) = (V(k)^2)*(g(k,m)) - V(k)*V(m)*(g(k,m)*cos(teta(k)- teta(m))+ b(k,m)*sin(teta(k)- teta(m)));

      else

           nfluatmk = nfluatmk + 1;

                m = i(cont);	% n�mero da barra "de"
                k = j(cont);	% n�mero da barra "para"

                Pmk(nfluatmk) = (V(m)^2)*(g(k,m)) - V(m)*V(k)*(g(k,m)*cos(teta(k)- teta(m))- b(k,m)*sin(teta(k)- teta(m))); 

     end
end

%% CALCULA Pkmd (COM DADOS DOS DISJUNTORES)

if tipo(cont)==2;    
    
    nfluatkmd = nfluatkmd +1;
        
    k = i(cont);	% n�mero da barra "de"
    m = j(cont);    % n�mero da barra "para"
    
%     
%     if (xdisj(k,m)==0||xdisj(k,m)==9999)% verifica se � um ramo chave�vel
%         nfluatkmd = nfluatkmd + 1;
%         tkmd(nfluatkmd) = t_est(nfluatkmd); % alterar equa��o % puxar estados
%     end
        for aux=1:nd
               if (k==disj(aux+nl,1)&&m==disj(aux+nl,2))
                    if k<=m
                        tkmd (nfluatkmd) = t_est(aux);
                    else
                        tkmd (nfluatkmd) = -t_est(aux);
                    end
               end
        end
end

%% CALCULA Qkm (COM DADOS DE LINHA)
if tipo(cont)==3;
    
       if i(cont)<=j(cont)  

        nflureatkm = nflureatkm + 1;

            k = i(cont);	% n�mero da barra "de"
            m = j(cont);    % n�mero da barra "para"

            Qkm(nflureatkm) = -(V(k)^2)*(b(k,m)+bsh(k,m)) - V(k)*V(m)*(g(k,m)*sin(teta(k)- teta(m))- b(k,m)*cos(teta(k)- teta(m)));

        else

        nflureatmk = nflureatmk + 1;

            m = i(cont);	% n�mero da barra "de"
            k = j (cont);   % n�mero da barra "para"

            Qmk(nflureatmk) = -(V(m)^2)*(b(m,k)+bsh(m,k)) + V(m)*V(k)*(g(m,k)*sin(teta(k) - teta(m))+ b(m,k)*cos(teta(k)- teta(m)));
        end
end

%%  CALCULA Qkmd (COM DADOS DOS DISJUNTORES)
if tipo(cont)==4;
    
        nflureatkmd = nflureatkmd+1;

    
     k = i(cont);	% n�mero da barra "de"
     m = j(cont);		% n�mero da barra "para"
%     
%      if (xdisj(k,m)==0||xdisj(k,m)==9999)% verifica se � um ramo chave�vel
%          nflureatkmd = nflureatkmd + 1;
%          ukmd(nflureatkmd) = u_est(nflureatkmd);
%      end
     
        for aux=1:nd
               if (k==disj(aux+nl,1)&&m==disj(aux+nl,2))
                    if k<=m
                        ukmd (nflureatkmd) = u_est(aux);
                    else
                        ukmd (nflureatkmd) = -u_est(aux);
                    end
               end
        end
end

%% CALCULA Pinj (COM DADOS DE BARRA)

if tipo(cont)==5;
  
      ninjat = ninjat + 1;
      k = n(cont);
      Pinj(ninjat)=Pcalc(k);
end

%% CALCULA Pinj_nula 

if tipo(cont)==6;
         
      ninjatnula = ninjatnula + 1;
          
      Pnula(ninjatnula) = 0; 
      
end

%% CALCULA Qinj (COM DADOS DE BARRA)

if tipo(cont)==7;
    
     ninjreat = ninjreat + 1;
     k = n(cont);
     Qinj(ninjreat)=Qcalc(k);
end  

%% CALCULA Qinj_nula 

if tipo(cont)==8;
    
        ninjreatnula =  ninjreatnula + 1;

        Qnula(ninjreatnula) = 0;     
end

%% �NGULO DA BARRA DE REFER�NCIA

if tipo(cont)==9;
    
%         k = n(cont);
    
        nref = nref + 1;

        teta_ref (nref) = 0; % teta(k)
end

%% CALCULA Vmag (NA BARRA)

if tipo(cont)==10;
    
        nv = nv + 1;

        k = n(cont);  

        Vmag(nv)= V(k);
end

%% CALCULA �ngulo (NA BARRA)

if tipo(cont)==15;
    
        nteta = nteta + 1;

        k = n(cont);  

        tetamag(nteta)= teta(k);
end

%% CALCULA FASOR DA CORRENTE PARTE REAL (NA LINHA)


if tipo(cont)==16;
    
      if i(cont)<=j(cont)

            nireal = nireal + 1;

                k = i(cont);	% n�mero da barra "de"
                m = j(cont);	% n�mero da barra "para"

%                 ireal(nireal) = -b(k,m)*(teta(k)- teta(m));
         ireal(nireal) = g(k,m)*V(k)*cos(teta(k)-teta(m))-V(m)*g(k,m)-(b(k,m)+bsh(k,m))*V(k)*sin(teta(k)-teta(m));

      end
end

%% CALCULA FASOR DA CORRENTE PARTE IMAGINARIA (NA LINHA)

if tipo(cont)==17;
    
      if i(cont)<=j(cont)

  
            niimag = niimag + 1;

                k = i(cont);	% n�mero da barra "de"
                m = j(cont);	% n�mero da barra "para"

%                 iimag(niimag) = b(k,m)*(V(k)- V(m));
            iimag(niimag) = (b(k,m)+bsh(k,m))*V(k)*cos(teta(k)-teta(m))+g(k,m)*V(k)*sin(teta(k)-teta(m))-b(k,m)*V(m);

      end
end

%% DIFEREN�A ANGULAR NULA (DISJUNTOR FECHADO)

if tipo(cont)==11;
    
        ndifangnula = ndifangnula + 1;
        
        k = i(cont);	% n�mero da barra "de"
        m = j(cont);		% n�mero da barra "para"
        
        if k<=m
            tetakm(ndifangnula)=teta(k)-teta(m);
        else
            tetakm(ndifangnula)=teta(m)-teta(k);
        end
        
    
end

%% DIFEREN�A DE POTENCIAL NULA (DISJUNTOR FECHADO)

if tipo(cont)==12;
    
        ndifVnula = ndifVnula + 1;
        
        k = i(cont);	% n�mero da barra "de"
        m = j(cont);		% n�mero da barra "para"
        
        if k<=m
            Vkm (ndifVnula) = V(k)-V(m);
        else 
            Vkm (ndifVnula) = V(m)-V(k);
        end
        
end

%% FLUXO DE POT�NCIA ATIVO NULO (DISJUNTOR ABERTO)

if tipo(cont)==13;
    
        nfluatnulo = nfluatnulo + 1;
        
        k = i(cont);	% n�mero da barra "de"
        m = j(cont);		% n�mero da barra "para"
        
        for aux=1:nd
               if (k==disj(aux+nl,1)&&m==disj(aux+nl,2))
                    if k<=m
                        tkm (nfluatnulo) = t_est(aux);
                    else
                        tkm (nfluatnulo) = -t_est(aux);
                    end
               end
        end
end

%% FLUXO DE POT�NCIA REATIVO NULO (DISJUNTOR FECHADO)

if tipo(cont)==14;
    
        nflureatnulo = nflureatnulo + 1;
        
        k = i(cont);	% n�mero da barra "de"
        m = j(cont);		% n�mero da barra "para"
        
        for aux=1:nd
               if (k==disj(aux+nl,1)&&m==disj(aux+nl,2))
                    if k<=m
                        ukm (nflureatnulo) = u_est(aux);
                    else
                        ukm (nflureatnulo) = -u_est(aux);
                    end
               end
        end
end

end

%% Calculo de hzinho

  haa = [Pkm';  % fluxo de pot�ncia ativa em ramos convencionais -> f(v,teta)
         Pmk';  % fluxo de pot�ncia ativa em ramos convencionais -> f(v,teta)
         tkmd'; % fluxo de pot�ncia ativa em ramos chave�veis -> f(tkm)
         %tmkd'; % fluxo de pot�ncia em ramos chave�veis -> f(-tkm)
         Pinj'; % inje�ao de pot�ncia ativa nas barras -> f(v,teta,tkm)
         tetamag'; %�ngulo
         Pnula'; % inje��o de pot�ncia ativa nula nas barras 
         ireal';
         teta_ref'; % numero de barras de refer�ncia
         tetakm'; % diferen�a angular nula - disjuntores fechados
         tkm' ]; % fluxo de pot�ncia ativo nulo - disjuntores abertos];
    
 hrr = [Qkm';
         Qmk';
         ukmd'; % fluxo de pot�ncia em ramos chave�veis
         %umkd'; % fluxo de pot�ncia em ramos chave�veis
         Qinj';
         Qnula'; % inje��o nula
         Vmag';
         iimag';
         Vkm'; % diferen�a m�dulo da tens�o nula - disjuntores fechados
         ukm' ]; % fluxo de pot�ncia reativo nulo - disjuntores abertos];
end