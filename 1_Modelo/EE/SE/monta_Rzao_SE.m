function [RA,RR] = monta_Rzao_SE(Medidas, Linha)

% R - matriz covari�ncia
% par�metros

%%
var = Medidas(:,7);
tipo = Medidas (:,1);
k = Medidas (:,4);
m = Medidas (:,5);

nmedidas = size(tipo,1);

% nramos
nramos = size(Linha(:,1),1);

x = Linha (:,5); % reatancia

%% matriz auxiliar x

xaux = [Linha(:,2) Linha(:,3) Linha(:,5)]; 

for cont = 1:nramos
    a = xaux(cont,1);
    b = xaux(cont,2);
    xdisj(a,b) = xaux(cont,3);
end

%%

ypkm = 0;
ypmk = 0;
yqkm = 0;
yqmk = 0;
ypinj = 0;
yqinj = 0;
yv = 0;
yteta = 0;
yireal = 0;
yiimag = 0;
% ramos chave�veis
ypkmd = 0;
yqkmd = 0;
ypnula = 0;
yqnula = 0;
ytetaref = 0;
ytetanulo = 0;
ytnulo = 0;
yVnulo = 0;
yunulo = 0;


varPkm = [];
varPmk = [];
varQkm = [];
varQmk = [];
varPinj = [];
varQinj = [];
varV = [];
varteta = [];
varireal = [];
variimag = [];
% ramos chave�veis
varPkmd = [];
varQkmd = [];
varPnula = [];
varQnula = [];
vartetaref = [];
vartetanulo = [];
vartnulo = [];
varVnulo = [];
varunulo = [];

%%
for aux = 1:nmedidas;
%% FLUXO DE POT�NCIA ATIVO EM RAMOS CONVENCIONAIS

        if tipo (aux) == 1; % fluxo ativo
            if k <= m %fluxo da barra k para a barra m 
                ypkm = ypkm+1;
                varPkm(ypkm,1) = var(aux).^2;
            else %fluxo da barra m para a barra k
                ypmk = ypmk+1;
                varPmk(ypmk,1) = var(aux).^2;
            end
        end
        
%% FLUXO DE POT�NCIA ATIVO EM RAMOS CHAVE�VEIS
        if tipo (aux) == 2; % fluxo ativo nas chaves
            a = k(aux);
            b = m(aux);
            if (xdisj(a,b)==0||xdisj(a,b)==9999)% verifica se � um ramo chave�vel
                ypkmd = ypkmd+1;
                varPkmd(ypkmd,1) = var(aux).^2;
            end
        end
%% FLUXO DE POT�NCIA REATIVO EM RAMOS CONVENCIONAIS 
        if tipo (aux) == 3; % fluxo reativo
                if k <= m %fluxo da barra k para a barra m 
                    yqkm = yqkm+1;
                    varQkm(yqkm,1) = var(aux).^2;
                else %fluxo da barra m para a barra k
                    yqmk = yqmk+1;
                    varQmk(yqmk,1) = var(aux).^2;
                end
        end
%% FLUXO DE POT�NCIA REATIVO EM RAMOS CHAVE�VEIS
        if tipo (aux) == 4; % fluxo reativo nas chaves
            a = k(aux);
            b = m(aux);
                if (xdisj(a,b)==0||xdisj(a,b)==9999) % verifica se � um ramo chave�vel
                yqkmd = yqkmd+1;
                varQkmd(yqkmd,1) = var(aux).^2;
                end
        end
%% INJE��O DE POT�NCIA ATIVA
        if tipo(aux) == 5; 
            ypinj = ypinj+1;
            varPinj(ypinj,1) = var(aux).^2;
        end
        
%% INJE��O DE POT�NCIA ATIVA NULA
        if tipo(aux) == 6;  
                ypnula = ypnula + 1;
                varPnula(ypnula,1) = var(aux).^2;
        end
%% INJE��O DE POT�NCIA REATIVA 
        if tipo(aux) == 7; 
                yqinj = yqinj+1;
                varQinj(yqinj,1)= var(aux).^2; 
        end
%% INJE��O DE POT�NCIA REATIVA NULA
        if tipo(aux) == 8;              
                yqnula = yqnula + 1;
                varQnula(yqnula,1) = var(aux).^2;
        end
%% BARRA DE REFER�NCIA
        if tipo(aux) == 9;              
                ytetaref = ytetaref + 1;
                vartetaref(ytetaref,1) = var(aux).^2;
        end
 %% M�DULO DO FASOR DE CORRENTE PARTE REAL
    if tipo(aux) == 16; 
       yireal = yireal+1;
       varireal(yireal,1)= var(aux).^2;  
    end       

%% M�DULO DE TENS�O
    if tipo(aux) == 10; 
       yv = yv+1;
       varV(yv,1)= var(aux).^2;  
    end
     %% M�DULO DO FASOR DE CORRENTE PARTE IMAGIN�RIA
    if tipo(aux) == 17; 
       yiimag = yiimag+1;
       variimag(yiimag,1)= var(aux).^2;  
    end       

    %% �NGULO
    if tipo(aux) == 15; 
       yteta = yteta+1;
       varteta(yteta,1)= var(aux).^2;  
    end
  %% DIFEREN�A ANGULAR NULA EM RAMOS CHAVE�VEIS FECHADOS
  if tipo(aux) == 11;
      ytetanulo = ytetanulo+1;
      vartetanulo(ytetanulo,1)= var(aux).^2;
  end
  
  %% DIFEREN�A DE POTENCIAL NULA EM RAMOS CHAVE�VEIS FECHADOS
  if tipo(aux) == 12;
      yVnulo = yVnulo+1;
      varVnulo(yVnulo,1)= var(aux).^2;
  end
  
  %% FLUXO DE POT�NCIA ATIVO NULO EM RAMOS CHAVE�VEIS ABERTOS
  if tipo(aux) == 13;
      ytnulo = ytnulo+1;
      vartnulo(ytnulo,1)= var(aux).^2;
  end
  
  %% FLUXO DE POT�NCIA REATIVO NULO EM RAMOS CHAVE�VEIS ABERTOS
  if tipo(aux) == 14;
      yunulo = yunulo+1;
      varunulo(yunulo,1)= var(aux).^2;
  end
  
end

%%
% monta mariz R - vari�ncia de medidas
% matriz diagonal

%%RA

raaux = [varPkm;
        varPmk;
        varPkmd;
        varPinj;
        varteta;
        varPnula;
        varireal;
        vartetaref;
        vartetanulo;
        vartnulo];
    
    RA = diag(raaux);
    
%%RR

rraux = [varQkm;
         varQmk;
         varQkmd;
         varQinj;
         varQnula;
         varV;
         variimag;
         varVnulo;
         varunulo];
     
     RR = diag (rraux);
     

end

