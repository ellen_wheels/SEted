function [ delta_zA,  delta_zR ] = monta_deltaz_SE(zA, zR, haa, hrr)
% ESTA FUN��O CALCULA O TA=HAA'*RA^-1*AzA'
%                       TR=HRR'*RR^-1*AzR'

delta_zA = zA-haa;
delta_zR = zR-hrr;

end