function [ delta_zRlinha ] = monta_delta_zRlinha( delta_zR, V, Medidas)
% ESTA FUN��O CALCULA O TA=HAA'*RA^-1*AzA'
%                       TR=HRR'*RR^-1*AzR'

tipo = Medidas(:,1);
n = Medidas (:,3);
i = Medidas (:,4);
j = Medidas (:,5);
nmedidas = size(tipo,1);

nfluatkm = 0; nfluatmk = 0;
nflureatkm = 0; nflureatmk = 0;
ninjat = 0;
ninjreat = 0;
nv = 0;

Vat=[];
Vatkm=[];
Vatmk=[];
Vreat=[];
Vreatkm=[];
Vreatmk=[];
Vv=[];

for cont = 1:nmedidas
    if tipo (cont)==1; %fluxo ativo
        if i(cont)<=j(cont)
        
        nfluatkm = nfluatkm + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vatkm(nfluatkm) = V(k);
        
        else
        
        nfluatmk = nfluatmk + 1;
        Vatmk(nfluatmk) = V(m);
        
        end
    end
    if tipo (cont)==2; %fluxo reativo
        if i(cont)<=j(cont)
        
        nflureatkm = nflureatkm + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
        
        Vreatkm(nflureatkm) = V(k);
        
        else
        
        nflureatmk = nflureatmk + 1;
        Vreatmk(nflureatmk) = V(m);
        end
        
    end
    if tipo (cont)==3; %inje��o ativa
       k=n(cont);  
       ninjat = ninjat + 1;
       
       Vat(ninjat) = V(k);
       
    end
    if tipo (cont)==4; %inje��o reativa
       k=n(cont);  
       ninjreat = ninjreat + 1;
       
       Vreat(ninjreat) = V(k);
       
    end
    if tipo (cont)==5; %tens�o
       k=n(cont);  
       nv = nv + 1;
       
       Vv(nv) = V(k);
       
    end
end

%%
  
Vr = [Vreat';
      Vreatkm';
      Vreatmk';
      Vv'];

 delta_zRlinha = delta_zR./Vr;


end