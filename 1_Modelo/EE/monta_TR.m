function [ TR ] = monta_TR( HRR, RR, delta_zRlinha)
% ESTA FUN��O CALCULA O TA=HAA'*RA^-1*AzA'
%                       TR=HRR'*RR^-1*AzR'

TR = HRR'*inv(RR)*delta_zRlinha;

end