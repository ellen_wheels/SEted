function [ hrr ] = monta_hzinhorr(V, teta, matrizG, matrizB, matrizbshkm, matrizbshb, Linha, Medidas)

% ESTA FUN��O CALCULA O haa e hrr

% Monta Estrutura da Matriz hzinho
G=matrizG;
B=matrizB;
g=-G;
b=-B;
bsh=matrizbshkm; %bsh de linha
bshb=matrizbshb; %bsh de barra

%%
tipo = Medidas(:,1);			    	% tipo das barras
nb1 = max(Linha (:,2));
nb2 = max(Linha (:,3));
nBarras= max(nb1,nb2);
nmedidas = size(tipo,1);

n = Medidas(:,3);
i = Medidas(:,4);
j = Medidas(:,5);

ii = Linha (:,2);
jj = Linha (:,3);

% nramos
nramos = size(Linha(:,1),1);

%%
nfluatkm = 0; nfluatmk = 0;
nflureatkm = 0; nflureatmk = 0;
ninjat = 0;
ninjreat = 0;
nv = 0;

%%
Pinj=[]; 
Qinj=[]; 
Pkm=[];  
Pmk=[];  
Qkm=[];  
Qmk=[];  
Vmag=[]; 


%%
    %     C�LCULO DOS P
    Pcalc = zeros(nBarras,1);

    for aux = 1:nramos
        k = ii(aux);
        m = jj(aux);
        Pcalc(k) = Pcalc(k) + (V(k)^2)*(g(k,m)) - V(k)*V(m)*(g(k,m)*cos(teta(k)- teta(m))+ b(k,m)*sin(teta(k)- teta(m)));
        Pcalc(m) = Pcalc(m) + (V(m)^2)*(g(k,m)) - V(m)*V(k)*(g(k,m)*cos(teta(k)- teta(m))- b(k,m)*sin(teta(k)- teta(m)));
    end
    %     C�LCULO DOS Q
    Qcalc=zeros(nBarras,1);

    for aux = 1:nramos
        k = ii(aux);
        m = jj(aux);
        Qcalc(k) = Qcalc(k) - (V(k)^2)*(b(k,m)+bsh(k,m)) - V(k)*V(m)*(g(k,m)*sin(teta(k)- teta(m))- b(k,m)*cos(teta(k)- teta(m)));
        Qcalc(m) = Qcalc(m) - (V(m)^2)*(b(m,k)+bsh(m,k)) + V(m)*V(k)*(g(m,k)*sin(teta(k)- teta(m))+ b(m,k)*cos(teta(k)- teta(m)));
    end
%% Monta express�es

for cont = 1:nmedidas    
%% Calcula Pkm e Qkm (com dados de linha)

if tipo(cont)==1;
   
        if i(cont)<=j(cont)

            nfluatkm = nfluatkm + 1;

                k = i(cont);	% n�mero da barra "de"
                m = j(cont);		% n�mero da barra "para"

                Pkm(nfluatkm) = (V(k)^2)*(g(k,m)) - V(k)*V(m)*(g(k,m)*cos(teta(k)- teta(m))+ b(k,m)*sin(teta(k)- teta(m)));

        else

           nfluatmk = nfluatmk + 1;

                m = i(cont);	% n�mero da barra "de"
                k = j(cont);		% n�mero da barra "para"

                Pmk(nfluatmk) = (V(m)^2)*(g(k,m)) - V(m)*V(k)*(g(k,m)*cos(teta(k)- teta(m))- b(k,m)*sin(teta(k)- teta(m))); 

        end
end
    
%% 
if tipo(cont)==2;
    
        if i(cont)<=j(cont)  

        nflureatkm = nflureatkm + 1;

            k = i(cont);	% n�mero da barra "de"
            m = j(cont);		% n�mero da barra "para"

            Qkm(nflureatkm) = -(V(k)^2)*(b(k,m)+bsh(k,m)) - V(k)*V(m)*(g(k,m)*sin(teta(k)- teta(m))- b(k,m)*cos(teta(k)- teta(m)));

        else

        nflureatmk = nflureatmk + 1;

            m = i(cont);	% n�mero da barra "de"
            k = j (cont);		% n�mero da barra "para"

            Qmk(nflureatmk) = -(V(m)^2)*(b(m,k)+bsh(m,k)) + V(m)*V(k)*(g(m,k)*sin(teta(k) - teta(m))+ b(m,k)*cos(teta(k)- teta(m)));
        end
    
end

%% Calcula Pinjcalc e Qinjcalc (com dados de barra)

if tipo(cont)==3;
    ninjat = ninjat + 1;
      k = n(cont);
      Pinj(ninjat)=Pcalc(k);
end

%%

if tipo(cont)==4;
    
    ninjreat = ninjreat + 1;
     k = n(cont);
     Qinj(ninjreat)=Qcalc(k);
end

%% Calcula Vmag (na barra)

if tipo(cont)==5;
    
    nv = nv + 1;
    
    k= n(cont);  
    
    Vmag(nv)= V(k);
end

end


%% Calculo de Hzinho

  hrr = [Qkm';
         Qmk';
         Qinj';
         Vmag'];
end