function [HRR] = monta_HRR(V, teta, matrizG, matrizB, matrizgkm, matrizbkm, matrizbshkm, Linha, Medidas, ref)
%ESTA fun��o calcula H(x)

% Monta Estrutura da Matriz hzinho
G=matrizG;
B=matrizB;
g=matrizgkm;
b=matrizbkm;
bsh=matrizbshkm; %bsh de linha

Barra_ref=ref;

nb1 = max(Linha (:,2));
nb2 = max(Linha (:,3));
nBarras= max(nb1,nb2);
tipo = Medidas(:,1);		    	% tipo das barras
nmedidas = size(tipo,1);

n = Medidas (:,3);
i = Medidas (:,4);
j = Medidas (:,5);

nfluatkm = 0; nfluatmk = 0;
nflureatkm = 0; nflureatmk = 0;
ninjat = 0;
ninjreat = 0;
nv = 0;

dPkm_dteta=[]; dPmk_dteta=[];
dQkm_dV=[]; dQmk_dV=[]; 
dPinj_dteta=[]; 
dQinj_dV=[];  
dV_dV=[];  

for cont = 1:nmedidas
    
    
% Calculo da derivada parcial de Pkm e Pmk

if tipo(cont)==1;
 
    if i(cont)<=j(cont)
        
        nfluatkm = nfluatkm + 1;
          
        k= i(cont);      % barra de
        m= j(cont);      % barra para
    
            for aux=1:nBarras            
                                        
                 if k==aux

                dPkm_dteta(nfluatkm,aux)= V(k)*V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                    -b(k,m)*cos(teta(k)-teta(m)));

                elseif m==aux    

                dPkm_dteta(nfluatkm,aux)= -V(k)*V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                    -b(k,m)*cos(teta(k)-teta(m)));

                else

                dPkm_dteta(nfluatkm,aux)=0; 
                
                end
            end
    else
        
        nfluatmk = nfluatmk + 1;
        
        for aux=1:nBarras        
                        
                if k==aux

                dPmk_dteta(nfluatmk,aux)= V(k)*V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

                elseif m==aux    

                dPmk_dteta(nfluatmk,aux)= -V(k)*V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

                else

                dPmk_dteta(nfluatmk,aux)=0; 

                end
        end
   end
end

%% Calculo da derivda parcial de Qkm e Qmk

if tipo(cont)==2;
 
    if i(cont)<=j(cont)
        
       nflureatkm = nflureatkm + 1;
       
        for aux=1:nBarras                    % esse aux representa a posi��o no vetor x transposto

               k= i(cont);      % barra de
               m= j(cont);      % barra para

               
                if k==aux

                    dQkm_dV(nflureatkm,aux)= -V(m)*(g(k,m)*sin(teta(k)-teta(m))...
                        -b(k,m)*cos(teta(k)-teta(m)))-2*V(k)*(b(k,m)+bsh(k,m));

                elseif m==aux    

                    dQkm_dV(nflureatkm,aux)= -V(k)*(g(k,m)*sin(teta(k)-teta(m))...
                        -b(k,m)*cos(teta(k)-teta(m)));

                else

                   dQkm_dV(nflureatkm,aux)=0;

                end
        end

    else
        
        nflureatmk = nflureatmk + 1;
        
       for aux=1:nBarras             % esse aux representa a posi��o no vetor x transposto

               if k==aux

                dQmk_dV(nflureatmk,aux)= (-V(m)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k))))-2*V(k)*(b(k,m)+bsh(k,m));

            elseif m==aux    

                dQmk_dV(nflureatmk,aux)= -V(k)*(g(k,m)*sin(teta(m)-teta(k))...
                    -b(k,m)*cos(teta(m)-teta(k)));

            else

                dQmk_dV(nflureatmk,aux)=0;

               end
               
       end
       
    end
 
end

%% Calculo da derivada parcial de Pinj, sendo  Pinj as inje��es de barra

if tipo (cont)==3;

       k=n(cont);  
       
       ninjat = ninjat + 1;

        for aux=1:nBarras   % esse aux representa a posi��o no vetor x transposto
                
                if aux==k
                
                somadP_dteta=0; 

                    for m=1:nBarras     

                        somadP_dteta=somadP_dteta+V(k)*V(m)*(-G(k,m)*sin(teta(k)-teta(m))+B(k,m)*cos(teta(k)-teta(m)));

                    end

                
                dPinj_dteta(ninjat,aux)=somadP_dteta-V(k)^2*B(k,k); 

                else              

                dPinj_dteta(ninjat,aux)= V(k)*V(aux)*(G(k,aux)*sin(teta(k)-teta(aux))-B(k,aux)*cos(teta(k)-teta(aux)));
            
                end
        end 
end

%% Calculo da derivada parcial de Qinj, sendo  Qinj as inje��es de barra
 
if tipo(cont)==4;
 
    k= n(cont);   
    
    ninjreat =  ninjreat + 1;

        for aux=1:nBarras   % esse aux representa a posi��o no vetor x transposto
               
             if aux==k

              somadQ_dV=0;

                for m=1:nBarras      

                    somadQ_dV=somadQ_dV+V(m)*(G(k,m)*sin(teta(k)-teta(m))-B(k,m)*cos(teta(k)-teta(m)));
                end             
                   
                dQinj_dV(ninjreat,aux)= somadQ_dV-V(k)*B(k,k);

             else              

                dQinj_dV(ninjreat,aux)= V(k)*(G(k,aux)*sin(teta(k)-teta(aux))-B(k,aux)*cos(teta(k)-teta(aux)));             
             end
              
        end
end

%% Calculo da derivda parcial de Vmag

if tipo(cont)==5;

    k= n(cont);   

    nv = nv + 1;
    
        for aux=1:nBarras   % esse aux representa a posi��o no vetor x transposto
                
                if aux==k      

                    dV_dV(nv,aux)=1;

                else

                    dV_dV(nv,aux)=0;

                end
        end    
end

end

%% Criando o vetor Hz�o

    HRR = [dQkm_dV ;
           dQmk_dV ;
           dQinj_dV;
           dV_dV ];

end
